import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-ratio-voting-widget',
  templateUrl: './ratio-voting-widget.component.html',
  styleUrls: ['./ratio-voting-widget.component.scss']
})
export class RatioVotingWidgetComponent implements OnInit {
  data: any = {};
  init = false;
  empty = true;

// top_percent =
  @Input() set newData(data) {
    if (data.data) {
      // data.is_gov_adopted = true
      this.empty = false;
      // // console.log(data);
      // if (data.is_gov_adopted) {
      //   data.top_percent = Math.floor((data.gov_for_sum - data.gov_vote_count) * 100 / data.gov_sum);
      //   data.top_percent = Math.floor((data.gov_sum - data.gov_vote_count) * 100 / data.gov_sum);
      //   // gov_percent
      //   // data: true
      //   // gov_for_sum: 196
      //   // gov_sum: 421
      //   // is_gov_adopted: false
      //   // is_users_adopted: true
      //   // users_for_sum: 7
      //   // users_sum: 13
      // } else {
      //   data.top_percent = Math.floor(data.gov_vote_count * 100 / data.gov_sum);
      // }
      // data.top_percent = Math.floor(data.gov_vote_count * 100 / data.gov_sum);
      data.bottom_percent = Math.floor(data.users_vote_count * 100 / data.users_sum);
      this.data = data;
      this.init = true;
    } else {
      this.empty = true;
    }
  }

  constructor() {
  }

  ngOnInit() {
  }

}
