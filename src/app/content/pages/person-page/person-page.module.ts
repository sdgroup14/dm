import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PersonPageRoutingModule} from './person-page-routing.module';
import {PersonPageComponent} from './person-page.component';
import {PersonPageService} from './services/person-page.service';
import {FinancialHelpModule} from '../../templates/widgets/financial-help/financial-help.module';
import {RightWidgetToolbarModule} from '../../templates/widgets/right-widget-toolbar/right-widget-toolbar.module';
import {FixedSocialModule} from '../../templates/widgets/fixed-social/fixed-social.module';
import {RightWidgetComponent} from './components/right-widget/right-widget.component';
import {MoreArticlesWidgetModule} from '../../templates/widgets/more-articles-widget/more-articles-widget.module';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {FixedWidgetModule} from '../../../directives/fixed-widget/fixed-widget.module';
import {PageMinHeightModule} from '../../../directives/page-min-height/page-min-height.module';
import {SheredTranslateModule} from '../../../modules/shered-translate.module';
import {SingleOpitionWidgetModule} from '../../templates/widgets/single-opition-widget/single-opition-widget.module';
import {TopComponent} from './components/top/top.component';
import {PartiesHistoryComponent} from './components/parties-history/parties-history.component';
import {StatisticModule} from './components/statistic/statistic.module';
import {LawsProblemsModule} from './components/laws-problems/laws-problems.module';
import {AddArticleBtnModule} from '../../templates/elements/add-article-btn/add-article-btn.module';
import {EmpyListStringModule} from '../../templates/elements/empy-list-string/empy-list-string.module';
import {MissingsComponent} from './components/missings/missings.component';
import {SheredTooltipModule} from '../../../modules/shered-tooltip.module';

@NgModule({
  declarations: [
    PersonPageComponent,
    RightWidgetComponent,
    TopComponent,
    PartiesHistoryComponent,
    MissingsComponent
  ],
  imports: [
    CommonModule,
    PersonPageRoutingModule,
    FinancialHelpModule,
    RightWidgetToolbarModule,
    FixedSocialModule,
    MoreArticlesWidgetModule,
    PerfectScrollbarModule,
    FixedWidgetModule,
    PageMinHeightModule,
    SheredTranslateModule,
    SingleOpitionWidgetModule,
    StatisticModule,
    LawsProblemsModule,
    AddArticleBtnModule,
    EmpyListStringModule,
    SheredTooltipModule
  ],
  providers: [
    PersonPageService
  ]
})
export class PersonPageModule {
}
