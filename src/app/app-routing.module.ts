import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {LangUaGuard} from './guards/lang-ua.guard';
import {LangRuGuard} from './guards/lang-ru.guard';
import {AuthEmailComponent} from './content/pages/auth-email-page/auth-email.component';
// import {AuthGuard} from './guards/auth.guard';
import {AuthPageComponent} from './content/pages/auth-page/auth-page.component';
import {PostLayoutPreloaderComponent} from './content/templates/preloaders/post-layout-preloader/post-layout-preloader.component';
import {AddPersonModalComponent} from './content/templates/main-elements/add-person-modal/add-person-modal.component';
import {FeedbackModalComponent} from './content/templates/main-elements/feedback-modal/feedback-modal.component';
import {VotesModalComponent} from './content/pages/law-page/templates/votes-modal/votes-modal.component';
import {AuthModalComponent} from './content/templates/main-elements/auth-modal/auth-modal.component';
// import {MainMenuModalComponent} from './content/templates/main-elements/main-menu-modal/main-menu-modal.component';
import {InfoModalComponent} from './content/templates/main-elements/info-modal/info-modal.component';
import {ContradictionModalComponent} from './content/templates/main-elements/contradiction-modal/contradiction-modal.component';
import {AuthModalGuard} from './guards/auth-modal.guard';
import {AuthGuardLogOut} from './guards/auth2.guard';

const routes: Routes = [
  {
    path: 'auth',
    component: AuthPageComponent
  },
  {
    path: 'preloader',
    component: PostLayoutPreloaderComponent,
    outlet: 'modals'
  },
  {
    path: 'add-person',
    component: AddPersonModalComponent,
    outlet: 'modals'
  },
  {
    path: 'feedback',
    component: FeedbackModalComponent,
    outlet: 'modals'
  },
  {
    path: 'contradiction',
    component: ContradictionModalComponent,
    outlet: 'modals',
    canActivate: [AuthModalGuard]
  },

  {
    path: 'law-votes-modal',
    component: VotesModalComponent,
    outlet: 'modals'
  },
  {
    path: 'auth',
    component: AuthModalComponent,
    outlet: 'modals'
  },
  // {
  //   path: 'main-menu-modal',
  //   component: MainMenuModalComponent,
  //   outlet: 'modals'
  // },
  {
    path: 'info',
    component: InfoModalComponent,
    outlet: 'modals'
  },
  {
    path: 'ru',
    canActivate: [LangRuGuard],
    children: [
      {
        path: 'account',
        loadChildren: () => import('./content/pages/account-page/account-page.module').then(mod => mod.AccountPageModule),
        canActivate: [AuthGuardLogOut]
      },
      {
        path: 'donate',
        loadChildren: () => import('./content/pages/donate-page/donate-page.module').then(mod => mod.DonatePageModule)
      },
      {
        path: 'info',
        loadChildren: () => import('./content/pages/info-page/info-page.module').then(mod => mod.InfoPageModule)
      },
      {
        path: 'user-agreement',
        loadChildren: () => import('./content/pages/terms-policy-page/terms-policy-page.module').then(mod => mod.TermsPolicyPageModule),
        data: {url: '/user-agreement'}
      },
      {
        path: 'privacy-policy',
        loadChildren: () => import('./content/pages/terms-policy-page/terms-policy-page.module').then(mod => mod.TermsPolicyPageModule),
        data: {url: '/policy'}
      },
      {
        path: 'laws',
        loadChildren: () => import('./content/pages/laws-page/laws-page.module').then(mod => mod.LawsPageModule)
      },
      {
        path: 'persons',
        loadChildren: () => import('./content/pages/persons-list-page/persons-list-page.module').then(mod => mod.PersonsListPageModule)
      },
      {
        path: 'parties',
        loadChildren: () => import('./content/pages/party-list-page/party-list-page.module').then(mod => mod.PartyListPageModule)
      },
      {
        path: 'person',
        loadChildren: () => import('./content/pages/person-page/person-page.module').then(mod => mod.PersonPageModule)
      },
      {
        path: 'election/vr2019',
        loadChildren: () => import('./content/pages/election-page/election-page.module').then(mod => mod.ElectionPageModule)
      },
      {
        path: 'law',
        loadChildren: () => import('./content/pages/law-page/law-page.module').then(mod => mod.LawPageModule)
      },
      {
        path: 'party',
        loadChildren: () => import('./content/pages/party-page/party-page.module').then(mod => mod.PartyPageModule)
      },
      {
        path: 'materials',
        loadChildren: () => import('./content/pages/materials-page/materials-page.module').then(mod => mod.MaterialsPageModule)
      },
      {
        path: 'material',
        loadChildren: () => import('./content/pages/article/article.module').then(mod => mod.ArticleModule),
        data: {
          isIndexin: true
        }
      },
      {
        path: 'preview',
        loadChildren: () => import('./content/pages/article/article.module').then(mod => mod.ArticleModule),
        data: {
          isIndexin: false
        }
      },
      {
        path: 'create/person',
        // canActivate: [AuthGuard],
        loadChildren: () => import('./content/pages/add-person-page/add-person-page.module').then(mod => mod.AddPersonPageModule)
      },
      {
        path: 'create/material', data: {notif: 'errors.auth-add-material-error'},
        // canActivate: [AuthGuard],
        loadChildren: () => import('./content/pages/add-material/add-material.module').then(mod => mod.AddMaterialModule)
      },

      {
        path: 'edit/material', data: {notif: 'errors.auth-add-material-error'},
        canActivate: [AuthGuardLogOut],
        loadChildren: () => import('./content/pages/add-material/add-material.module').then(mod => mod.AddMaterialModule)
      },
      {
        path: '',
        redirectTo: '/persons',
        pathMatch: 'full'
      },
      {
        path: 'auth/email',
        component: AuthEmailComponent
      },
      // {
      //   path: '404',
      //   component: NotFoundComponent
      // },
      {
        path: '**',
        redirectTo: '/persons',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    canActivate: [LangUaGuard],
    children: [
      {
        path: 'account',
        loadChildren: () => import('./content/pages/account-page/account-page.module').then(mod => mod.AccountPageModule),
        canActivate: [AuthGuardLogOut]
      },
      {
        path: 'donate',
        loadChildren: () => import('./content/pages/donate-page/donate-page.module').then(mod => mod.DonatePageModule)
      },
      {
        path: 'info',
        loadChildren: () => import('./content/pages/info-page/info-page.module').then(mod => mod.InfoPageModule)
      },
      {
        path: 'user-agreement',
        loadChildren: () => import('./content/pages/terms-policy-page/terms-policy-page.module').then(mod => mod.TermsPolicyPageModule),
        data: {url: '/user-agreement'}
      },
      {
        path: 'privacy-policy',
        loadChildren: () => import('./content/pages/terms-policy-page/terms-policy-page.module').then(mod => mod.TermsPolicyPageModule),
        data: {url: '/policy'}
      },
      {
        path: 'person',
        loadChildren: () => import('./content/pages/person-page/person-page.module').then(mod => mod.PersonPageModule)
      },
      {
        path: 'persons',
        loadChildren: () => import('./content/pages/persons-list-page/persons-list-page.module').then(mod => mod.PersonsListPageModule)
      },
      {
        path: 'parties',
        loadChildren: () => import('./content/pages/party-list-page/party-list-page.module').then(mod => mod.PartyListPageModule)
      },
      {
        path: 'laws',
        loadChildren: () => import('./content/pages/laws-page/laws-page.module').then(mod => mod.LawsPageModule)
      },
      {
        path: 'election/vr2019',
        loadChildren: () => import('./content/pages/election-page/election-page.module').then(mod => mod.ElectionPageModule)
      },
      {
        path: 'law',
        loadChildren: () => import('./content/pages/law-page/law-page.module').then(mod => mod.LawPageModule)
      },
      {
        path: 'party',
        loadChildren: () => import('./content/pages/party-page/party-page.module').then(mod => mod.PartyPageModule)
      },
      {
        path: 'materials',
        loadChildren: () => import('./content/pages/materials-page/materials-page.module').then(mod => mod.MaterialsPageModule)
      },
      {
        path: 'material',
        loadChildren: () => import('./content/pages/article/article.module').then(mod => mod.ArticleModule),
        data: {
          isIndexin: true
        }
      },
      {
        path: 'preview',
        loadChildren: () => import('./content/pages/article/article.module').then(mod => mod.ArticleModule),
        data: {
          isIndexin: false
        }
      },
      {
        path: 'create/person',
        // canActivate: [AuthGuard],
        loadChildren: () => import('./content/pages/add-person-page/add-person-page.module').then(mod => mod.AddPersonPageModule)
      },
      {
        path: 'create/material', data: {notif: 'errors.auth-add-material-error'},
        // canActivate: [AuthGuard],
        loadChildren: () => import('./content/pages/add-material/add-material.module').then(mod => mod.AddMaterialModule)
      },
      {
        path: 'edit/material', data: {notif: 'errors.auth-add-material-error'},
        canActivate: [AuthGuardLogOut],
        loadChildren: () => import('./content/pages/add-material/add-material.module').then(mod => mod.AddMaterialModule)
      },
      {
        path: '',
        redirectTo: '/persons',
        pathMatch: 'full'
      },
      {
        path: 'auth/email',
        component: AuthEmailComponent
      },
      // {
      //   path: '404',
      //   component: NotFoundComponent
      // },
      {
        path: '**',
        redirectTo: '/persons',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      initialNavigation: 'enabled',
      onSameUrlNavigation: 'reload',
      preloadingStrategy: PreloadAllModules
      // scrollPositionRestoration: 'enabled'
    })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
