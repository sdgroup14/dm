import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AddPersonPageComponent} from './add-person-page.component';

const routes: Routes = [
  {
    path: '',
    component: AddPersonPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddPersonPageRoutingModule {
}
