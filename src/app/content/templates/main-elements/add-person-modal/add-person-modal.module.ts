import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AddPersonModalComponent} from './add-person-modal.component';
import {BtnCloseModule} from '../../elements/btn-close/btn-close.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SheredTranslateModule} from '../../../../modules/shered-translate.module';
import {LangsWidgetModule} from '../../../pages/add-material/ui/langs-widget/langs-widget.module';
import {SheredTooltipModule} from '../../../../modules/shered-tooltip.module';
import {DatePickerModule} from '../../../pages/add-material/templates/datepicker-block/date-picker.module';
import {SelectSearchModule} from '../../textareas/select-search/select-search.module';
import {SelectWithGroupModule} from '../../textareas/select-with-groups/select-with-group.module';
import {InputPhotoModule} from './components/input-photo/input-photo.module';

@NgModule({
  declarations: [AddPersonModalComponent],
  imports: [
    CommonModule,
    BtnCloseModule,
    FormsModule,
    ReactiveFormsModule,
    SheredTranslateModule,
    LangsWidgetModule,
    SheredTooltipModule,
    DatePickerModule,
    SelectSearchModule,
    SelectWithGroupModule,
    InputPhotoModule
  ],
  exports: [AddPersonModalComponent]
})
export class AddPersonModalModule {
}
