import {Component, NgZone, OnInit, Renderer2} from '@angular/core';
import {take} from 'rxjs/operators';
import {CommonAppService} from '../../../../services/common-app.service';
import {AppAuthService} from '../../../../services/app-auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-contradiction-modal',
  templateUrl: './contradiction-modal.component.html',
  styleUrls: ['./contradiction-modal.component.scss']
})
export class ContradictionModalComponent implements OnInit {
  isOpen = false;
  errors: any = {
    text: null,
    title: null
  };
  disabled = false;
  model: any = {
    id: null,
    text: '',
    title: ''
  };
  role: any = '';


  submit() {
    // console.log(this.model);
    this.service.saveContradiction(this.model.id, this.model.text, this.model.title).pipe(take(1)).subscribe((resp: any) => {
      this.service.notification.next({
        type: 'succsess',
        text: resp.message
      });
      // this.model = {};
      // this.errors = {};
      this.close();
    }, error => {
      const data = error.error.errors;
      for (const _error in data) {
        this.errors[_error] = data[_error][0];
      }
      // this.service.notification.next({
      //   type: 'warning',
      //   text: error.error.message
      // });
      // console.log(this.errors);
    });
    // console.log(this.model);
    // form.submitted = false;
  }

  close() {
    this.isOpen = false;
    setTimeout(() => {
      this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
      this.renderer.removeClass(document.body, 'scrollOff');
    }, 150);
    // this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
    // this.service.feedbackModal.next(false);
    // setTimeout(() => {
    //   this.model = {
    //     text: '',
    //     type: 'proposal'
    //   };
    // }, 250);
  }


  constructor(route: ActivatedRoute, private service: CommonAppService, private renderer: Renderer2, private auth: AppAuthService, private router: Router) {
    route.queryParamMap.subscribe(() => {
      const data: any = this.router.getCurrentNavigation().extras.state;
      // console.log(data);
      this.model.id = data.id;
      this.role = data.role;
      this.disabled = this.role !== 'specialist';
    });
  }

  ngOnInit() {
    setTimeout(() => {
      this.isOpen = true;
      this.renderer.addClass(document.body, 'scrollOff');
    });
    // const token_data = this.auth.currentUserValue;
    // if (token_data) {
    //   // this.disabled = !currentUser ? true : false;
    //   // console.log(token_data)
    //   this.disabled = token_data.user.role !== 'specialist';
    // }
    // this.auth.currentUserSubject.subscribe(data => {
    //   if (data) {
    //     this.zone.run(() => {
    //       this.disabled = false;
    //     });
    //   } else {
    //     this.disabled = true;
    //   }
    // });
    // this.service.feedbackModal.subscribe((bool: boolean) => {
    //   this.isOpen = bool;
    // });

  }

}
