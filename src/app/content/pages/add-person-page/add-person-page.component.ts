import {Component, NgZone, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {forkJoin, of, Subscription} from 'rxjs';
// import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {take} from 'rxjs/operators';
import {MetaService} from '../../../services/meta.service';
import {ActivatedRoute, Router} from '@angular/router';
// import {AddMaterialService} from '../add-material/services/add-material.service';
import {PagePreloaderService} from '../../templates/preloaders/page-preloader/page-preloader.service';
import {Location} from '@angular/common';
import {AppAuthService} from '../../../services/app-auth.service';
import {PlatformService} from '../../../services/platform.service';
import {CommonAppService} from '../../../services/common-app.service';
import {TranslateService} from '@ngx-translate/core';
import {GoogleTranslateService} from '../../../services/google-translate.service';


const saveApiModel = [
  'sirname_ru',
  'sirname_ua',
  'name_ru',
  'name_ua',
  'birthday',
  'workplace_id',
  'position_id',
  'region_id',
  'district_id',
  'city_id',
  'party_id',
  'photo'];

@Component({
  selector: 'app-add-person-page',
  templateUrl: './add-person-page.component.html',
  styleUrls: ['./add-person-page.component.scss']
})
export class AddPersonPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  // blocks: any = [];
  // lang = 'ua';
  // topData: any = {};
  // material_id = null;
  // source_list: any = [{
  //   value: ''
  // }];
  title = '';
  // errors: any = {};
  isAuth = false;

  isOpen = false;
  error = false;
  disabled = false;
  lang = 'ua';
  person: any = {};
  errors: any = {};

  sirname: any;
  // cover: any;
  // administrative_checkbox: any = false;
  // criminal_checkbox: any = false;
  // slug: any;
  // translateHover = false;


  switchLang(lang) {
    this.lang = lang;
  }


  showInfo(_slug: any, block_type?) {
    if (_slug) {
      this.router.navigate([{outlets: {modals: 'info'}}],
        {
          skipLocationChange: true,
          state: {
            slug: _slug,
            isOpen: true,
            icon: 'info',
            title: 'info'
          }
        });
    } else {
      let __slug = '';
      if (block_type === 1) {
        __slug = 'material.publish.h2';
      } else if (block_type === 2) {
        __slug = 'material.publish.text';
      } else if (block_type === 3) {
        __slug = 'material.publish.photo';
      } else if (block_type === 4) {
        __slug = 'material.publish.vid';
      } else if (block_type === 5) {
        __slug = 'material.publish.aud';
      } else if (block_type === 6) {
        __slug = 'material.publish.cite';
      } else if (block_type === 7) {
        __slug = 'material.publish.slider';
      } else if (block_type === 8) {
        __slug = 'material.publish.ytb';
      } else if (block_type === 9) {
        __slug = 'material.publish.tw';
      } else if (block_type === 10) {
        __slug = 'material.publish.fb';
      }
      this.router.navigate([{outlets: {modals: 'info'}}],
        {
          skipLocationChange: true,
          state: {
            slug: __slug,
            isOpen: true,
            icon: 'info',
            title: 'info'
          }
        });
    }

  }


  translateHandler(key) {
    const diff_lang = this.lang === 'ua' ? 'ru' : 'ua';
    if (!this.person[key + '_' + diff_lang] && this.person[key + '_' + this.lang]) {
      return this.google.translate(
        this.person[key + '_' + this.lang],
        this.lang === 'ua' ? 'uk' : 'ru',
        this.lang !== 'ua' ? 'uk' : 'ru'
      );
      //   .subscribe(text => {
      //   const translate_key = this.lang !== 'ua' ? 'ua' : 'ru';
      //   this.person[key + '_' + translate_key] = text;
      // });
    }
    return of('');
  }

  translate() {
    forkJoin([this.translateHandler('sirname').pipe(take(1)), this.translateHandler('name').pipe(take(1))]).pipe(take(1)).subscribe(data => {
      // console.log(data);
      const translate_key = this.lang !== 'ua' ? 'ua' : 'ru';
      if (data[0]) {
        this.person['sirname_' + translate_key] = data[0];
      }
      if (data[1]) {
        this.person['name_' + translate_key] = data[1];
      }

      // console.log(this.person);
    });
  }

  close() {
    if (this.isOpen) {
      this.isOpen = false;
      setTimeout(() => {
        this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
        this.renderer.removeClass(document.body, 'scrollOff');
      }, 150);
    }
  }

  changeDistrict(item) {
    this.person.district_id = item.id;
    this.person.city_id = '';
    this.errors.district_id = null;
    // console.log(this.person.district_id)
    // this.updateList();
  }


  changeRegion(item) {
    this.person.region_id = item.id;
    this.person.district_id = '';
    this.person.city_id = '';
    this.errors.region_id = null;
    // this.updateList();
  }

  submit() {
    // forkJoin([this.translateHandler('sirname').pipe(take(1)), this.translateHandler('name')
    //   .pipe(take(1))]).pipe(take(1)).subscribe(data => {
    //   console.log(data);
    //   const translate_key = this.lang !== 'ua' ? 'ua' : 'ru';
    //   if (data[0]) {
    //     this.person['sirname_' + translate_key] = data[0];
    //   }
    //   if (data[1]) {
    //     this.person['name_' + translate_key] = data[1];
    //   }
    if (this.person.sirname_ru && !this.person.sirname_ua) {
      this.person.sirname_ua = this.person.sirname_ru;
    }
    if (this.person.sirname_ua && !this.person.sirname_ru) {
      this.person.sirname_ru = this.person.sirname_ua;
    }
    if (this.person.name_ru && !this.person.name_ua) {
      this.person.name_ua = this.person.name_ru;
    }
    if (this.person.name_ua && !this.person.name_ru) {
      this.person.name_ru = this.person.name_ua;
    }
    const api_data: any = {};
    for (const key in this.person) {
      const value = this.person[key];
      if (key === 'cover' && typeof value === 'string') {

      } else if (saveApiModel.indexOf(key) >= 0 && value !== undefined && value !== null && value) {
        api_data[key] = value;
        // apiData.append(option, article[key]); // тут подправить, если проверяется несколько объектов
      }
    }
    // console.log(api_data)
    this.service.addPerson(api_data).pipe(take(1)).subscribe((resp: any) => {
      // console.log(resp);
      this.close();
      this.service.newPerson.next(resp.result);
      setTimeout(() => {
        this.person = {};
        this.errors = {};
      }, 200);

      this.app_service.notification.next({
        type: 'succsess',
        text: resp.message
      });
      this.router.navigate(['/persons']);
    }, error => {
      const errors = error.error.errors;
      // console.log(errors);
      for (const _error in errors) {
        this.errors[_error] = errors[_error];
      }
    });
    // console.log(this.person);
    // });
    // console.log(this.person);
    // this.translate();


  }


  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  constructor(
    private appMeta: MetaService,
    private route: ActivatedRoute,
    // private router: Router,
    // private renderer: Renderer2,
    // private service: AddMaterialService,
    private page_spinner: PagePreloaderService,
    private location: Location,
    private auth: AppAuthService,
    private zone: NgZone,
    public platform: PlatformService,
    private app_service: CommonAppService,
    public _translate: TranslateService,
    // public google: GoogleTranslateService,

    private service: CommonAppService, private renderer: Renderer2, private router: Router, public google: GoogleTranslateService
  ) {
  }

  ngOnInit() {
    this.appMeta.setMeta(
      '',
      '',
      '',
      '',
      '',
      true
    );
    const currentUser: any = this.auth.currentUserValue;
    if (currentUser) {
      this.isAuth = true;
    }
    this.auth.currentUserSubject.subscribe(data => {

      if (data) {
        // console.log('123')
        this.zone.run(() => {
          this.isAuth = true;
        });
      } else {
        this.isAuth = false;
      }
    });
    if (this.platform.check()) {
      this.page_spinner.hide();
    }

  }

}
