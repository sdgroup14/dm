import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InputPhotoComponent} from './input-photo.component';
import {SafeUrlPipeModule} from '../../../../../../pipes/safe-url/safe-url-pipe.module';

@NgModule({
  declarations: [InputPhotoComponent],
  imports: [
    CommonModule,
    SafeUrlPipeModule
  ],
  exports: [InputPhotoComponent]
})
export class InputPhotoModule { }
