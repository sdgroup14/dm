import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ArticleService} from '../services/article.service';
import {ActivatedRoute} from '@angular/router';
import {AppAuthService} from '../../../../services/app-auth.service';
import {take} from 'rxjs/operators';
import {PagePreloaderService} from '../../../templates/preloaders/page-preloader/page-preloader.service';
import {PlatformService} from '../../../../services/platform.service';
import {MetaService} from '../../../../services/meta.service';
import {ShareService} from '../../../../services/share.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  author: any = {};
  workplace: any = {};
  person: any = {};
  title: string;
  pub_time: string;
  blocks: any = [];
  tags: any = [];
  similars: any = [];
  sources: any = [];
  articles: any = [];
  material_id = null;
  loadOpition = false;
  opitionMood: any = {};
  opitionVerity: any = {};
  cover: any;

  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  constructor(
    private appMeta: MetaService,
    private share: ShareService,
    private service: ArticleService,
    private page_spinner: PagePreloaderService,
    private platform: PlatformService,
    private route: ActivatedRoute,
    private zone: NgZone,
    private auth: AppAuthService) {
  }

  ngOnInit() {

    this.subscriptions.add(
      this.route.params.subscribe(params => {
        if (params.slug) {
          this.subscriptions.add(
            this.service.getArticle(params.slug).subscribe((_resp: any) => {
              // console.log('crawlers see: ', this.route.snapshot.data.isIndexin);
              if (!this.route.snapshot.data.isIndexin) {
                this.appMeta.setMeta(
                  '',
                  '',
                  '',
                  '',
                  '',
                  true
                );
              }
              // const resp = _resp.result;
              this.material_id = null;
              this.author = {};
              this.person = {};
              this.workplace = {};
              this.title = '';
              this.pub_time = null;
              this.blocks = [];
              this.tags = [];
              this.cover = null;
              this.sources = [];
              this.similars = [];
              this.loadOpition = false;
              this.opitionMood = {};
              this.opitionVerity = {};

              // console.log(_resp);
              const resp = _resp.result;
              this.material_id = resp.id;
              this.author = resp.author;
              this.person = resp.person;
              this.workplace = resp.workplace;
              this.title = resp.title;
              this.pub_time = resp.pub_time;
              this.blocks = resp.blocks;
              this.tags = resp.tags;
              this.cover = resp.cover_md;

              resp.sources.map(source => {
                const link = source.replace('http://', '');
                const link2 = link.replace('https://', '');
                // console.log(link2.length);
                let link_final = link2;

                if (link2.length > 25) {
                  link_final = link2.substring(0, 25) + '...';
                }
                this.sources.push({
                  text: link_final,
                  link: source
                });

              });

              const seo: any = resp.seo;
              // console.log(resp.sources);
              // this.translate.get('election-page.seo').pipe(take(1)).subscribe((seo: any) => {
              this.appMeta.setMeta(
                resp.title + ' | democratium',
                seo.description,
                seo.keywords,
                resp.cover_fb ? resp.cover_fb : seo.img,
                this.share.langPath() + '/material' + '/' + params.slug
              );

              this.subscriptions.add(this.service.getSimilars(params.slug).subscribe((data: any) => {
                this.similars = data.result;
                this.page_spinner.hide();
              }));


              const currentUser: any = this.auth.currentUserValue;

              // if (currentUser) {
              //   this.service.canVoteMood(currentUser.token, this.material_id).pipe(take(1)).subscribe((mood: any) => {
              //     // console.log(mood);
              //     this.loadOpition = true;
              //     this.opitionMood = mood.result;
              //   });
              //   this.service.canVoteVerity(currentUser.token, this.material_id).pipe(take(1)).subscribe((mood: any) => {
              //     // console.log(mood);
              //     this.loadOpition = true;
              //     this.opitionVerity = mood.result;
              //   });
              //
              // }
              if (this.platform.check()) {
                setTimeout(() => {
                  document.body.scrollIntoView();
                });

                this.subscriptions.add(this.service.getArticles(this.material_id, this.person.id).subscribe((data: any) => {
                  this.articles = data.result;
                  // this.page_spinner.hide();
                }));

                this.auth.currentUserSubject.subscribe(data => {
                  // console.log(data);
                  if (data) {
                    // console.log(data)
                    this.zone.run(() => {
                      this.service.canVoteMood(data.token, this.material_id).pipe(take(1)).subscribe((mood: any) => {
                        // console.log(mood);
                        if (!mood.result.can_vote) {
                          this.opitionMood = mood.result;
                        }
                        this.loadOpition = true;
                      });
                      this.service.canVoteVerity(data.token, this.material_id).pipe(take(1)).subscribe((mood: any) => {
                        // console.log(mood);
                        if (!mood.result.can_vote) {
                          this.opitionVerity = mood.result;
                        }
                        this.loadOpition = true;
                      });
                    });
                  } else {
                    // this.canVote = true;
                    this.opitionMood = {
                      can_vote: true
                    };
                    this.loadOpition = true;

                    this.opitionVerity = {
                      can_vote: true
                    };
                    this.loadOpition = true;

                    // this.user = null;
                  }
                });
              }

            })
          );
        }
      })
    );


  }

}
