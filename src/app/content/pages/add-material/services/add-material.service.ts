import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddMaterialService {
  public block = new Subject();
  public slide = new Subject();

  constructor(private  http: HttpClient) {
  }

  saveTop(data) {

    const apiData = new FormData();
    for (const key in data) {
      apiData.append(key, data[key]);
    }
    // const _url = url ? url : '/materials/update/';
    if (data.hasOwnProperty('id')) {
      return this.http.post(environment.GETApiUrl + '/materials/update/' + data.id, apiData, {});
    }
    return this.http.post(environment.GETApiUrl + '/materials', apiData, {});
  }

  publishTop(id) {
    return this.http.post(environment.GETApiUrl + '/materials/publish/' + id, {}, {});
  }


  saveSliderImg(img, block_id, order, caption_ru, caption_ua, slide_id) {
    const data = new FormData();
    if (slide_id) {
      data.append('id', slide_id);
    }
    data.append('block_id', block_id);
    data.append('order', order);
    if (typeof img !== 'string') {
      data.append('img', img);
    }

    data.append('caption_ru', caption_ru);
    data.append('caption_ua', caption_ua);

    return this.http.post(environment.GETApiUrl + '/material-blocks/slide', data, {}).subscribe(resp => {
      this.slide.next({resp, order});
    });
  }


  saveBlock(block, material_id, order) {
    const data = new FormData();

    data.append('material_id', material_id);
    data.append('order', order);
    //   material_id,
    //   order

    if (block.hasOwnProperty('id')) {
      data.append('id', block.id);
    }
    if (block.type === 1) {
      data.append('h2_ru', block.h2_ru);
      data.append('h2_ua', block.h2_ua);
    } else if (block.type === 2) {
      data.append('text_ru', block.text_ru);
      data.append('text_ua', block.text_ua);
    } else if (block.type === 6) {
      data.append('cite_ru', block.cite_ru);
      data.append('cite_ua', block.cite_ua);




      if (block.caption_ru) {
        data.append('caption_ru', block.caption_ru);
      }
      if (!block.caption_ru && block.caption_ua) {
        data.append('caption_ru', block.caption_ua);
      }

      if (block.caption_ua) {
        data.append('caption_ua', block.caption_ua);
      }
      if (!block.caption_ua && block.caption_ru) {
        data.append('caption_ua', block.caption_ru);
      }
    } else if (block.type === 3) {
      if (typeof block.img !== 'string') {
        data.append('img', block.img);
      }
      if (block.caption_ru) {
        data.append('caption_ru', block.caption_ru);
      }
      if (block.caption_ua) {
        data.append('caption_ua', block.caption_ua);
      }
    } else if (block.type === 4) {
      if (typeof block.video !== 'string') {
        data.append('video', block.video);
      }
      if (block.caption_ru) {
        data.append('caption_ru', block.caption_ru);
      }
      if (block.caption_ua) {
        data.append('caption_ua', block.caption_ua);
      }
    } else if (block.type === 5) {
      if (typeof block.audio !== 'string') {
        data.append('audio', block.audio);
      }
      if (block.caption_ru) {
        data.append('caption_ru', block.caption_ru);
      }
      if (block.caption_ua) {
        data.append('caption_ua', block.caption_ua);
      }
    } else if (block.type === 10 || block.type === 8 || block.type === 9) {
      data.append('embed', block.embed);
    }

    // console.log(data);
    return this.http.post(environment.GETApiUrl + '/material-blocks/save/' + block.type, data, {}).subscribe(resp => {
      this.block.next({resp, order});
    }, error => {
      this.block.next({error, order});

    });
  }

  deleteBlock(id) {
    return this.http.delete(environment.GETApiUrl + '/material-blocks/' + id, {});
  }

  deleteCover(id) {
    return this.http.delete(environment.GETApiUrl + '/materials/cover/' + id, {});
  }


  deleteSlide(id) {
    return this.http.delete(environment.GETApiUrl + '/material-blocks/slide/' + id, {});
  }


  getMaterial(id) {
    return this.http.get(environment.GETApiUrl + '/materials/id/' + id, {});
  }
}
