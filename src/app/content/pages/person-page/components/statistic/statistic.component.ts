import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.scss']
})
export class StatisticComponent implements OnInit {
  adoption: any;
  checkInit = false;
  @Input() checkSingle: boolean;

  @Input() set setAdoption(data) {
    if (data && Object.keys(data).length) {
      data.improve.for_percent = Math.floor(data.improve.for_count * 100 / data.improve.count);
      data.improve.against_percent = 100 - data.improve.for_percent;
      data.worse.for_percent = Math.floor(data.worse.for_count * 100 / data.worse.count);
      data.worse.against_percent = 100 - data.worse.for_percent;
      data.necessary.for_percent = Math.floor(data.necessary.for_count * 100 / data.necessary.count);
      data.necessary.against_percent = 100 - data.necessary.for_percent;
      this.adoption = data;
      this.checkInit = true;
      // console.log(data)
    }
  }

  constructor() {
  }

  ngOnInit() {
  }

}
