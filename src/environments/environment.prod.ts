export const environment = {
  production: true,
  GETApiUrl: 'https://res1.democratium.net/api',
  APIUrl: 'https://api1.democratium.net/v1',
  domain: 'https://democratium.net'
};
