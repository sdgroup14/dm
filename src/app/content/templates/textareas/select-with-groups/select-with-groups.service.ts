import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SelectWithGroupsService {
  public data = new Subject();

  constructor(private  http: HttpClient) {
  }

  get(url, str) {
    const query = str ? '&search=' + str : '';
    return this.http.get(environment.APIUrl + url + query).subscribe(resp => {
      this.data.next(resp);
    });
  }
}
