import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-influence',
  templateUrl: './influence.component.html',
  styleUrls: ['./influence.component.scss']
})
export class InfluenceComponent implements OnInit {
  parties: any = [];

  @Input() set setInfluence(data) {
    if (data.length) {
      this.parties = data;
      // console.log(data);
    }
  }

  constructor() {
  }

  ngOnInit() {
  }

}
