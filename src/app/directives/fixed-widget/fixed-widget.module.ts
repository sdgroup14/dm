import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FixedWidgetDirective} from './fixed-widget.directive';

@NgModule({
  declarations: [FixedWidgetDirective],
  imports: [
    CommonModule
  ],
  exports: [FixedWidgetDirective]
})
export class FixedWidgetModule {
}
