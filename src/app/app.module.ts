import {NgModule} from '@angular/core';
import {BrowserModule, TransferState} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {HeaderComponent} from './content/templates/main-elements/header/header.component';
import {FooterComponent} from './content/templates/main-elements/footer/footer.component';
import {MainMenuModalComponent} from './content/templates/main-elements/main-menu-modal/main-menu-modal.component';
import {AuthModalComponent} from './content/templates/main-elements/auth-modal/auth-modal.component';
import {CommonAppService} from './services/common-app.service';
import {InfoModalComponent} from './content/templates/main-elements/info-modal/info-modal.component';
import {AppNotificationsComponent} from './content/templates/main-elements/app-notifications/app-notifications.component';
import {ShearedModule} from './modules/sheared.module';
import {PlatformService} from './services/platform.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserCookiesModule} from '@ngx-utils/cookies/browser';
import {LangService} from './services/lang.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {JwtInterceptor} from './interceptors/http.interceptor';
// import {AngularFireModule} from '@angular/fire';
// import {AngularFireAuthModule} from '@angular/fire/auth';
import {AuthEmailComponent} from './content/pages/auth-email-page/auth-email.component';
import {TransferHttpCacheModule} from '@nguniversal/common';
import {TranslateBrowserLoader} from './services/translate-browser-loader.service';
import {CookieService} from './services/cookies-service.service';
import {BtnCloseModule} from './content/templates/elements/btn-close/btn-close.module';
import {AuthPageComponent} from './content/pages/auth-page/auth-page.component';
import {FeedbackModalModule} from './content/templates/main-elements/feedback-modal/feedback-modal.module';
import {SafeHtmlPipeModule} from './pipes/safe-html/safe-html-pipe.module';
import {CheckboxModule} from './content/templates/elements/checkbox/checkbox.module';
import {AddPersonModalModule} from './content/templates/main-elements/add-person-modal/add-person-modal.module';
import {PostLayoutPreloaderComponent} from './content/templates/preloaders/post-layout-preloader/post-layout-preloader.component';
import {VotesModalComponent} from './content/pages/law-page/templates/votes-modal/votes-modal.component';
import {ContradictionModalModule} from './content/templates/main-elements/contradiction-modal/contradiction-modal.module';
import {PagePreloaderModule} from './content/templates/preloaders/page-preloader/page-preloader.module';
import {ClickOutsideModule} from './directives/click-outside/click-outside.module';
import {SheredTooltipModule} from './modules/shered-tooltip.module';
import {EmpyListStringModule} from './content/templates/elements/empy-list-string/empy-list-string.module';
import {StoreModule} from '@ngrx/store';
import {PersonsReducer} from './content/pages/persons-list-page/redux/persons-list/persons-list.reducer';
import {environment} from '../environments/environment.prod';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
// import {EffectsModule} from '@ngrx/effects';
// import {PersonsEffects} from './content/pages/persons-list-page/redux/persons-list.effects';
// import {AdsenseModule} from 'ng2-adsense';

export function exportTranslateStaticLoader(http: HttpClient, transferState: TransferState) {
  return new TranslateBrowserLoader('/assets/i18n/', '.json', transferState, http);
}

// const _config = {
//   apiKey: 'AIzaSyC2nA51RB1TQ6WKe0lKM3I7DcQ7FurNc4c',
//   authDomain: 'democratium-903e5.firebaseapp.com',
//   databaseURL: 'https://democratium-903e5.firebaseio.com',
//   projectId: 'democratium-903e5',
//   storageBucket: 'democratium-903e5.appspot.com',
//   messagingSenderId: '539687069634',
//   appId: '1:539687069634:web:5f6e0ce5b4db901b'
// };

@NgModule({
  imports: [
    BrowserModule.withServerTransition({appId: 'dm'}),
    TransferHttpCacheModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ShearedModule,
    FeedbackModalModule,
    ContradictionModalModule,
    BtnCloseModule,
    ClickOutsideModule,
    SafeHtmlPipeModule,
    BrowserAnimationsModule,
    CheckboxModule,
    PagePreloaderModule,
    AddPersonModalModule,
    SheredTooltipModule,
    EmpyListStringModule,
    BrowserCookiesModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: exportTranslateStaticLoader,
        deps: [HttpClient, TransferState]
      }
    }),
    StoreModule.forRoot({personsPage: PersonsReducer}),
    // EffectsModule.forRoot([PersonsEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    // AngularFireModule.initializeApp(_config),
    // AngularFireAuthModule
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainMenuModalComponent,
    AuthModalComponent,
    InfoModalComponent,
    AppNotificationsComponent,
    AuthEmailComponent,
    AuthPageComponent,
    PostLayoutPreloaderComponent,
    VotesModalComponent
  ],
  providers: [
    CommonAppService,
    PlatformService,
    LangService,
    CookieService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
  }
}
