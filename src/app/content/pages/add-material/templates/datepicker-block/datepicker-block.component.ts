import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-datepicker-block',
  templateUrl: './datepicker-block.component.html',
  styleUrls: ['./datepicker-block.component.scss'],
})
export class DatepickerBlockComponent implements OnInit {
  date: any;

  @Input() set setDate(timestamp) {
    if (timestamp) {
      this.date = new Date(timestamp * 1000);
      // console.log(this.date);
    }
  }

  @Output() newDate = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

}
