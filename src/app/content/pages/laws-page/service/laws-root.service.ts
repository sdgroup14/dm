import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LawsRootService {
  articles = new Subject();

  // widgetAttracted = new Subject();

  constructor(private  http: HttpClient) {
  }

  // getMainStats() {
  //   return this.http.get(environment.APIUrl + '/laws/main-stats', {});
  // }


  getListNext(params) {
    // let urlParams = '?lang=ru&is_truth=' + params.is_truth;
    let urlParams = '?';
    urlParams += params.page ? '&page=' + params.page : '';
    urlParams += params.year ? '&year=' + params.year : '';
    urlParams += params.is_improve ? '&is_improve=' + params.is_improve : '';
    urlParams += params.workplace_id ? '&workplace_id=' + params.workplace_id : '';
    urlParams += params.region_id ? '&region_id=' + params.region_id : '';
    urlParams += params.city_id ? '&city_id=' + params.city_id : '';
    urlParams += params.district_id ? '&district_id=' + params.district_id : '';
    urlParams += params.workplace_ext_id ? '&workplace_ext_id=' + params.workplace_ext_id : '';
    urlParams += params.law_workplace_id ? '&law_workplace_id=' + params.law_workplace_id : '';
    urlParams += params.search !== '' ? '&search=' + params.search : '';
    return this.http.get(environment.APIUrl + '/laws/list' + urlParams, {}).subscribe(resp => {
      this.articles.next(resp);
    });
  }

  getList(params) {
    // let urlParams = '?lang=ru&is_truth=' + params.is_truth;
    let urlParams = '?';
    urlParams += params.page ? '&page=' + params.page : '';
    // urlParams += params.workplace_id ? '&workplace_id=' + params.workplace_id : '';
    // urlParams += params.region_id ? '&region_id=' + params.region_id : '';
    // urlParams += params.city_id ? '&city_id=' + params.city_id : '';
    urlParams += params.search !== '' ? '&search=' + params.search : '';
    return this.http.get(environment.APIUrl + '/laws/list' + urlParams, {});
  }

  getOpitionWidget(ext) {
    return this.http.get(environment.APIUrl + '/laws/stat-improve?workplace_ext_id=' + ext, {});
  }

  getRatioWidget(ext) {
    return this.http.get(environment.APIUrl + '/laws/stat-gov-user-overlap?workplace_ext_id=' + ext, {});
  }



}
