import {Component, OnDestroy, OnInit} from '@angular/core';
import {InfoPageService} from './service/info-page.service';
import {Subscription} from 'rxjs';
import {PagePreloaderService} from '../../templates/preloaders/page-preloader/page-preloader.service';
import {MetaService} from '../../../services/meta.service';
import {ShareService} from '../../../services/share.service';
import {take} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-info-page',
  templateUrl: './info-page.component.html',
  styleUrls: ['./info-page.component.scss']
})
export class InfoPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  sections: any = [];

  constructor(
    private appMeta: MetaService,
    private share: ShareService,
    private service: InfoPageService,
    private translate: TranslateService,
    private page_spinner: PagePreloaderService) {
  }

  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.subscriptions.add(this.service.get().subscribe((resp: any) => {

      this.translate.get('info-page').pipe(take(1)).subscribe((data: any) => {
        const seo = data.seo;
        this.appMeta.setMeta(
          data.title + ' | democratium',
          seo.description,
          seo.keywords,
          seo.img,
          this.share.langPath() + '/info'
        );
      });
      // this.appMeta.setMeta(
      //   resp.title + ' | democratium',
      //   seo.description,
      //   seo.keywords,
      //   resp.cover_fb ? resp.cover_fb : seo.img,
      //   this.share.langPath() + '/material' + '/' + params.slug
      // );
      // this.sections = resp.result;
      // console.log(resp);
      this.sections = resp.result;
      this.sections[0].selected = true;
      this.page_spinner.hide();
      // this.subscriptions.add(this.service.getFile(resp.result.file).subscribe(res => {
      //   // console.log(res);

      // }));
    }));
  }

}
