import {AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {PlatformService} from '../../../../../services/platform.service';

@Component({
  selector: 'app-law-opition-widget',
  templateUrl: './law-opition-widget.component.html',
  styleUrls: ['./law-opition-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LawOpitionWidgetComponent implements OnInit, AfterViewInit {
  @ViewChild('canvas', {static: false}) canvas: ElementRef;
  @ViewChild('contentEl', {static: false}) contentEl: ElementRef;
  @ViewChild('row1', {static: false}) row1: ElementRef;
  @ViewChild('row2', {static: false}) row2: ElementRef;
  @ViewChild('row3', {static: false}) row3: ElementRef;
  ctx: any;
  empty = true;
  adoption: any = {};
  init = false;

  @Input() set changeAdoption(data) {
    if (data.data) {
      this.empty = false;
      if (Object.keys(data).length) {
        // console.log(data)

        data.improve_stat.for_percent = Math.floor(data.improve_stat.for_count * 100 / data.improve_stat.votes);
        data.improve_stat.against_percent = 100 - data.improve_stat.for_percent;
        data.worse_stat.for_percent = Math.floor(data.worse_stat.for_count * 100 / data.worse_stat.votes);
        data.worse_stat.against_percent = 100 - data.worse_stat.for_percent;
        data.necessary_stat.for_percent = Math.floor(data.necessary_stat.for_count * 100 / data.necessary_stat.votes);
        data.necessary_stat.against_percent = 100 - data.necessary_stat.for_percent;
        // this.adoption = data.adoption;

        this.adoption = data;
        this.init = true;
        if (this.platform.check()) {
          setTimeout(() => {
            const startX = 65;
            const endX = 76;

            const getStartYPos = (row) => {
              const icon = row.getElementsByClassName('icon')[0];
              return icon.offsetTop + icon.offsetHeight;
            };


            this.ctx = (<HTMLCanvasElement>this.canvas.nativeElement).getContext('2d');

            this.canvas.nativeElement.width = this.contentEl.nativeElement.offsetWidth;
            this.canvas.nativeElement.height = this.contentEl.nativeElement.offsetHeight;

            if (data.laws_improve) {
              const el1 = this.row1.nativeElement;
              const startY1 = getStartYPos(el1) + 3;
              const endY1 = startY1 + 10;
              this.drawCurve(startX, startY1, endX, endY1, .2);
            }

            if (data.laws_worse) {
              const el2 = this.row2.nativeElement;
              const startY2 = getStartYPos(el2) + 3;
              const endY2 = startY2 + 10;
              this.drawCurve(startX, startY2, endX, endY2, .2);
            }

            if (data.laws_necessary) {
              const el3 = this.row3.nativeElement;
              const startY3 = getStartYPos(el3) + 3;
              const endY3 = startY3 + 10;
              this.drawCurve(startX, startY3, endX, endY3, .2);
            }
          }, 100);
        }
      }
    } else {
      this.empty = true;
    }
  }

  constructor(public platform: PlatformService) {
  }

  drawCurve(startX, startY, endX, endY, angle) {
    const dx = endX - startX;
    const delta = 1;
    this.ctx.strokeStyle = '#C8D0D2';
    this.ctx.lineWidth = 1;
    this.ctx.beginPath();
    this.ctx.moveTo(startX, startY);
    this.ctx.bezierCurveTo(startX, startY, endX - dx * delta, endY, endX, endY);
    this.ctx.stroke();
    const startXPos = endX + 20;
    const startYPos = endY - 5;
    const endXPos = endX;
    const endYPos = endY;
    const canvas_arrow = (fromx, fromy, tox, toy) => {
      const headlen = 6;
      this.ctx.lineTo(tox, toy);
      this.ctx.lineTo(tox - headlen * Math.cos(angle - Math.PI / 5), toy - headlen * Math.sin(angle - Math.PI / 5));
      this.ctx.moveTo(tox, toy);
      this.ctx.lineTo(tox - headlen * Math.cos(angle + Math.PI / 5), toy - headlen * Math.sin(angle + Math.PI / 5));
    };
    this.ctx.beginPath();
    canvas_arrow(startXPos, startYPos, endXPos, endYPos);
    this.ctx.strokeStyle = '#C8D0D2';
    this.ctx.lineWidth = 1;
    this.ctx.stroke();
  }

  ngAfterViewInit() {
  }

  ngOnInit() {
  }

}
