import {Component, OnInit} from '@angular/core';
import {PagePreloaderService} from './page-preloader.service';

@Component({
  selector: 'app-page-preloader',
  templateUrl: './page-preloader.component.html',
  styleUrls: ['./page-preloader.component.scss']
})
export class PagePreloaderComponent implements OnInit {
  isVisible: any = false;

  constructor(private service: PagePreloaderService) {
  }

  ngOnInit() {
    this.service.visibility.subscribe(bool => {
      this.isVisible = bool;
    });
    // this.__common.preloader.subscribe((bool: any) => {
    //   this.isPreloader = bool;
    // });
  }

}
