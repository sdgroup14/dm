import {Person} from '../../models/person.model';


export default class PersonsState {
  Persons: Person[];
}

export const initializeState = () => {
  return {Persons: Array<Person>()};
};
