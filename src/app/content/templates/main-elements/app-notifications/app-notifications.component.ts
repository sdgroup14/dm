import {Component, NgZone, OnInit} from '@angular/core';
import {CommonAppService} from '../../../../services/common-app.service';

@Component({
  selector: 'app-app-notifications',
  templateUrl: './app-notifications.component.html',
  styleUrls: ['./app-notifications.component.scss']
})
export class AppNotificationsComponent implements OnInit {
  count = 4;
  isOpen = false;
  type = 'text';
  content = '';
  interval: any;

  constructor(
    public service: CommonAppService,
    private zone: NgZone
  ) {
  }

  close() {
    clearInterval(this.interval);
    this.interval = null;
    this.isOpen = false;
    setTimeout(() => {
      this.content = '';
      this.type = 'text';
    }, 500);
  }

  startInterval() {
    if (!this.interval) {
      this.interval = setInterval(() => {
        if (this.count === 1) {
          this.close();
          return;
        }
        this.count--;
      }, 1000);
    }
  }

  ngOnInit() {
    this.service.notification.subscribe((data: any) => {
      if (data) {
        this.zone.run(() => {
          this.type = data.type;
          this.content = data.text;
          this.isOpen = true;
          this.count = 4;
          this.startInterval();
        });
      }
    });

  }

}
