import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  files = [];
  @Output() newFiles = new EventEmitter();
  @Output() save = new EventEmitter();
  @Output() cancel = new EventEmitter();
  error = '';
  text = '';


  saveHandler() {
    this.save.emit(this.text);
  }

  reset() {
    this.cancel.emit(true);
  }


  getFiles(e) {
    this.error = null;
    if (e.target.files.length > 3) {
      this.translate.get('account-page.roles-tab.form.length-file-error').pipe(take(1)).subscribe(content => {
        this.error = content;
      });
      return;
    }
    this.files = e.target.files;
    this.newFiles.emit(this.files);
  }

  constructor(private translate: TranslateService) {
  }

  ngOnInit() {
  }

}
