import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {SelectWithGroupsService} from './select-with-groups.service';
import {take} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-select-with-groups',
  templateUrl: './select-with-groups.component.html',
  styleUrls: ['./select-with-groups.component.scss']
})
export class SelectWithGroupsComponent implements OnInit {
  active = false;
  choiceTitle = '';
  region_id: any;
  @Input() url: string;
  @Input() placeholder: string;
  chainKeys: any = {};
  serach = '';
  states: any = [];
  allBtnText = '';
  @Input() direction: string;

  @Input() set setValue(data) {
    if (data && data.hasOwnProperty('title')) {
      // console.log(data);
      setTimeout(() => {
        this.choiceTitle = data.title;
        this.change.emit({
          id: data.id
          // region_id: data.state.id
        });
      }, 1);
    }
  }

  @Input() set reset(params) {
    // console.log(params);
    for (const key in params)
      // this.dependenceKeys.map(key => {
      if (params[key] !== this.chainKeys[key]) {
        this.chainKeys[key] = params[key];
        this.choiceTitle = '';
        this.states = [];
      }
    // });
  }

  close() {
    if (this.active) {
      this.active = false;
    }
    // console.log('123')
  }

  // ngOnChanges(changes: SimpleChanges) {
  //   this.resetState(changes.reset.currentValue);
  //
  // }
  //
  // resetState(params) {
  //   // console.log(params);
  //   for (const key in params)
  //     // this.dependenceKeys.map(key => {
  //     if (params[key] !== this.chainKeys[key]) {
  //       this.chainKeys[key] = params[key];
  //       this.choiceTitle = '';
  //       this.states = [];
  //     }
  //   // });
  // }

  @Output() change = new EventEmitter();

  toggleState(state) {
    state.isOpen = !state.isOpen;
    // this.states.map(s => {
    //   if (state.title !== s.title) {
    //     s.isOpen = false;
    //   }
    // });
  }

  apply(city, group) {
    this.choiceTitle = city.title;
    this.active = false;
    this.serach = '';
    this.change.emit({
      id: city.id,
      region_id: group.id
    });
  }

  switchInputs(search) {
    this.states = [];
    this.active = true;
    setTimeout(() => {
      search.focus();
    }, 1);
    this.service.get(this.url, this.serach);
  }

  searchApi() {
    // this.active = true;
    // this.service.get(this.url, this.serach).pipe(take(1)).subscribe((resp: any) => {
    //   if (resp.result && resp.result.hasOwnProperty('data')) {
    //
    //     this.data = resp.result.data;
    //     return;
    //   }
    //   this.data = resp.result;
    // });
  }

  constructor(public service: SelectWithGroupsService, private translate: TranslateService) {
  }

  ngOnInit() {
    this.translate.get('input-select.list-all-btn').pipe(take(1)).subscribe(content => {
      this.allBtnText = content;
    });
    this.service.data.subscribe((resp: any) => {
      this.states = resp.result;
      if (this.serach !== '' && resp.result) {
        this.states.map(state => state.isOpen = true);
      }
    });
  }

}
