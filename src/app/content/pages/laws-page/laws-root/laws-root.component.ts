import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {LawsRootService} from '../service/laws-root.service';
import {DateService} from '../../../../services/date.service';
import {PagePreloaderService} from '../../../templates/preloaders/page-preloader/page-preloader.service';
import {PlatformService} from '../../../../services/platform.service';
import {MetaService} from '../../../../services/meta.service';
import {ShareService} from '../../../../services/share.service';

@Component({
  selector: 'app-materials-root',
  templateUrl: './laws-root.component.html',
  styleUrls: ['./laws-root.component.scss']
})
export class LawsRootComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  @ViewChild('topEl', {static: true}) topEl: ElementRef;
  @ViewChild('searchEl', {static: false}) searchEl: ElementRef;
  filtersToggle = false;
  searchValue = '';
  workplace: any;
  showFilters = false;
  extend: any = [];
  opitionWidget: any = {};
  ratioWidget: any = {};
  lastPage = 1;
  isStatisticWidget = false;
  lastWorkPlaceExtId: any = null;
  selectBoxYears: any = [
    {
      type: '2014',
      title: '2014',
      selected: true
    },
    {
      type: '2015',
      title: '2015',
      selected: false
    },
    {
      type: '2016',
      title: '2016',
      selected: false
    },
    {
      type: '2017',
      title: '2017',
      selected: false
    },
    {
      type: '2018',
      title: '2018',
      selected: false
    },
    {
      type: '2019',
      title: '2019',
      selected: false
    }
  ];


  filterParams: any = {
    is_improve: '',
    year: '',
    page: 1,
    law_workplace_id: '',
    workplace_id: '4',
    workplace_ext_id: '',
    region_id: '',
    city_id: '',
    search: ''
  };
  isRegion = false;
  init = false;
  isCity = false;
  isDistrict = false;
  isExt = true;
  // isRegion = false;
  articles: any = [];
  // is_city: false
  // is_def: true
  // is_district: false
  // is_ext: true
  // is_region: false
  changeWorkplace(item) {
    this.isRegion = false;
    this.isCity = false;
    this.isDistrict = false;
    this.isExt = false;
    this.lastPage = this.filterParams.page = 1;
    setTimeout(() => {
      // console.log(item);
      this.isRegion = item.is_region;
      this.isCity = item.is_city;
      this.isDistrict = item.is_district;
      this.isExt = item.is_ext;
      this.filterParams.workplace_id = item.id;
      this.filterParams.city_id = '';
      this.filterParams.region_id = '';
      this.filterParams.district_id = '';
      this.filterParams.workplace_ext_id = '';
      this.updateList();
    }, 1);
    // this.extend = [];
    // this.filterParams.city_id = '';
    // this.filterParams.law_workplace_id = '';
    // this.filterParams.year = '';
    // this.filterParams.workplace_id = item.id;
    // this.changeYears(2014, 2019);
    // if (item.extend.length) {
    //   this.extend = item.extend;
    // } else {
    //
    // }
  }

  changeRegion(item) {
    this.filterParams.region_id = item.id;
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  changeDistrict(item) {
    this.filterParams.district_id = item.id;
    this.filterParams.city_id = '';
    // filterParams.district_id = $event.id; updateList()
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  changeCity(item) {
    this.filterParams.city_id = item.id;
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }


  changeExt(item) {
    this.lastWorkPlaceExtId = this.filterParams.workplace_ext_id;
    this.filterParams.workplace_ext_id = item.id;
    this.lastPage = this.filterParams.page = 1;
    // console.log('123', item);
    this.updateList();
  }

  changeYears(from: any, to) {
    const _to = to ? to : new Date().getFullYear();
    const diff = _to - from;
    this.selectBoxYears = [];
    for (let i = 0; i <= diff; i++) {
      this.selectBoxYears.push({
        type: +from + i,
        title: +from + i,
        selected: false
      });
    }
  }

  changeExtends(item) {
    this.filterParams.law_workplace_id = item.id;
    this.changeYears(item.from, item.to);
    this.filterParams.year = '';
    // this.extend = [];
    // this.filterParams.workplace_id = item.id;
    // if (item.hasOwnProperty('extend')) {
    //   this.extend = item.extend;
    // }
    this.updateList();
  }


  clearSearch() {
    this.searchValue = this.filterParams.search = '';
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  changePageHandler(page) {
    this.filterParams.page = page;
    this.updateList();
  }

  switchFilters() {
    this.showFilters = true;
    this.filtersToggle = !this.filtersToggle;
    if (this.filtersToggle) {
      setTimeout(() => {
        this.searchEl.nativeElement.focus();
      });
    }
  }

  searchHandler() {
    this.filtersToggle = false;
    this.searchValue = this.filterParams.search;
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  changePageArrowHandler(value) {
    this.filterParams.page += value;
    this.updateList();
    // this.lastWorkPlaceExtId = this.filterParams.workplace_ext_id;
    // this.subscriptions.add(this.service.getOpitionWidget(this.filterParams.workplace_ext_id).subscribe((res: any) => {
    //   // console.log(res);
    //   this.opitionWidget = res.result;
    // }));
    // this.subscriptions.add(this.service.getRatioWidget(this.filterParams.workplace_ext_id).subscribe((res: any) => {
    //   // console.log(res);
    //   this.ratioWidget = res.result;
    // }));
  }

  updateList() {
    this.service.getListNext(this.filterParams);
  }


  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  constructor(
    private appMeta: MetaService,
    private share: ShareService,
    private platform: PlatformService,
    private service: LawsRootService,
    public dateService: DateService,
    private page_spinner: PagePreloaderService) {
  }

  ngOnInit() {
    // this.service.getListNext(this.filterParams);
    this.subscriptions.add(this.service.getList(this.filterParams).subscribe((resp: any) => {
      this.filterParams.itemsPerPage = resp.result.per_page;
      this.filterParams.total = resp.result.total;
      this.filterParams.lastPage = resp.result.last_page;
      this.articles = resp.result.list;
      // console.log(this.articles);
      this.page_spinner.hide();
      const seo: any = resp.result.seo;
      // console.log(seo);
      // this.translate.get('election-page.seo').pipe(take(1)).subscribe((seo: any) => {
      this.appMeta.setMeta(
        seo.title,
        seo.description,
        seo.keywords,
        seo.img,
        this.share.langPath() + '/laws'
      );

      // if (this.platform.check()) {
      //   // if (this.filterParams.workplace_ext_id !== '') {
      //     this.subscriptions.add(this.service.getOpitionWidget(this.filterParams.workplace_ext_id).subscribe((res: any) => {
      //       // console.log(res);
      //       this.opitionWidget = res.result;
      //     }));
      //     this.subscriptions.add(this.service.getRatioWidget(this.filterParams.workplace_ext_id).subscribe((res: any) => {
      //       // console.log(res);
      //       this.ratioWidget = res.result;
      //     }));
      //   // }
      // }
    }));
    this.subscriptions.add(this.service.articles.subscribe((resp: any) => {
      this.filterParams.itemsPerPage = resp.result.per_page;
      this.filterParams.total = resp.result.total;
      this.filterParams.lastPage = resp.result.last_page;
      this.articles = resp.result.list;
      if (this.lastPage !== this.filterParams.page) {
        this.lastPage = this.filterParams.page;
        this.topEl.nativeElement.scrollIntoView({behavior: 'smooth', block: 'start'});
      }
      // if(){
      //
      // }
      // console.log(this.lastWorkPlaceExtId)
      // console.log(this.filterParams.workplace_ext_id)
      // if (this.filterParams.workplace_ext_id !== '' && this.lastWorkPlaceExtId !== this.filterParams.workplace_ext_id && this.lastPage !== this.filterParams.page) {
      if (this.init) {
        this.lastWorkPlaceExtId = this.filterParams.workplace_ext_id;
        this.subscriptions.add(this.service.getOpitionWidget(this.filterParams.workplace_ext_id).subscribe((res: any) => {
          // console.log(res);
          this.opitionWidget = res.result;
        }));
        this.subscriptions.add(this.service.getRatioWidget(this.filterParams.workplace_ext_id).subscribe((res: any) => {
          // console.log(res);
          this.ratioWidget = res.result;
        }));
      }
      this.init = true;
      // } else {
      //   this.ratioWidget = {data: false};
      //   this.opitionWidget = {data: false};
      // }

      // console.log(this.articles);
    }));

  }

}
