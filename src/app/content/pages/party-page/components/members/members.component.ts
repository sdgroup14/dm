import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {
  @Input() members: any = [];
  isOpen = false;

  constructor() {
  }

  ngOnInit() {
  }

}
