import {Component, Input, OnInit} from '@angular/core';
import {PlatformService} from '../../../../../services/platform.service';

declare var twttr: any;

@Component({
  selector: 'app-article-twitter-embed',
  templateUrl: './article-twitter-embed.component.html',
  styleUrls: ['./article-twitter-embed.component.scss']
})
export class ArticleTwitterEmbedComponent implements OnInit {
  embed: any;

  @Input() set setContent(content) {
    this.embed = content.embed;
    if (this.platform.check()) {
      setTimeout(() => {
        twttr.widgets.load();
      }, 1);
    }
  }

  constructor(private platform: PlatformService) {
  }

  ngOnInit() {

  }

}
