import {Component, Inject, OnInit, Renderer2} from '@angular/core';
import {LangService} from './services/lang.service';
import {TranslateService} from '@ngx-translate/core';
import {DOCUMENT, Location} from '@angular/common';
import {AppAuthService} from './services/app-auth.service';
import {NavigationEnd, Router} from '@angular/router';
import {PagePreloaderService} from './content/templates/preloaders/page-preloader/page-preloader.service';
import {CommonAppService} from './services/common-app.service';
import {PlatformService} from './services/platform.service';
import {fadeAnimation} from './animations/animations';
// import {take} from 'rxjs/operators';
declare var gtag: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeAnimation]
})

export class AppComponent implements OnInit {
  title = '';
  isMenu = false;
  isAuth = false;
  defaultLang = true;
  lang = 'uk';
  // isLoading = false;
  isPreloader: any = false;

  closeMainMenu() {
    // console.log('deactive')
    setTimeout(() => {
      this.isMenu = false;
      this.renderer.removeClass(document.body, 'scrollOff');
    }, 150);
    // console.log(this.isMenu)
  }

  routeChange() {
    if (this.platform.check()) {
      setTimeout(() => {
        document.body.scrollIntoView();
      }, 100);
      // section
    }
  }

  constructor(
    public langService: LangService,
    private translate: TranslateService,
    private location: Location,
    private renderer: Renderer2,
    public platform: PlatformService,
    private service: PagePreloaderService,
    private __common: CommonAppService,
    private router: Router,
    private auth: AppAuthService,
    @Inject(DOCUMENT) private _document: any) {
    this.translate.setDefaultLang('uk');
    const lang = this.langService.get();
    if (lang) {
      this.defaultLang = lang === 'uk';
      this.translate.use(lang);
      this._document.documentElement.lang = lang;
    } else {
      const routerLang: any = this.location.path().split('/')[1];
      this.defaultLang = routerLang !== 'ru';
      const _lang = routerLang === 'ru' ? 'ru' : 'uk';
      this.translate.use(_lang);
      this._document.documentElement.lang = _lang;
    }
    this.auth.checkExpares();
    if (platform.check()) {
      router.events.subscribe((ev: any) => {
        if (ev instanceof NavigationEnd) {
          // console.log('123', ev.urlAfterRedirects)
          // console.log('send');
          // setTimeout(() => {
          gtag('config', 'UA-143775933-1', {page_path: ev.urlAfterRedirects});
          // }, 2000)
          // // Hide loading indicator
          // if (ev.url === '/account/notifications') {
          //   this.isNotif = 0;
          // } else {
          //   this.service.checkNotif().pipe(take(1)).subscribe((resp: any) => {
          //     // console.log(resp);
          //     this.isNotif = resp.result.count;
          //   });
          // }
        }

      });
    }

  }

  ngOnInit() {

  }
}
