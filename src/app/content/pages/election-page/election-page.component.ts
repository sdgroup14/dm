import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {take} from 'rxjs/operators';
import {ElectionService} from './services/election.service';
import {MetaService} from '../../../services/meta.service';
import {AppAuthService} from '../../../services/app-auth.service';
import {TranslateService} from '@ngx-translate/core';
import {ShareService} from '../../../services/share.service';
import {PlatformService} from '../../../services/platform.service';
import {PagePreloaderService} from '../../templates/preloaders/page-preloader/page-preloader.service';
import {Subscription} from 'rxjs';
// import {forkJoin} from 'rxjs';

@Component({
  selector: 'app-election-page',
  templateUrl: './election-page.component.html',
  styleUrls: ['./election-page.component.scss']
})
export class ElectionPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  parties: any = [];
  partiesWidgetStats: any = [];
  modalData: any = {};
  voteData: any = {};


  openModal(data) {
    this.modalData = data;
    this.modalData.visibility = true;
  }

  closeModal() {
    this.modalData.visibility = true;
    setTimeout(() => {
      this.modalData = {};
    }, 300);
  }

  constructor(
    private service: ElectionService,
    private appMeta: MetaService,
    private translate: TranslateService,
    private page_spinner: PagePreloaderService,
    private share: ShareService,
    public platform: PlatformService,
    public appAuth: AppAuthService,
    public zone: NgZone,
    private auth: AppAuthService
  ) {
  }
  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }
  ngOnInit() {
    this.translate.get('election-page.seo').pipe(take(1)).subscribe((seo: any) => {
      this.appMeta.setMeta(
        seo.title,
        seo.description,
        seo.tags,
        'https://static1.democratium.net/seo_cover/fb_elections_parlament.jpg',
        this.share.langPath() + '/election/vr2019'
      );
    });
    const currentUser: any = this.auth.currentUserValue;
    // if (currentUser) {
    //   this.service.statsOrVoteHandler().pipe(take(1)).subscribe((resp: any) => {
    //     this.voteData = resp.result;
    //     // console.log( resp.result)
    //   });
    //
    // }
    this.service.getWidgetStats().pipe(take(1)).subscribe((resp: any) => {
      // console.log(resp);
      this.partiesWidgetStats = resp.stats;
    });
    // this.service.getWidgetStats().pipe(take(1)).subscribe((resp: any) => {
    //   // this.partiesWidget = resp.result;
    //   console.log(resp.result);
    // });
    // this.service.getParties().pipe(take(1)).subscribe((resp: any) => {
    //   // this.partiesWidget = resp.result;
    //   console.log(resp.result);
    // });


    if (this.platform.check()) {
      this.service.statsOrVoteHandler().pipe(take(1)).subscribe((resp: any) => {
        this.parties = resp.result;
        this.page_spinner.hide();
      });
      // this.service.statsOrVoteHandler().pipe(take(1)).subscribe((resp: any) => {
      //   this.voteData = resp.result;
      //   // console.log( resp.result)
      // });

      // this.appAuth.currentUserSubject.subscribe(data => {
      //   if (data) {
      //     this.zone.run(() => {
      //       this.service.statsOrVoteHandler().pipe(take(1)).subscribe((resp: any) => {
      //         this.voteData = resp.result;
      //       });
      //       // this.service.canVoteHandler().pipe(take(1)).subscribe((resp: any) => {
      //       //   this.voteData = resp.result;
      //       // });
      //       //
      //     });
      //   } else {
      //     this.voteData = {};
      //   }
      // });
    }
  }

}
