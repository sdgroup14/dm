import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-h1-block',
  templateUrl: './h1-block.component.html',
  styleUrls: ['./h1-block.component.scss']
})
export class H1BlockComponent implements OnInit {
  value = '';

  @Input() set changeValue(text) {
    this.value = text;
    this.newValue.emit(text)
  }

  @Output() newValue = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

}
