import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProfileTabComponent} from './tabs/profile-tab/profile-tab.component';
import {RolesTabComponent} from './tabs/roles-tab/roles-tab.component';
import {MaterialsTabComponent} from './tabs/materials-tab/materials-tab.component';
import {NotificationsTabComponent} from './tabs/notifications-tab/notifications-tab.component';
import {AccountPageComponent} from './account-page.component';

const routes: Routes = [
  {
    path: '',
    component: AccountPageComponent,
    children: [
      {
        path: 'profile',
        component: ProfileTabComponent
      },
      {
        path: 'materials',
        component: MaterialsTabComponent
      },
      {
        path: 'notifications',
        component: NotificationsTabComponent
      },
      {
        path: 'roles',
        component: RolesTabComponent
      },
      {
        path: '',
        redirectTo: '/account/profile',
        pathMatch: 'full'
      },
    ]
  },
  // {
  //   path: '',
  //   redirectTo: 'account/profile',
  //   pathMatch: 'full'
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountPageRoutingModule {
}
