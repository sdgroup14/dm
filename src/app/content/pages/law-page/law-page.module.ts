import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LawPageRoutingModule } from './law-page-routing.module';
import {LawRootComponent} from './law-root/law-root.component';
import {RightWidgetComponent} from './templates/right-widget/right-widget.component';
import {FinancialHelpModule} from '../../templates/widgets/financial-help/financial-help.module';
import {RightWidgetToolbarModule} from '../../templates/widgets/right-widget-toolbar/right-widget-toolbar.module';
import {FixedSocialModule} from '../../templates/widgets/fixed-social/fixed-social.module';
import {PopularOpinionWidgetComponent} from './templates/popular-opinion-widget/popular-opinion-widget.component';
import {SheredTranslateModule} from '../../../modules/shered-translate.module';
import {LawListBottomComponent} from './templates/law-list-bottom/law-list-bottom.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {CommentsComponent} from '../../templates/widgets/comments/comments.component';
import {FormsModule} from '@angular/forms';
import {LawOpitionWidgetComponent} from './templates/law-opition-widget/law-opition-widget.component';
import {RatioVotingWidgetComponent} from './templates/ratio-voting-widget/ratio-voting-widget.component';
import {CheckCountPosDirective} from './templates/popular-opinion-widget/check-count-pos.directive';
import {LawRootService} from './services/law-root.service';
import {SafeHtmlPipeModule} from '../../../pipes/safe-html/safe-html-pipe.module';
import {FixedWidgetModule} from '../../../directives/fixed-widget/fixed-widget.module';
import {PageMinHeightModule} from '../../../directives/page-min-height/page-min-height.module';
import {AddArticleBtnModule} from '../../templates/elements/add-article-btn/add-article-btn.module';
import {SheredTooltipModule} from '../../../modules/shered-tooltip.module';
import {MobileShareWidgetModule} from '../../templates/main-elements/mobile-share-widget/mobile-share-widget.module';
import {BtnCloseModule} from '../../templates/elements/btn-close/btn-close.module';
import {EmpyListStringModule} from '../../templates/elements/empy-list-string/empy-list-string.module';
import {AutosizeModule} from 'ngx-autosize';

@NgModule({
  declarations: [
    LawRootComponent,
    RightWidgetComponent,
    PopularOpinionWidgetComponent,
    CheckCountPosDirective,
    LawListBottomComponent,
    CommentsComponent,
    LawOpitionWidgetComponent,
    RatioVotingWidgetComponent
  ],
  imports: [
    CommonModule,
    LawPageRoutingModule,
    FinancialHelpModule,
    RightWidgetToolbarModule,
    FixedSocialModule,
    SheredTranslateModule,
    PerfectScrollbarModule,
    FormsModule,
    SafeHtmlPipeModule,
    FixedWidgetModule,
    PageMinHeightModule,
    AddArticleBtnModule,
    SheredTooltipModule,
    MobileShareWidgetModule,
    BtnCloseModule,
    EmpyListStringModule,
    AutosizeModule
  ],
  providers: [LawRootService]
})
export class LawPageModule { }
