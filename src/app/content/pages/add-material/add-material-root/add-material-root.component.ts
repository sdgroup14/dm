import {Component, HostListener, NgZone, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {AddMaterialService} from '../services/add-material.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Location} from '@angular/common';
import {PlatformService} from '../../../../services/platform.service';
import {GoogleTranslateService} from '../../../../services/google-translate.service';
import {take} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {AppAuthService} from '../../../../services/app-auth.service';
import {CommonAppService} from '../../../../services/common-app.service';
import {PagePreloaderService} from '../../../templates/preloaders/page-preloader/page-preloader.service';
import {MetaService} from '../../../../services/meta.service';

const topDataModel = ['person', 'region', 'district', 'city', 'workplace', 'title_ru', 'title_ua', 'pub_time', 'id', 'cover', 'sources', 'crime'];
const topDataModelApi = ['person_id', 'region_id', 'city_id', 'workplace_id', 'district_id', 'title_ru', 'title_ua', 'pub_time', 'id', 'cover', 'sources', 'crime'];
const textModel = ['caption_', 'cite_', 'text_', 'h2_', 'title_ru', 'title_ua', 'pub_time', 'id'];

@Component({
  selector: 'app-add-material-root',
  templateUrl: './add-material-root.component.html',
  styleUrls: ['./add-material-root.component.scss']
})
export class AddMaterialRootComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  blocks: any = [];
  lang = 'ua';
  topData: any = {};
  material_id = null;
  source_list: any = [{
    value: ''
  }];
  title = '';
  errors: any = {};
  isAuth = false;
  cover: any;
  administrative_checkbox: any = false;
  criminal_checkbox: any = false;
  slug: any;
  translateHover = false;

  @HostListener('document:mouseup', ['$event']) onMouseUp(e) {
    this.renderer.removeClass(document.body, '__mousedwn');
  }

  dragStart() {
    this.renderer.addClass(document.body, '__mousedwn');
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.blocks, event.previousIndex, event.currentIndex);
  }

  deleteBlock(block) {
    // console.log(block);
    if (block.hasOwnProperty('id')) {
      this.service.deleteBlock(block.id).subscribe(resp => {
        const index = this.blocks.indexOf(block);
        this.blocks.splice(index, 1);
      });
    } else {
      const index = this.blocks.indexOf(block);
      this.blocks.splice(index, 1);
    }
  }

  deleteSource(el) {
    if (this.source_list.length > 1) {
      const index = this.source_list.indexOf(el);
      this.source_list.splice(index, 1);
    }
  }


  addSource() {

    if (this.source_list.length < 3) {
      this.source_list.push({
        value: ''
      });
    }
  }

  addBlock(i, item) {
    this.blocks.splice(i + 1, 0, item);
  }

  switchLang(lang) {
    this.lang = lang;
  }

  changeDistrict(item) {
    this.topData.district_id = item.id;
    this.topData.city_id = '';
    this.errors.district_id = null;
    // console.log(this.topData.district_id);
    // setTimeout(() => {
    //   // console.log(this.topData.district_id);
    // }, 1000);
    // this.updateList();
  }

  openModal() {
    setTimeout(() => {
      this.router.navigate([{outlets: {modals: 'add-person'}}], {skipLocationChange: true});
      // this.app_service.addPersonModal.next(true);
    });
  }

  changeRegion(item) {
    this.topData.region_id = item.id;
    this.topData.district_id = '';
    this.topData.city_id = '';
    this.errors.region_id = null;
    // this.updateList();
  }

  final_message = '';

  deleteCover() {
    this.service.deleteCover(this.material_id).subscribe(resp => {

      this.topData.cover = null;
    });
  }

  saveBlock(block, id, order) {
    if (!block.caption_ru && block.caption_ua) {
      block.caption_ru = block.caption_ua;
    }
    if (!block.caption_ua && block.caption_ru) {
      block.caption_ua = block.caption_ru;
    }

    if (!block.h2_ru && block.h2_ua) {
      block.h2_ru = block.h2_ua;
    }
    if (!block.h2_ua && block.h2_ru) {
      block.h2_ua = block.h2_ru;
    }

    if (!block.text_ru && block.text_ua) {
      block.text_ru = block.text_ua;
    }
    if (!block.text_ua && block.text_ru) {
      block.text_ua = block.text_ru;
    }

    if (!block.cite_ru && block.cite_ua) {
      block.cite_ru = block.cite_ua;
    }
    if (!block.cite_ua && block.cite_ru) {
      block.cite_ua = block.cite_ru;
    }

    this.service.saveBlock(block, id, order);
  }

  saveSliderImg(img, id, order, slide) {
    if (!slide.caption_ru && slide.caption_ua) {
      slide.caption_ru = slide.caption_ua;
    }
    if (!slide.caption_ua && slide.caption_ru) {
      slide.caption_ua = slide.caption_ru;
    }

    this.service.saveSliderImg(img, id, order, slide.caption_ru, slide.caption_ua, slide.id);
  }


  publish() {
    this.submit(true);
    // this.submit('materials/publish/');
    // materials/publish/{id}
  }

  submit(check?) {
    this.router.navigate([{outlets: {modals: 'preloader'}}], {skipLocationChange: true});
    const api_data: any = {};

    this.topData.crime = this.administrative_checkbox ? 'admin' : null;
    if (this.criminal_checkbox && !this.administrative_checkbox) {
      this.topData.crime = 'criminal';
    }
    const sources = [];
    this.source_list.map(source => {
      sources.push(source.value);
    });
    this.topData.sources = JSON.stringify(this.source_list);

    // if(this.criminal_checkbox && this){
    //
    // }
    for (const key in this.topData) {
      const value = this.topData[key];
      if (key === 'cover' && typeof value === 'string') {

      } else if (topDataModelApi.indexOf(key) >= 0 && value !== undefined && value !== null && value) {
        api_data[key] = value;
      }
    }
    // this.
    // console.log(api_data);
    this.service.saveTop(api_data).pipe(take(1)).subscribe((resp: any) => {
      // console.log(resp);
      if (check) {
        this.service.publishTop(this.material_id).pipe(take(1)).subscribe((res: any) => {
          // console.log('check');
          this.app_service.notification.next({
            type: 'succsess',
            text: res.message
          });
        });
      }
      this.final_message = resp.message;
      if (resp.success && resp.result) {
        if (!this.material_id) {
          this.material_id = resp.result.id;
          this.slug = resp.result.slug;
          this.topData.id = resp.result.id;
          this.location.go('/edit/material/' + this.material_id);
        }

      }
      if (this.blocks.length) {
        this.saveBlock(this.blocks[0], this.material_id, 0);
      } else {
        if (!check) {
          setTimeout(() => {
            this.app_service.notification.next({
              type: 'succsess',
              text: this.final_message
            });
            // this.app_service.preloader.next(false);
          }, 500);
        }
        this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
        // setTimeout(() => {
        //   this.app_service.notification.next({
        //     type: 'succsess',
        //     text: this.final_message
        //   });
        //   // this.app_service.preloader.next(false);
        //   this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
        // }, 500);
      }
    }, error => {
      const errors = error.error.errors;
      // console.log(errors);
      for (const _error in errors) {
        this.errors[_error] = errors[_error];
      }
      setTimeout(() => {
        this.app_service.notification.next({
          type: 'warning',
          text: error.error.message
        });
        this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
      }, 500);
    });
  }

  showInfo(_slug: any, block_type?) {
    if (_slug) {
      this.router.navigate([{outlets: {modals: 'info'}}],
        {
          skipLocationChange: true,
          state: {
            slug: _slug,
            isOpen: true,
            icon: 'info',
            title: 'info'
          }
        });
    } else {
      let __slug = '';
      if (block_type === 1) {
        __slug = 'material.publish.h2';
      } else if (block_type === 2) {
        __slug = 'material.publish.text';
      } else if (block_type === 3) {
        __slug = 'material.publish.photo';
      } else if (block_type === 4) {
        __slug = 'material.publish.vid';
      } else if (block_type === 5) {
        __slug = 'material.publish.aud';
      } else if (block_type === 6) {
        __slug = 'material.publish.cite';
      } else if (block_type === 7) {
        __slug = 'material.publish.slider';
      } else if (block_type === 8) {
        __slug = 'material.publish.ytb';
      } else if (block_type === 9) {
        __slug = 'material.publish.tw';
      } else if (block_type === 10) {
        __slug = 'material.publish.fb';
      }
      this.router.navigate([{outlets: {modals: 'info'}}],
        {
          skipLocationChange: true,
          state: {
            slug: __slug,
            isOpen: true,
            icon: 'info',
            title: 'info'
          }
        });
    }

  }

  translate() {
    // this.blocks.map()
    // console.log(this.topData)
    // textModel
    const diff_lang = this.lang === 'ua' ? 'ru' : 'ua';
    if (!this.topData['title_' + diff_lang]) {
      this.google.translate(
        this.topData['title_' + this.lang],
        this.lang === 'ua' ? 'uk' : 'ru',
        this.lang !== 'ua' ? 'uk' : 'ru'
      ).pipe(take(1)).subscribe(text => {
        const translate_key = this.lang !== 'ua' ? 'ua' : 'ru';
        this.topData['title_' + translate_key] = text;
      });
    }

    // this.google.translate(
    //   block[key],
    //   this.lang === 'ua' ? 'uk' : 'ru',
    //   this.lang !== 'ua' ? 'uk' : 'ru'
    // ).pipe(take(1)).subscribe(text => {
    //   const translate_key = key.substring(0, key.length - 2) + (this.lang !== 'ua' ? 'ua' : 'ru');
    //   block[translate_key] = text;
    // });

    this.blocks.map(block => {
      for (const key in block) {
        const value = block[key];
        if (key.search(this.lang) !== -1 && value) {
          this.google.translate(
            block[key],
            this.lang === 'ua' ? 'uk' : 'ru',
            this.lang !== 'ua' ? 'uk' : 'ru'
          ).pipe(take(1)).subscribe(text => {
            const translate_key = key.substring(0, key.length - 2) + diff_lang;
            block[translate_key] = text;
          });
        }
      }

      if (block.type === 7) {
        block.slides.map(slide => {

          for (const key in slide) {
            const value = slide[key];
            if (key.search(this.lang) !== -1 && value) {
              this.google.translate(
                slide[key],
                this.lang === 'ua' ? 'uk' : 'ru',
                this.lang !== 'ua' ? 'uk' : 'ru'
              ).pipe(take(1)).subscribe(text => {
                const translate_key = key.substring(0, key.length - 2) + diff_lang;
                slide[translate_key] = text;
              });
            }
          }

        });
      }
    });

  }


  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  constructor(
    private appMeta: MetaService,
    private route: ActivatedRoute,
    private router: Router,
    private renderer: Renderer2,
    private service: AddMaterialService,
    private page_spinner: PagePreloaderService,
    private location: Location,
    private auth: AppAuthService,
    private zone: NgZone,
    public platform: PlatformService,
    private app_service: CommonAppService,
    public _translate: TranslateService,
    public google: GoogleTranslateService
  ) {
  }

  ngOnInit() {
    this.appMeta.setMeta(
      '',
      '',
      '',
      '',
      '',
      true
    );
    const currentUser: any = this.auth.currentUserValue;
    if (currentUser) {
      this.isAuth = true;
    }
    this.auth.currentUserSubject.subscribe(data => {

      if (data) {
        // console.log('123')
        this.zone.run(() => {
          this.isAuth = true;
        });
      } else {
        this.isAuth = false;
      }
    });
    if (this.platform.check()) {
      this.subscriptions.add(
        this.route.params.subscribe(params => {
          const checkRU = this.location.path().split('/')[1];
          this.lang = checkRU === 'ru' ? 'ru' : 'ua';
          if (params.id) {
            this.title = 'edit';
            this.subscriptions.add(this.service.getMaterial(params.id).subscribe((resp: any) => {
              // if (resp) {
              // console.log('123', resp)
              const data = resp.result;
              this.slug = data.slug;
              this.material_id = data.id;
              for (const key in data) {
                const value = data[key];
                if (topDataModel.indexOf(key) >= 0 && value !== undefined) {
                  this.topData[key] = value;
                }
              }
              if (this.topData.crime === 'admin') {
                this.administrative_checkbox = true;
              }
              if (this.topData.crime === 'criminal') {
                this.criminal_checkbox = true;
              }
              if (this.topData.sources.length) {
                this.source_list = [];
                this.topData.sources.map(source => {
                  this.source_list.push({value: source});
                });
              }
              this.page_spinner.hide();
              this.blocks = resp.result.blocks;
            }, error => {
              this.router.navigate(['/create/material']);
            }));
          } else {
            this.title = 'create';
            this._translate.get('add-material-page.blocks').pipe(take(1)).subscribe(data => {
              this.topData.title_ru = data.title_ru;
              this.topData.title_ua = data.title_ua;
            });
            this.page_spinner.hide();
          }
        })
      );
      let last_block_order = 0;
      this.subscriptions.add(
        this.service.block.subscribe((data: any) => {
          const order = last_block_order = data.order;
          const block = this.blocks[order];
          // console.log('123', data);
          if (data.hasOwnProperty('resp')) {
            block.id = data.resp.result.id;
          } else {
            // console.log('123', data);
            setTimeout(() => {
              this.app_service.notification.next({
                type: 'warning',
                text: data.error.error.message
              });
              this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
            }, 300);
            return;
          }
          if (block.type === 7 && block.slides && block.slides.length) {
            const slide = block.slides[0];
            this.saveSliderImg(slide.img, block.id, 0, slide);
          } else {
            if (this.blocks.length > order + 1) {
              this.saveBlock(this.blocks[order + 1], this.material_id, order + 1);
            } else {
              setTimeout(() => {
                this.app_service.notification.next({
                  type: 'succsess',
                  text: this.final_message
                });
                this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
              }, 300);
            }
          }
        })
      );

      this.subscriptions.add(
        this.service.slide.subscribe((data: any) => {
          const order = data.order;
          const block = this.blocks[last_block_order];
          if (data.resp.success && data.resp.result) {
            block.slides[order].id = data.resp.result.id;
            block.slides[order].img = data.resp.result.img;
          }
          if (block.slides.length > order + 1) {
            const slide = block.slides[order + 1];
            this.saveSliderImg(slide.img, block.id, order + 1, slide);
          } else {
            if (this.blocks.length > last_block_order + 1) {
              this.saveBlock(this.blocks[last_block_order + 1], this.material_id, last_block_order + 1);
            } else {
              this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
            }
          }
          // }
        })
      );

      this.subscriptions.add(
        this.app_service.newPerson.subscribe((data: any) => {
          this.topData.person = data;
        })
      );
    }


  }

}
