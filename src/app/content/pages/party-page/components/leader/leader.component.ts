import {Component, Input, OnInit} from '@angular/core';
import {DateService} from '../../../../../services/date.service';

@Component({
  selector: 'app-leader',
  templateUrl: './leader.component.html',
  styleUrls: ['./leader.component.scss']
})
export class LeaderComponent implements OnInit {
  @Input() h: any = {};
  @Input() set head(data) {
    if (data && data.hasOwnProperty('birthday')) {
      const years_old = new Date().getFullYear() - new Date(data.birthday * 1000).getFullYear();
      data.years_old = years_old;
      data.years_old_lastNumber = data.years_old.toString()[1];
      this.h = data;
    }
  }
  constructor(public dateService: DateService) { }

  ngOnInit() {
  }

}
