import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AddMaterialRoutingModule} from './add-material-routing.module';
import {AddMaterialRootComponent} from './add-material-root/add-material-root.component';
import {ShearedModule} from '../../../modules/sheared.module';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {EditorModule} from '@tinymce/tinymce-angular';
import {TextBlockComponent} from './templates/text-block/text-block.component';
import {FormsModule} from '@angular/forms';
import {AddBlockBtnComponent} from './ui/add-block-btn/add-block-btn.component';
import {H2BlockComponent} from './templates/h2-block/h2-block.component';
import {AutosizeModule} from 'ngx-autosize';
import {QuoteBlockComponent} from './templates/quote-block/quote-block.component';
import {EmbedBlockComponent} from './templates/embed-block/embed-block.component';
import {PhotoBlockComponent} from './templates/photo-block/photo-block.component';
import {VideoBlockComponent} from './templates/video-block/video-block.component';
import {AudioBlockComponent} from './templates/audio-block/audio-block.component';
import {GalleryBlockComponent} from './templates/gallery-block/gallery-block.component';
import {SwiperModule} from 'ngx-swiper-wrapper';
import {PersonBlockComponent} from './templates/person-block/person-block.component';
import {H1BlockComponent} from './templates/h1-block/h1-block.component';
import {SelectSearchService} from '../../templates/textareas/select-search/select-search.service';
import {SelectWithGroupsService} from '../../templates/textareas/select-with-groups/select-with-groups.service';
import {AddMaterialService} from './services/add-material.service';
import {SheredTooltipModule} from '../../../modules/shered-tooltip.module';
import {SelectSearchModule} from '../../templates/textareas/select-search/select-search.module';
import {SelectWithGroupModule} from '../../templates/textareas/select-with-groups/select-with-group.module';
import {SafeHtmlPipeModule} from '../../../pipes/safe-html/safe-html-pipe.module';
import {SheredTranslateModule} from '../../../modules/shered-translate.module';
import {FixedSocialModule} from '../../templates/widgets/fixed-social/fixed-social.module';
import {GoogleTranslateService} from '../../../services/google-translate.service';
import {FixedWidgetModule} from '../../../directives/fixed-widget/fixed-widget.module';
import {CheckboxModule} from '../../templates/elements/checkbox/checkbox.module';
import { LangsWidgetModule } from './ui/langs-widget/langs-widget.module';
import {DatePickerModule} from './templates/datepicker-block/date-picker.module';
import {BlockDropdownModule} from './ui/block-dropdown/block-dropdown.module';
import {ClickOutsideModule} from '../../../directives/click-outside/click-outside.module';

@NgModule({
  declarations: [
    AddMaterialRootComponent,
    TextBlockComponent,
    AddBlockBtnComponent,
    H2BlockComponent,
    QuoteBlockComponent,
    EmbedBlockComponent,
    PhotoBlockComponent,
    VideoBlockComponent,
    AudioBlockComponent,
    GalleryBlockComponent,
    PersonBlockComponent,
    H1BlockComponent
  ],
  imports: [
    CommonModule,
    AddMaterialRoutingModule,
    ShearedModule,
    DragDropModule,
    EditorModule,
    FormsModule,
    ClickOutsideModule,
    AutosizeModule,
    SwiperModule,
    SheredTooltipModule,
    SelectSearchModule,
    SelectWithGroupModule,
    SafeHtmlPipeModule,
    SheredTranslateModule,
    FixedSocialModule,
    FixedWidgetModule,
    CheckboxModule,
    LangsWidgetModule,
    DatePickerModule,
    BlockDropdownModule
  ],
  providers: [
    SelectSearchService,
    SelectWithGroupsService,
    AddMaterialService,
    GoogleTranslateService
  ],
})
export class AddMaterialModule {
}
