import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InfoPageRoutingModule } from './info-page-routing.module';
import {InfoPageComponent} from './info-page.component';
import {RightWidgetComponent} from './components/right-widget/right-widget.component';
import {ArticlePhotoComponent} from './components/article-photo/article-photo.component';
import {ArticleFbEmbedComponent} from './components/article-fb-embed/article-fb-embed.component';
import {SafeHtmlPipeModule} from '../../../pipes/safe-html/safe-html-pipe.module';
import {PageMinHeightModule} from '../../../directives/page-min-height/page-min-height.module';
import {FixedWidgetModule} from '../../../directives/fixed-widget/fixed-widget.module';
import {InfoPageService} from './service/info-page.service';
import {SheredTranslateModule} from '../../../modules/shered-translate.module';

@NgModule({
  declarations: [
    InfoPageComponent,
    RightWidgetComponent,
    ArticlePhotoComponent,
    ArticleFbEmbedComponent
  ],
  imports: [
    CommonModule,
    InfoPageRoutingModule,
    SafeHtmlPipeModule,
    PageMinHeightModule,
    FixedWidgetModule,
    SheredTranslateModule
  ],
  providers: [InfoPageService]
})
export class InfoPageModule { }
