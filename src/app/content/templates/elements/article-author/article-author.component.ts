import {Component, Input, OnInit} from '@angular/core';
import {DateService} from '../../../../services/date.service';

@Component({
  selector: 'app-article-author',
  templateUrl: './article-author.component.html',
  styleUrls: ['./article-author.component.scss']
})
export class ArticleAuthorComponent implements OnInit {
  @Input() timestamp?: number;
  @Input() author: any = {};

  constructor(
    public dateService: DateService) {
  }

  ngOnInit() {
  }

}
