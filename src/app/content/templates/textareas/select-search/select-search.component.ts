import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild} from '@angular/core';
import {SelectSearchService} from './select-search.service';
import {take} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-select-search',
  templateUrl: './select-search.component.html',
  styleUrls: ['./select-search.component.scss']
})
export class SelectSearchComponent implements OnInit {
  active = false;
  choiceTitle = '';
  allBtnText = '';
  @Input() url: string;
  @Input() exist: boolean;
  @Input() addBtn: boolean;
  @Input() direction: string;
  @ViewChild('searchEl', {static: false}) searchEl: ElementRef;
  isDefault = true;
  id = 0;
  chainKeys: any = {};

  @Input() set setValue(item) {
    if (item) {
      this.choiceTitle = item.title ? item.title : item.sirname + ' ' + item.name;
      this.valueChange.emit(item);
    }
  }

  @Input() set reset(params) {
    // console.log(params);
    for (const key in params)
      // this.dependenceKeys.map(key => {
      if (params[key] !== this.chainKeys[key]) {
        this.chainKeys[key] = params[key];
        this.choiceTitle = '';
        this.data = [];
      }
    // });
  }


  @Input() set setDefault(defaultText) {
    // console.log(defaultText);
    this.isDefault = true;
    this.choiceTitle = '';
    if (defaultText && this.isDefault) {
      this.service.get(this.url, this.serach).pipe(take(1)).subscribe((resp: any) => {
        if (resp.result && resp.result.hasOwnProperty('data')) {

          this.data = resp.result.data;
          return;
        }
        this.data = resp.result;
        if (this.isDefault) {
          this.isDefault = false;
          // console.log(this.data);
          const array = this.data.filter(it => {
            return it.is_def;
          });
          if (array.length) {
            this.valueChange.emit(array[0]);
            setTimeout(() => {
              this.choiceTitle = array[0][this.key];
            }, 100);
          }
          // console.log(this.choiceTitle);
        }
      });
    }
  }


  @Input() placeholder: string;
  @Input() allBtn: boolean;
  @Input() key: string;
  @Input() listKey: string;
  @Input() icon: boolean;
  @Input() arrow: boolean;
  @Output() valueChange = new EventEmitter();
  @Output() toggleModal = new EventEmitter();
  serach = '';
  data: any = [];

  close() {
    if (this.active) {
      this.active = false;
    }
    // console.log('123')
  }

  searchApi() {
    this.active = true;
    this.service.get(this.url, this.serach).pipe(take(1)).subscribe((resp: any) => {
      if (resp.result && resp.result.hasOwnProperty('data')) {

        this.data = resp.result.data;
        return;
      }
      this.data = resp.result;
    });
  }

  // openModal() {
  //   // console.log('aaaaaaaaaaaa')
  //   // setTimeout(() => {
  //   this.toggleModal.emit(true);
  //   //   console.log('end')
  //   // }, 1000);
  // }

  switchInputs() {
    this.active = true;
    setTimeout(() => {
      this.searchEl.nativeElement.focus();
    }, 1);
    this.service.get(this.url, this.serach).pipe(take(1)).subscribe((resp: any) => {
      if (resp.result && resp.result.hasOwnProperty('data')) {
        this.data = resp.result.data;
        return;
      }
      this.data = resp.result;
    });
  }

  apply(item) {
    this.choiceTitle = item[this.key] ? item[this.key] : item.sirname + ' ' + item.name;
    this.active = false;
    this.serach = '';
    this.valueChange.emit(item);
  }

  constructor(public service: SelectSearchService, private translate: TranslateService) {
  }


  ngOnInit() {
    this.translate.get('input-select.list-all-btn').pipe(take(1)).subscribe(content => {
      this.allBtnText = content;
    });
    // this.add_person_service.getsubject().subscribe((bool: boolean) => {
    //   // console.log('1234444444');
    //   // this.isOpen = bool;
    // });
    // setTimeout(() => {
    //   this.openModal();
    // }, 5000);
  }

}
