import {NgModule} from '@angular/core';

import {ArticleRoutingModule} from './article-routing.module';
import {ArticleTopComponent} from './templates/article-top/article-top.component';
import {ArticlePhotoComponent} from './templates/article-photo/article-photo.component';
import {ArticleQuoteComponent} from './templates/article-quote/article-quote.component';
import {ArticleVideoComponent} from './templates/article-video/article-video.component';
import {ArticleSliderComponent} from './templates/article-slider/article-slider.component';
import {SwiperModule} from 'ngx-swiper-wrapper';
import {ArticleFbEmbedComponent} from './templates/article-fb-embed/article-fb-embed.component';
import {ArticleTwitterEmbedComponent} from './templates/article-twitter-embed/article-twitter-embed.component';
import {RightWidgetComponent} from './templates/right-widget/right-widget.component';
import {ShearedModule} from '../../../modules/sheared.module';
import {ArticleComponent} from './root/article.component';
import {ArticleTagsModule} from '../../templates/elements/article-tags/article-tags.module';
import {MoreArticlesWidgetModule} from '../../templates/widgets/more-articles-widget/more-articles-widget.module';
import {RightWidgetToolbarModule} from '../../templates/widgets/right-widget-toolbar/right-widget-toolbar.module';
import {FinancialHelpModule} from '../../templates/widgets/financial-help/financial-help.module';
import {ArticleService} from './services/article.service';
import {ArticleAudioComponent} from './templates/article-audio/article-audio.component';
import {SafeHtmlPipeModule} from '../../../pipes/safe-html/safe-html-pipe.module';
import {SheredTranslateModule} from '../../../modules/shered-translate.module';
import {FixedSocialModule} from '../../templates/widgets/fixed-social/fixed-social.module';
import {PopularOpinionWidgetModule} from '../../templates/widgets/popular-opinion-widget/popular-opinion-widget.module';
import {PageMinHeightModule} from '../../../directives/page-min-height/page-min-height.module';
import {FixedWidgetModule} from '../../../directives/fixed-widget/fixed-widget.module';
import {ArticleSimilarsModule} from './templates/article-similars/article-similars.module';

@NgModule({
  declarations: [
    ArticleComponent,
    ArticleTopComponent,
    ArticlePhotoComponent,
    ArticleQuoteComponent,
    ArticleVideoComponent,
    ArticleSliderComponent,
    ArticleTwitterEmbedComponent,
    ArticleFbEmbedComponent,
    RightWidgetComponent,
    ArticleAudioComponent

  ],
  imports: [
    ArticleRoutingModule,
    SwiperModule,
    ShearedModule,
    ArticleTagsModule,
    MoreArticlesWidgetModule,
    RightWidgetToolbarModule,
    FinancialHelpModule,
    SafeHtmlPipeModule,
    SheredTranslateModule,
    FixedSocialModule,
    PopularOpinionWidgetModule,
    PageMinHeightModule,
    FixedWidgetModule,
    ArticleSimilarsModule
  ],
  exports: [
    RightWidgetComponent
  ],
  providers: [ArticleService]
})
export class ArticleModule {
}
