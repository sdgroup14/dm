import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PageMinHeightDirective} from './page-min-height.directive';

@NgModule({
  declarations: [PageMinHeightDirective],
  imports: [
    CommonModule
  ],
  exports: [
    PageMinHeightDirective
  ]
})
export class PageMinHeightModule {
}
