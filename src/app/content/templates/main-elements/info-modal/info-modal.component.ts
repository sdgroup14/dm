import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {CommonAppService} from '../../../../services/common-app.service';
import {PlatformService} from '../../../../services/platform.service';
// import {pipe} from 'rxjs';
import {take} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-info-modal',
  templateUrl: './info-modal.component.html',
  styleUrls: ['./info-modal.component.scss']
})
export class InfoModalComponent implements OnInit {
  @ViewChild('iconEl', {static: false}) iconEl: ElementRef;
  isOpen = false;
  blocks = [];
  link: any;
  icon: any = '';
  title: any;
  sub_title: any;
  // @Output() close = new EventEmitter();

  // @Input() set setVisibility(bool) {
  //   this.isOpen = bool;
  //   this.isOpen ? this.renderer.addClass(document.body, 'scrollOff') : this.renderer.removeClass(document.body, 'scrollOff');
  // }

  constructor(private renderer: Renderer2, private route: ActivatedRoute, private router: Router, public appCommon: CommonAppService) {
    this.route.queryParamMap.subscribe(() => {
      const data: any = this.router.getCurrentNavigation().extras.state;
      this.isOpen = true;
      // if (data.isOpen) {
      this.appCommon.getInfo(data.slug).pipe(take(1)).subscribe((resp: any) => {
        // console.log(resp);
        const res = resp.result;
        this.blocks = res.blocks;
        this.link = res.link ? res.link : null;
        this.title = data.title;
        this.sub_title = res.title ? res.title : '';
        this.icon = data.icon;
        // console.log(this.title);

        this.isOpen = data.isOpen;
        setTimeout(() => {
          this.renderer.addClass(this.iconEl.nativeElement, 'svg-' + this.icon);
        }, 1);
        this.isOpen ? this.renderer.addClass(document.body, 'scrollOff') : this.renderer.removeClass(document.body, 'scrollOff');
      });
      // this.isOpen = true;
      // }

      // if (data.items.length) {
      //   this.items = data.items;
      //   this.lawId = data.law_id;
      //   // this.isOpen = true;
      //   setTimeout(() => {
      //     this.isOpen = true;
      //     this.renderer.addClass(document.body, 'scrollOff');
      //   }, 100);
      //
      // }
    });
  }

  close() {
    this.isOpen = false;
    setTimeout(() => {
      this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
      this.renderer.removeClass(document.body, 'scrollOff');
    }, 150);
  }

  ngOnInit() {
    // if (this.platform.check()) {
    //   this.appCommon.infoVisibility.subscribe((data: any) => {
    //     if (data.isOpen) {
    //       this.appCommon.getInfo(data.url).pipe(take(1)).subscribe((resp: any) => {
    //         console.log(resp);
    //         this.blocks = resp.result.blocks;
    //         this.link = resp.result.link;
    //         this.title = data.title;
    //         this.icon = data.icon;
    //         // console.log(this.title);
    //
    //         this.isOpen = data.isOpen;
    //         setTimeout(() => {
    //           this.renderer.addClass(this.iconEl.nativeElement, 'svg-' + this.icon);
    //         }, 1);
    //         this.isOpen ? this.renderer.addClass(document.body, 'scrollOff') : this.renderer.removeClass(document.body, 'scrollOff');
    //       });
    //     } else {
    //       this.isOpen = data.isOpen;
    //       this.isOpen ? this.renderer.addClass(document.body, 'scrollOff') : this.renderer.removeClass(document.body, 'scrollOff');
    //     }
    //   });
    // }


  }

}
