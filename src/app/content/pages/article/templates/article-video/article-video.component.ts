import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-article-video',
  templateUrl: './article-video.component.html',
  styleUrls: ['./article-video.component.scss']
})
export class ArticleVideoComponent implements OnInit {
  @Input() content: any;
  isPlay = false;

  play(video) {
    this.isPlay = !this.isPlay;
    video.controls = true;
    video.play();
  }

  constructor() {
  }

  ngOnInit() {
  }

}
