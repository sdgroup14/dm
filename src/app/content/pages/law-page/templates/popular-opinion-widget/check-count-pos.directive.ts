import {Directive, ElementRef, Input} from '@angular/core';

@Directive({
  selector: '[appCheckCountPos]'
})
export class CheckCountPosDirective {
  @Input() set check(val) {

    if (val) {
      const el = this.element.nativeElement;
      const progressEl = el.parentNode;
      const choiceItemEl = progressEl.getElementsByClassName('choice_item')[0];
      const startMargin = 20;
      setTimeout(() => {
        if (choiceItemEl.offsetWidth > progressEl.offsetWidth) {
          el.style.left = choiceItemEl.offsetWidth + startMargin + 'px';
        } else {
          el.style.marginLeft = startMargin + 'px';
        }
      }, 800);
    }
  }

  constructor(private element: ElementRef) {

  }

}
