import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommonAppService {
  public infoVisibility = new Subject();
  public notification = new Subject();
  public login = new Subject();
  public authModal = new Subject();
  public feedbackModal = new Subject();
  // public addPersonModal = new Subject();
  public mobileMenuModal = new Subject();
  public newPerson = new Subject();
  public preloader = new Subject();

  constructor(private http: HttpClient) {
  }

  authTwitter() {
    return this.http.get(environment.GETApiUrl + '/auth/twitter-oauth-token', {});
  }

  getInfo(slug) {
    return this.http.get(environment.APIUrl + '/block-info?slug=' + slug, {});
  }

  sendFeedback(text, type) {
    return this.http.post(environment.GETApiUrl + '/messages', {text, type});
  }

  saveContradiction(law_id, text, title) {
    return this.http.post(environment.GETApiUrl + '/law/collision-comment', {law_id, text, title});
  }


  addPerson(data) {
    const apiData = new FormData();
    for (const key in data) {
      apiData.append(key, data[key]);
    }
    return this.http.post(environment.GETApiUrl + '/person', apiData, {});
  }


}
