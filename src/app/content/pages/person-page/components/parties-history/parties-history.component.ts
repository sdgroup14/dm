import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-parties-history',
  templateUrl: './parties-history.component.html',
  styleUrls: ['./parties-history.component.scss']
})
export class PartiesHistoryComponent implements OnInit {
  h: any = [];
  init = false;

  @Input() set history(data) {
    if (data.check) {
      data.history.map(item => {

        // item.from = new Date(item.from * 1000).getFullYear();
        item._from = new Date(item.from * 1000).getFullYear();
        // console.log(item)
        // console.log(item.from)
        // console.log(new Date(item.from * 1000).getFullYear())
        item._to = item.to !== '' ? new Date(item.to * 1000).getFullYear() : null;
        item.check_year = item._from !== item._to && item._to;
      });
      this.h = data.history;
      this.init = true;
    }
  }

  constructor() {
  }

  ngOnInit() {
  }

}
