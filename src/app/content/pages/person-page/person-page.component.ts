import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {PersonPageService} from './services/person-page.service';
import {delay, take} from 'rxjs/operators';
import {AppAuthService} from '../../../services/app-auth.service';
import {PagePreloaderService} from '../../templates/preloaders/page-preloader/page-preloader.service';
import {PlatformService} from '../../../services/platform.service';
import {MetaService} from '../../../services/meta.service';
import {ShareService} from '../../../services/share.service';

@Component({
  selector: 'app-person-page',
  templateUrl: './person-page.component.html',
  styleUrls: ['./person-page.component.scss']
})
export class PersonPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  person: any;
  stats: any;
  static: any = {};
  missings: any = {};
  // checkStatistic = false
  history: any = [];
  problems: any = [];
  totalMissings = 0;
  articles: any = [];
  checkHttpArticles = false;
  historyCheck = false;
  statsCheck = false;
  problemsCheck = false;

  opition: any = {};
  loadOpition = false;

  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  constructor(
    private appMeta: MetaService,
    private share: ShareService,
    private service: PersonPageService,
    private route: ActivatedRoute,
    private platform: PlatformService,
    private page_spinner: PagePreloaderService,
    private router: Router,
    private zone: NgZone,
    private auth: AppAuthService) {
  }

  showInfo(_slug) {
    this.router.navigate([{outlets: {modals: 'info'}}],
      {
        skipLocationChange: true,
        state: {
          slug: _slug,
          isOpen: true,
          icon: 'info',
          title: 'info'
        }
      });
  }

  ngOnInit() {
    this.subscriptions.add(
      this.route.params.subscribe(params => {
        if (params.slug) {
          this.subscriptions.add(
            this.service.getPerson(params.slug).subscribe((_resp: any) => {
              const resp = _resp.result;
              this.person = resp;

              const seo: any = resp.seo;
              // console.log(seo);
              // this.translate.get('election-page.seo').pipe(take(1)).subscribe((seo: any) => {
              this.appMeta.setMeta(
                this.person.sirname + ' ' + this.person.name + ' | democratium',
                seo.description,
                seo.keywords + ', ' + this.person.sirname + ' ' + this.person.name,
                seo.img,
                this.share.langPath() + '/person' + '/' + params.slug
              );
              // console.log(resp);
              // this.material_id = resp.id;
              // this.author = resp.author;
              // this.person = resp.person;
              // this.workplace = resp.workplace;
              // this.title = resp.title;
              // this.pub_time = resp.pub_time;
              // this.blocks = resp.blocks;
              // this.tags = resp.tags;
              //
              this.subscriptions.add(this.service.getPartyHistory(resp.id).subscribe((data: any) => {
                this.history = data.result;
                this.historyCheck = true;
              }));


              // this.subscriptions.add(this.service.getStats(resp.id).subscribe((data: any) => {
              //   // this.stats = data.result;
              //   // this.statsCheck = true;
              //   // this.totalMissings = Math.floor(this.stats.voting.absent * 100 / this.stats.voting.laws_count);
              // }));


              this.subscriptions.add(this.service.getLawsProblems(resp.id).subscribe((data: any) => {
                // console.log(data)
                this.problems = data.result;
                this.problemsCheck = true;
                this.page_spinner.hide();
              }));


              if (this.platform.check()) {
                this.subscriptions.add(this.service.getMoreArticlesWidget(resp.id).subscribe((res: any) => {
                  // console.log(resp);
                  // if (resp) {
                  this.checkHttpArticles = true;
                  this.articles = res.result;
                  // }
                }));
              }
              this.subscriptions.add(this.service.getStatistic(resp.id).subscribe((res: any) => {
                // console.log(resp);
                // if (resp) {
                // this.checkHttpArticles = true;
                this.static = res.result;
                // }
              }));
              this.subscriptions.add(this.service.getMissings(resp.id).subscribe((res: any) => {
                // console.log(resp);
                // if (resp) {
                // this.checkHttpArticles = true;
                // res.result.not_vote_percent = Math.floor(res.result.not_vote_percent);
                // res.result.absent_percent = Math.floor(res.result.absent_percent);
                if (res.result) {
                  this.missings = res.result;
                  this.missings.absent_percent = Math.floor(this.missings.absent_percent);
                  this.missings.not_vote_percent = Math.floor(this.missings.not_vote_percent);
                }
                // not_vote_percent
                // console.log(this.missings);
                // }
              }));


              //
              //
              this.auth.currentUserSubject.subscribe(data => {
                // console.log(data);
                if (data) {
                  // console.log(data)
                  this.zone.run(() => {
                    this.service.canVote(data.token, resp.id).pipe(take(1)).subscribe((mood: any) => {
                      // console.log(mood);
                      if (!mood.result.can_vote) {
                        this.opition = mood.result;
                      }
                      this.loadOpition = true;
                    });
                  });
                } else {
                  // this.canVote = true;
                  this.opition = {
                    can_vote: true
                  };
                  this.loadOpition = true;

                  // this.user = null;
                }
              });
            })
          );
        }
      })
    );
  }

}
