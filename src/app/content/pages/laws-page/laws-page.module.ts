import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LawsPageRoutingModule} from './laws-page-routing.module';
import {FormsModule} from '@angular/forms';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ClickOutsideModule} from 'ng-click-outside';
import {ArticleTagsModule} from '../../templates/elements/article-tags/article-tags.module';
import {SheredTooltipModule} from '../../../modules/shered-tooltip.module';
import {SheredPaginationModule} from '../../../modules/shered-pagination.module';
import {RightWidgetComponent} from './templates/right-widget/right-widget.component';
import {MoreArticlesWidgetModule} from '../../templates/widgets/more-articles-widget/more-articles-widget.module';
import {RightWidgetToolbarModule} from '../../templates/widgets/right-widget-toolbar/right-widget-toolbar.module';
import {AttractedWidgetModule} from '../../templates/widgets/attracted-widget/attracted-widget.module';
import {SelectSearchModule} from '../../templates/textareas/select-search/select-search.module';
import {SelectWithGroupModule} from '../../templates/textareas/select-with-groups/select-with-group.module';
import {LawsRootService} from './service/laws-root.service';
import {SheredTranslateModule} from '../../../modules/shered-translate.module';
import {LawsRootComponent} from './laws-root/laws-root.component';
import {LawOpitionWidgetComponent} from './templates/law-opition-widget/law-opition-widget.component';
import {RatioVotingWidgetComponent} from './templates/ratio-voting-widget/ratio-voting-widget.component';
import {SimpleSelectModule} from '../../templates/textareas/simple-select/simple-select.module';
import {FinancialHelpModule} from '../../templates/widgets/financial-help/financial-help.module';
import {PageMinHeightModule} from '../../../directives/page-min-height/page-min-height.module';
import {NewsHtmlTemplateModule} from '../../templates/preloaders/news-html-template/news-html-template.module';
import {FixedWidgetModule} from '../../../directives/fixed-widget/fixed-widget.module';
import {BtnCloseModule} from '../../templates/elements/btn-close/btn-close.module';
import {EmpyListStringModule} from '../../templates/elements/empy-list-string/empy-list-string.module';
// import {AdsenseModule} from 'ng2-adsense';

@NgModule({
  declarations: [
    LawsRootComponent,
    RightWidgetComponent,
    LawOpitionWidgetComponent,
    RatioVotingWidgetComponent
  ],
  imports: [
    CommonModule,
    LawsPageRoutingModule,
    FormsModule,
    PerfectScrollbarModule,
    ClickOutsideModule,
    ArticleTagsModule,
    SheredTooltipModule,
    SheredPaginationModule,
    MoreArticlesWidgetModule,
    RightWidgetToolbarModule,
    FinancialHelpModule,
    AttractedWidgetModule,
    SelectSearchModule,
    SelectWithGroupModule,
    SheredTranslateModule,
    NewsHtmlTemplateModule,
    SimpleSelectModule,
    PageMinHeightModule,
    FixedWidgetModule,
    BtnCloseModule,
    EmpyListStringModule
  ],
  providers: [LawsRootService]
})
export class LawsPageModule {
}
