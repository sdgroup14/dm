import {Component, Input, OnInit} from '@angular/core';
import {DateService} from '../../../../../services/date.service';

@Component({
  selector: 'app-laws-problems',
  templateUrl: './laws-problems.component.html',
  styleUrls: ['./laws-problems.component.scss']
})
export class LawsProblemsComponent implements OnInit {
  problems: any = [];
  isOpen = true;
  problemsCheck = false;

  @Input() set setProblems(data) {
    this.problems = data;
    // if (data.check) {
    //   this.problemsCheck = true;
    // }
  }

  constructor(public dateService: DateService) {
  }

  ngOnInit() {
  }

}
