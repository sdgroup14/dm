import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-missings',
  templateUrl: './missings.component.html',
  styleUrls: ['./missings.component.scss']
})
export class MissingsComponent implements OnInit {
  missings: any;
  // check = false;

  @Input() set setVoting(data) {
    // console.log(data)
    if (data && Object.keys(data).length) {
      // console.log(data)
      this.missings = data;
      // this.check = data.check;
    }
  }

  constructor() {
  }

  ngOnInit() {
  }

}
