import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LawsRootComponent} from './laws-root/laws-root.component';

const routes: Routes = [
  {
    path: '',
    component: LawsRootComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LawsPageRoutingModule { }
