import {Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PersonPageService {

  constructor(private  http: HttpClient) {
  }

  getPerson(slug) {
    return this.http.get(environment.APIUrl + '/persons/by-slug?slug=' + slug, {});
  }

  getPartyHistory(id) {
    return this.http.get(environment.APIUrl + '/persons/party-history?person_id=' + id, {});
  }

  // getStats(id) {
  //   return this.http.get(environment.APIUrl + '/persons/law-stats?person_id=' + id, {});
  // }

  getLawsProblems(id) {
    return this.http.get(environment.APIUrl + '/materials/convict-person?person_id=' + id, {});
  }

  getMoreArticlesWidget(slug) {
    const query = slug ? '?person_id=' + slug : '';
    return this.http.get(environment.APIUrl + '/materials/person' + query, {})
  }

  getStatistic(id) {
    return this.http.get(environment.APIUrl + '/persons/stat-improve?person_id=' + id, {})
  }

  getMissings(id) {
    return this.http.get(environment.APIUrl + '/persons/stat-votes?person_id=' + id, {})
  }

  canVote(token, id) {
    const apiOptions = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', token)
    };
    // const query = page ? '&page=' + page : '';
    return this.http.get(environment.GETApiUrl + '/vote/person-user/stats-or-vote?id=' + id, apiOptions);
  }




}
