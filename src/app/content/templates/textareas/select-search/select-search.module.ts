import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SelectSearchComponent} from './select-search.component';
import {FormsModule} from '@angular/forms';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {SheredTranslateModule} from '../../../../modules/shered-translate.module';
import {ClickOutsideModule} from '../../../../directives/click-outside/click-outside.module';

@NgModule({
  declarations: [SelectSearchComponent],
  imports: [
    CommonModule,
    FormsModule,
    PerfectScrollbarModule,
    ClickOutsideModule,
    SheredTranslateModule
  ],
  providers: [],
  exports: [SelectSearchComponent]
})
export class SelectSearchModule { }
