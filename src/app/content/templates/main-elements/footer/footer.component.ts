import {Component, Input, OnInit} from '@angular/core';
import {LangService} from '../../../../services/lang.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  copyYear: number;
  @Input() defaultLang: boolean;
  menu: any = [];

  constructor(public langService: LangService, private translate: TranslateService) {
  }

  ngOnInit() {
    this.copyYear = new Date().getFullYear();

    this.menu = this.translate.get('footer.menu');
  }

}
