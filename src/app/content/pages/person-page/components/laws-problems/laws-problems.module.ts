import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LawsProblemsComponent} from './laws-problems.component';
import {SheredTranslateModule} from '../../../../../modules/shered-translate.module';
import {EmpyListStringModule} from '../../../../templates/elements/empy-list-string/empy-list-string.module';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [LawsProblemsComponent],
  imports: [
    CommonModule,
    SheredTranslateModule,
    RouterModule,
    EmpyListStringModule
  ],
  exports: [LawsProblemsComponent]
})
export class LawsProblemsModule { }
