import {Injectable} from '@angular/core';
import {CookieService} from './cookies-service.service';

@Injectable({
  providedIn: 'root'
})
export class LangService {

  switch(lang) {
    if (this.cookie.get('applang') === lang) {
      return;
    }
    this.cookie.remove('applang');
    const _d = new Date();
    const exp = _d.setFullYear(_d.getFullYear() + 1);
    const d_final = new Date(exp);
    this.cookie.put('applang', lang, {
      expires: d_final
    });
    window.location.reload();
  }

  get() {
    return this.cookie.get('applang');
  }

  check() {
  }

  constructor(private cookie: CookieService) {
  }
}
