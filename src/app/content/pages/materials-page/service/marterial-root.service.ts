import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MarterialRootService {
  articles = new Subject();
  widgetAttracted = new Subject();

  constructor(private  http: HttpClient) {
  }

  getWidgetAttracted() {
    return this.http.get(environment.APIUrl + '/materials/convict', {});
  }

  getMoreArticles() {
    return this.http.get(environment.APIUrl + '/materials/popular', {});
  }

  getList(params) {
    let urlParams = '?lang=ru&is_truth=' + params.is_truth;
    urlParams += params.page ? '&page=' + params.page : '';
    urlParams += params.workplace_id ? '&workplace_id=' + params.workplace_id : '';
    urlParams += params.region_id ? '&region_id=' + params.region_id : '';
    urlParams += params.city_id ? '&city_id=' + params.city_id : '';
    urlParams += params.search !== '' ? '&search=' + params.search : '';
    return this.http.get(environment.APIUrl + '/materials/list' + urlParams, {});
  }

  getListNext(params) {
    let urlParams = '?lang=ru&is_truth=' + params.is_truth;
    urlParams += params.page ? '&page=' + params.page : '';
    urlParams += params.workplace_id ? '&workplace_id=' + params.workplace_id : '';
    urlParams += params.region_id ? '&region_id=' + params.region_id : '';
    urlParams += params.city_id ? '&city_id=' + params.city_id : '';
    urlParams += params.search !== '' ? '&search=' + encodeURIComponent(params.search) : '';
    return this.http.get(environment.APIUrl + '/materials/list' + urlParams, {}).subscribe(resp => {
      this.articles.next(resp);
    });
  }



}
