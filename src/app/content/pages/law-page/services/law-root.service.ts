import {Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LawRootService {

  constructor(private  http: HttpClient) {
  }

  getLaw(slug) {
    return this.http.get(environment.APIUrl + '/laws?slug=' + slug, {});
  }

  getLawFullText(url) {
    return this.http.get(url, {});
  }

  getBottomList(id, page?) {
    const query = page ? '&page=' + page : '';
    return this.http.get(environment.APIUrl + '/laws/collision?id=5' + query, {});
  }

  getWidgetOpition(id) {
    // const query = page ? '&page=' + page : '';
    return this.http.get(environment.APIUrl + '/laws/stat-party-for-against?law_id=' + id, {});
  }

  getWidgetsRatio(id) {
    // const query = page ? '&page=' + page : '';
    return this.http.get(environment.APIUrl + '/laws/stat-gov-vs-users?law_id=' + id, {});
  }

  getSpecialisComments(id) {
    // const query = page ? '&page=' + page : '';
    return this.http.get(environment.APIUrl + '/laws/prof-comments?law_id=' + id, {});
  }

  sendMessage(data) {
    return this.http.post(environment.GETApiUrl + '/law/prof-comments', data, {});
  }


  getVotes(id) {
    // const query = page ? '&page=' + page : '';
    return this.http.get(environment.APIUrl + '/laws/parties-votes?law_id=' + id, {});
  }

  getVotesDropdown(id, law_id) {
    // const query = page ? '&page=' + page : '';
    return this.http.get(environment.APIUrl + '/laws/persons-votes?law_id=' + law_id + '&party_id=' + id, {});
  }

  canVoteMood(token, id) {
    const apiOptions = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', token)
    };
    // const query = page ? '&page=' + page : '';
    return this.http.get(environment.GETApiUrl + '/vote/law-user-improve/stats-or-vote?id=' + id, apiOptions);
  }

  canVoteVerity(token, id) {
    const apiOptions = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', token)
    };
    // const query = page ? '&page=' + page : '';
    return this.http.get(environment.GETApiUrl + '/vote/law-user/stats-or-vote?id=' + id, apiOptions);
  }


}
