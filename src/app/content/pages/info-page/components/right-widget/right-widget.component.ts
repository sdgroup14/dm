import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-right-widget',
  templateUrl: './right-widget.component.html',
  styleUrls: ['./right-widget.component.scss']
})
export class RightWidgetComponent implements OnInit {
  @Input() sections: any = [];

  goTo(i, item) {
    item.selected = true;
    this.sections.map(it => {
      if (it.title !== item.title) {
        it.selected = false;
      }
    });
    const section = document.getElementsByClassName('section')[i];
    // console.log(document.getElementsByClassName('section'));
    // const list = document.getElementsByClassName('.info')[0];
    section.scrollIntoView({behavior: 'smooth'});
  }


  // fixed = false;

  // @HostListener('document:scroll', ['$event']) onScroll(e) {
  //   if (this.el) {
  //     if (0 >= this.el.nativeElement.getBoundingClientRect().top - 20) {
  //       this.fixed = true;
  //     } else {
  //       this.fixed = false;
  //     }
  //   }
  // }

  constructor() {
  }

  ngOnInit() {
  }

}
