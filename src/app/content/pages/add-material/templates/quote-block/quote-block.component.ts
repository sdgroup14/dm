import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-quote-block',
  templateUrl: './quote-block.component.html',
  styleUrls: ['./quote-block.component.scss']
})
export class QuoteBlockComponent implements OnInit {
  value = '';
  descr = '';
  @Input() placeholder: any;

  @Input() set changeСontent(text) {
    if (text) {
      this.value = text;
    }
  }

  @Input() set changeDescr(text) {
    if (text) {
      this.descr = text;
    }
  }

  @Output() newCite = new EventEmitter();
  @Output() newDescr = new EventEmitter();


  constructor() {
  }

  ngOnInit() {
  }

}
