import {Component, EventEmitter, HostListener, Input, NgZone, OnInit, Output, Renderer2} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Location} from '@angular/common';
import {LangService} from '../../../../services/lang.service';
import {AppAuthService} from '../../../../services/app-auth.service';
import {CookieService} from '../../../../services/cookies-service.service';
import {Router} from '@angular/router';
import {CommonAppService} from '../../../../services/common-app.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  // isMenu = false;
  isHide = false;
  isDropdown = false;
  @Output() menuToggle = new EventEmitter();
  @Output() authToggle = new EventEmitter();
  @Input() defaultLang: boolean;
  @Input() isMenu: boolean;
  menu: any = [];
  profileDropdown: any = [];
  isLogin = false;
  user: any = {};
  checkLangCookie = false;

  @HostListener('window:scroll', ['$event']) onScroll(e) {
    // console.log(e.target.scrollingElement.scrollTop)
    if (e.target.scrollingElement.scrollTop > 100) {
      // this.isHide = true;
    }
    // if (this.el) {
    //   if (0 >= this.el.nativeElement.getBoundingClientRect().top - 20) {
    //     this.fixed = true;
    //   } else {
    //     this.fixed = false;
    //   }
    // }
  }

  closeDropdown() {
    if (this.isDropdown) {
      this.isDropdown = false;
    }
    // console.log('123')
  }

  toggleMenu() {
    this.isMenu = !this.isMenu;
    // if (this.isMenu) {
    //   this.router.navigate([{outlets: {modals: 'main-menu-modal'}}], {skipLocationChange: true});
    // } else {
    //   this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
    // }
    this.menuToggle.emit(this.isMenu);
    this.isMenu ? this.renderer.addClass(document.body, 'scrollOff') : this.renderer.removeClass(document.body, 'scrollOff');
  }

  openAuth() {
    this.router.navigate([{outlets: {modals: 'auth'}}], {skipLocationChange: true});
  }

  backHandler() {
    this.location.back();
  }

  constructor(
    public langService: LangService,
    private translate: TranslateService,
    public appAuth: AppAuthService,
    public service: CommonAppService,
    private cookie: CookieService,
    private renderer: Renderer2,
    private router: Router,
    private zone: NgZone,
    private location: Location
  ) {
  }

  ngOnInit() {
    // const resp: any = {success: true, result: {user: {name: '\u0414\u0430\u043d\u0438\u0438\u043b \u0421\u043e\u043b\u043e\u0432\u044c\u0451\u0432', avatar: 'https://graph.facebook.com/v3.0/1445358465589136/picture?type=normal', role: 'user'}, token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJkZW1vY3JhdGl1bS5uZXQiLCJzdWIiOjU2LCJpYXQiOjE1NjkxMzcwNzQsImV4cCI6MTU2OTE0MDY3NH0.sxPTs_F0S3ZVN9dwKOqRA1VYD5tRJlBNGUUYKJJ665YWlkLp9u_-breLBv_7nIy8VKU5Qt07cf6_F6-V3s94e07FyBGInR_Fblyd4hVILNLV3ihGjIJakD22krZhkCdtbEpFKS7s8Pxty4Lad6kJhFiwNdqExQJy5EmHnpzqy4Q', token_exp: '1569140674', refresh: '$2y$12$cwDAMeJJmDjZZWIQfZohk.FnWyWonSJeagjV7FyPgCS.BS55Q7jfK', refresh_exp: '1569309874', sign: 'fbe416c8d54ed6d1'}};
    // const d_final = new Date(Date.now() + 60000000);
    // this.cookie.put('appToken', JSON.stringify(resp.result), {
    //   expires: d_final
    // });
    this.service.mobileMenuModal.subscribe(bool => {
      // console.log('123')
      setTimeout(() => {
        this.isMenu = false;
      }, 100);
    });
    this.appAuth.currentUserSubject.subscribe(data => {
      if (data) {
        this.zone.run(() => {
          this.isLogin = true;
          this.user = data.user;
          // console.log(this.user);
          // this.authCheck.emit(false);
          // this._renderer.removeClass(document.body, 'scrollOff');
        });
      } else {
        this.isLogin = false;
        this.user = null;
      }
    });

    if (this.appAuth.currentUserValue) {
      this.isLogin = true;
      this.user = this.appAuth.currentUserValue.user;
    }

    this.menu = this.translate.get('header.menu');
    this.profileDropdown = this.translate.get('header.profile-dropdown');

    const lang = this.cookie.get('applang');
    if (lang) {
      // this.lang = lang === 'uk';
      this.checkLangCookie = true;
    } else {
      let count: any;
      const _d = new Date();
      const exp = _d.setFullYear(_d.getFullYear() + 1);
      const d_final = new Date(exp);
      if (!this.cookie.get('applangCounter')) {
        this.cookie.put('applangCounter', '0', {
          expires: d_final
        });
      } else {

        count = this.cookie.get('applangCounter');
        this.cookie.put('applangCounter', (+count + 1).toString(), {
          expires: d_final
        });
      }
      // this.router.events.subscribe((event: any) => {
      //   if (event instanceof NavigationEnd) {
      const routerLang: any = this.location.path().split('/')[1];
      // this.lang = routerLang === 'ru' ? false : true;

      if (count >= 4) {
        this.cookie.put('applang', routerLang === 'ru' ? 'ru' : 'uk', {
          expires: d_final
        });
        this.checkLangCookie = true;
      }
      // }
      // });
    }
  }

}
