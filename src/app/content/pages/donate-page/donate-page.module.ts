import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DonatePageRoutingModule} from './donate-page-routing.module';
import {DonatePageService} from './services/donate-page.service';
import {DonatePageComponent} from './donate-page.component';
import {SheredTranslateModule} from '../../../modules/shered-translate.module';

@NgModule({
  declarations: [DonatePageComponent],
  imports: [
    CommonModule,
    DonatePageRoutingModule,
    SheredTranslateModule
  ],
  providers: [DonatePageService]
})
export class DonatePageModule {
}
