import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-article-tags',
  templateUrl: './article-tags.component.html',
  styleUrls: ['./article-tags.component.scss']
})
export class ArticleTagsComponent implements OnInit {
  @Input() tags: any = [];

  constructor() {
  }

  ngOnInit() {
  }

}
