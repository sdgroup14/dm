import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SingleOpitionWidgetComponent} from './single-opition-widget.component';
import {SheredTranslateModule} from '../../../../modules/shered-translate.module';
import {CheckCountPosModule} from '../../../../directives/check-count-pos/check-count-pos.module';
import {SingleOpitionWidgetService} from './single-opition-widget.service';

@NgModule({
  declarations: [SingleOpitionWidgetComponent],
  imports: [
    CommonModule,
    SheredTranslateModule,
    CheckCountPosModule
  ],
  providers: [SingleOpitionWidgetService],
  exports: [SingleOpitionWidgetComponent]
})
export class SingleOpitionWidgetModule {
}
