import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SelectSearchService} from '../select-search/select-search.service';

@Component({
  selector: 'app-simple-select',
  templateUrl: './simple-select.component.html',
  styleUrls: ['./simple-select.component.scss']
})
export class SimpleSelectComponent implements OnInit {
  active = false;
  choiceTitle = '';
  placeholder = false;

  @Input() set setValues(array) {
    if (array.length) {
      this.data = array;
      const selected = this.data.filter(it => {
        return it.selected;
      });
      if (selected.length) {
        this.choiceTitle = selected[0].title;
      }
    }
  }

  // @Input() placeholder: string;
  // @Input() icon: boolean;
  @Output() valueChange = new EventEmitter();
  // serach = '';
  data: any = [];

  close() {
    if (this.active) {
      this.active = false;
    }
    // console.log('123')
  }

  apply(item) {
    this.placeholder = false;
    this.choiceTitle = item.title;
    this.active = false;
    // this.serach = '';
    this.valueChange.emit(item);
  }

  constructor(public service: SelectSearchService) {
  }

  ngOnInit() {
    // this.service.data.subscribe((resp: any) => {
    //   if (resp.result && resp.result.hasOwnProperty('data')) {
    //
    //     this.data = resp.result.data;
    //     return;
    //   }
    //   this.data = resp.result;
    // });
  }

}
