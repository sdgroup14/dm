import {Component, EventEmitter, Input, OnInit, Output, Renderer2, ViewChild} from '@angular/core';
import {LawRootService} from '../../services/law-root.service';
import {take} from 'rxjs/operators';
import {PerfectScrollbarDirective} from 'ngx-perfect-scrollbar';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-votes-modal',
  templateUrl: './votes-modal.component.html',
  styleUrls: ['./votes-modal.component.scss']
})
export class VotesModalComponent implements OnInit {
  isOpen = false;
  // @Output() closeModal = new EventEmitter();
  @ViewChild(PerfectScrollbarDirective, {static: false}) directiveRef?: PerfectScrollbarDirective;

  // @Input() set data(data) {
  // }

  lawId: any;

  // checkbox = false;
  // model = {
  //   email: ''
  // };
  // isPost = false;
  // errors: any = {};
  items: any = [];

  close() {
    this.isOpen = true;
    this.renderer.removeClass(document.body, 'scrollOff');
    setTimeout(() => {
      this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
    }, 150);

  }

  toggle(item) {
    if (!item.values) {
      this.service.getVotesDropdown(item.party.id, this.lawId).pipe(take(1)).subscribe((resp: any) => {
        // console.log(resp);
        item.values = resp.result;
        // console.log(item.values);
      });
    }
    item.isOpen = !item.isOpen;
    this.items.map(it => {
      if (item.party.id !== it.party.id) {
        // it.isOpen = false;
      }
    });
    setTimeout(() => {

      this.directiveRef.update();
    }, 100);
  }

  constructor(private service: LawRootService, private renderer: Renderer2, private router: Router, private route: ActivatedRoute) {
    // console.log(this.route);
    this.route.queryParamMap.subscribe(() => {
      const data = this.router.getCurrentNavigation().extras.state;
      // if (data.items.length) {
      this.items = data.items;
      this.lawId = data.law_id;
      // this.isOpen = true;
      setTimeout(() => {
        this.isOpen = true;
        this.renderer.addClass(document.body, 'scrollOff');
      }, 100);

      // }
    });
  }

  ngOnInit() {

  }

}
