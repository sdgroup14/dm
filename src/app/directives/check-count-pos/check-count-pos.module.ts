import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CheckCountPosDirective} from './check-count-pos.directive';

@NgModule({
  declarations: [CheckCountPosDirective],
  imports: [
    CommonModule
  ],
  exports: [CheckCountPosDirective]
})
export class CheckCountPosModule { }
