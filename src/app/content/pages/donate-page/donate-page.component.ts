import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {DonatePageService} from './services/donate-page.service';
import {PagePreloaderService} from '../../templates/preloaders/page-preloader/page-preloader.service';
import {take} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {MetaService} from '../../../services/meta.service';
import {ShareService} from '../../../services/share.service';

@Component({
  selector: 'app-donate-page',
  templateUrl: './donate-page.component.html',
  styleUrls: ['./donate-page.component.scss']
})
export class DonatePageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  blocks: any = [];

  constructor(
    private translate: TranslateService,
    private appMeta: MetaService,
    private share: ShareService,
    private service: DonatePageService,
    private page_spinner: PagePreloaderService) {
  }

  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.subscriptions.add(this.service.get().subscribe((resp: any) => {
      // console.log(resp.result.text);
      // console.log(JSON.parse(resp))
      this.blocks = resp.result.text;
      this.page_spinner.hide();

      this.translate.get('page-donate').pipe(take(1)).subscribe((data: any) => {
        const seo = data.seo;
        this.appMeta.setMeta(
          data.title + ' | democratium',
          seo.description,
          seo.keywords,
          seo.img,
          this.share.langPath() + '/donate',
          true
        );
      });

      // this.subscriptions.add(this.service.getFile(resp.result.file).subscribe(res => {
      //   // console.log(res);
      //   this.sections = res;
      //
      // }));
    }));
  }

}
