import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-article-top',
  templateUrl: './article-top.component.html',
  styleUrls: ['./article-top.component.scss']
})
export class ArticleTopComponent implements OnInit {
  @Input() person: any;
  @Input() workplace: any;

  constructor() {
  }

  ngOnInit() {
  }

}
