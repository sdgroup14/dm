import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PartiesModalComponent} from './parties-modal.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {BtnCloseModule} from '../../elements/btn-close/btn-close.module';

@NgModule({
  declarations: [PartiesModalComponent],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    BtnCloseModule
  ],
  exports: [PartiesModalComponent]
})
export class PartiesModalModule {
}
