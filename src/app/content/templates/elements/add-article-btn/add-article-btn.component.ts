import {Component, Input, OnInit} from '@angular/core';
import {isBoolean} from 'util';

@Component({
  selector: 'app-add-article-btn',
  templateUrl: './add-article-btn.component.html',
  styleUrls: ['./add-article-btn.component.scss']
})
export class AddArticleBtnComponent implements OnInit {
  @Input() rLink: any = [];
  @Input() skipLoc: boolean;
  @Input() state: any = {};

  // [{outlets: {modals: 'auth'}}], {skipLocationChange: true}
  routerHandler(e) {
    // e.preventDefault();
    // const currentUser = this.auth.currentUserValue;
    // if (!currentUser) {
    //   this.translate.get('errors.auth-add-material-error').subscribe(content => {
    //     this._common.notification.next({
    //       type: 'warning',
    //       text: content
    //     });
    //   });
    //   return;
    // }
    // this.router.navigate(['/create/material']);
  }

  constructor() {
  }

  // constructor(private router: Router, private auth: AppAuthService, private translate: TranslateService, private _common: CommonAppService) {
  // }
  //

  ngOnInit() {
  }

}
