import {Component, Input, NgZone, OnInit} from '@angular/core';
import {AppAuthService} from '../../../../../services/app-auth.service';
import {CommonAppService} from '../../../../../services/common-app.service';
import {ElectionService} from '../../services/election.service';
import {take} from 'rxjs/operators';
import {PlatformService} from '../../../../../services/platform.service';

@Component({
  selector: 'app-online-voting',
  templateUrl: './online-voting.component.html',
  styleUrls: ['./online-voting.component.scss']
})
export class OnlineVotingComponent implements OnInit {
  parties: any = [];
  check = false;

  @Input() set data(data) {
    if (this.platform.check()) {
      if (data.length) {
        this.parties = data;
        this.check = true;
        this.editRsp(data);
      }
      // if (!data.sheets.can_vote && data.sheets.hasOwnProperty('stats')) {
      //   if (data.sheets.stats && this.parties.length && data.sheets.stats.length && this.check === false) {
      //     this.check = true;
      //     this.editRsp(data.sheets.stats);
      //   }
      // } else {
      //   this.isVote = false;
      //   this.lastPercent = 100;
      //   this.total = 0;
      //   this.check = false;
      //   this.parties.map((_party: any, i) => {
      //     _party.percent = 0;
      //     _party.votes = 0;
      //     _party.votes_percent = 0;
      //   });
      // }
    }
  }

  isLogin = false;
  isVote = false;
  total = 0;
  lastPercent = 100;

  // percent = 0;
  editRsp(stats) {
    this.isVote = true;
    // this.parties
    // console.log(stats)

    // this.parties.map((_party: any, i) => {
    //   const stats_party = stats.filter(p => {
    //     return p.party_id === _party.id;
    //   })[0];
    //   // if (_party.id === stats[i].party_id) {
    //   _party.percent = 0;
    //   _party.votes = stats_party.value;
    //   // this.total += _party.votes;
    //   // }
    // });

    this.parties.map((_party: any, i) => {
      // console.log(_party)
      _party.percent = 0;
      _party.votes_percent = _party.value;
      // this.lastPercent -= _party.votes_percent;
      // if (i === this.parties.length - 1 && i !== 0) {
      //   _party.votes_percent = this.lastPercent;
      // } else {
      //   _party.votes_percent = Math.floor(_party.votes * 100 / this.total);
      //   this.lastPercent -= _party.votes_percent;
      // }
      setTimeout(() => {
        const timer = setInterval(() => {
          if (_party.percent === _party.votes_percent) {
            clearInterval(timer);
            return;
          }
          _party.percent++;
        }, 5);
      });
    });
  }

  vote(party) {
    if (!this.isLogin) {
      this.appCommon.authModal.next(true);
      return;
    }
    // console.log(party)
    this.service.voteHandler('vr2019', party.id).pipe(take(1)).subscribe((resp: any) => {
      this.editRsp(resp.result.stats);
    });


  }

  constructor(
    public appAuth: AppAuthService,
    public appCommon: CommonAppService,
    public service: ElectionService,
    public platform: PlatformService,
    private zone: NgZone
  ) {
  }

  ngOnInit() {
    if (this.platform.check()) {
      this.appAuth.currentUserSubject.subscribe(data => {

        if (data) {
          // console.log('123')
          this.zone.run(() => {
            this.isLogin = true;
          });
        } else {
          this.isLogin = false;
        }
      });


      if (this.appAuth.currentUserValue) {
        this.isLogin = true;
      }
    }

  }

}
