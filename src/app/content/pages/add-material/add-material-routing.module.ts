import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AddMaterialRootComponent} from './add-material-root/add-material-root.component';

const routes: Routes = [
  {
    path: ':id',
    component: AddMaterialRootComponent
  },
  {
    path: '',
    component: AddMaterialRootComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddMaterialRoutingModule {
}
