import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PopularOpinionWidgetComponent} from './popular-opinion-widget.component';
import {SheredTranslateModule} from '../../../../modules/shered-translate.module';
import {CheckCountPosModule} from '../../../../directives/check-count-pos/check-count-pos.module';

@NgModule({
  declarations: [PopularOpinionWidgetComponent],
  imports: [
    CommonModule,
    SheredTranslateModule,
    CheckCountPosModule
  ],
  exports: [PopularOpinionWidgetComponent, SheredTranslateModule]
})
export class PopularOpinionWidgetModule { }
