import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TermsPolicyPageRoutingModule } from './terms-policy-page-routing.module';
import {TermsPolicyPageComponent} from './terms-policy-page.component';
import {TermsPolicyPageService} from './services/terms-policy-page.service';
import {SheredTranslateModule} from '../../../modules/shered-translate.module';

@NgModule({
  declarations: [TermsPolicyPageComponent],
  imports: [
    CommonModule,
    TermsPolicyPageRoutingModule,
    SheredTranslateModule
  ],
  providers: [TermsPolicyPageService]
})
export class TermsPolicyPageModule { }
