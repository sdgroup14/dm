import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SimpleSelectComponent} from './simple-select.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ClickOutsideModule} from '../../../../directives/click-outside/click-outside.module';

@NgModule({
  declarations: [SimpleSelectComponent],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    ClickOutsideModule
  ],
  exports: [SimpleSelectComponent]
})
export class SimpleSelectModule { }
