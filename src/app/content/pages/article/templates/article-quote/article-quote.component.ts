import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-article-quote',
  templateUrl: './article-quote.component.html',
  styleUrls: ['./article-quote.component.scss']
})
export class ArticleQuoteComponent implements OnInit {
  @Input() content: any;
  constructor() { }

  ngOnInit() {
  }

}
