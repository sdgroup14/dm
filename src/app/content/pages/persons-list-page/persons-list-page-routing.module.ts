import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PersonsListPageComponent} from './persons-list-page.component';

const routes: Routes = [
  {
    path: '',
    component: PersonsListPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonsListPageRoutingModule {
}
