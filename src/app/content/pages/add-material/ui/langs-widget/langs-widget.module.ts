import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LangsWidgetComponent} from './langs-widget.component';

@NgModule({
  declarations: [LangsWidgetComponent],
  imports: [
    CommonModule
  ],
  exports: [LangsWidgetComponent]
})
export class LangsWidgetModule { }
