import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {ArticleSimilarsComponent} from './article-similars.component';

@NgModule({
  declarations: [ArticleSimilarsComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [ArticleSimilarsComponent]
})
export class ArticleSimilarsModule { }
