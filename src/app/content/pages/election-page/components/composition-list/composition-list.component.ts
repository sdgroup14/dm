import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {CurveService} from '../../../../../services/curve.service';
import {ElectionService} from '../../services/election.service';
import {Subscription} from 'rxjs';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-composition-list',
  templateUrl: './composition-list.component.html',
  styleUrls: ['./composition-list.component.scss']
})
export class CompositionListComponent implements OnInit, OnDestroy {
  @Output() partiesIsOpen = new EventEmitter();
  @Output() changeParties = new EventEmitter();
  private subscriptions = new Subscription();
  parties: any = [];


  toggle(party, canvas, resultEl: HTMLElement) {
    this.service.getMainListItem(party.id).pipe(take(1)).subscribe((resp: any) => {
      // console.log(resp);
      if (!party.groups) {
        party.groups = resp.result;
        party.member_count = 0;
        party.groups.map(it => {
          party.member_count += it.count;
        });
        // for (const key in resp.result) {
        //   party.groups.push()resp.result[key];
        //   // Use `key` and `value`
        // }
      }
      party.isOpen = true;
      this.parties.map(p => {
        if (p.title !== party.title) {
          p.isOpen = false;
        }
      });

      let startPosX = 0;
      let startPosY = 0;
      const box: any = resultEl.parentElement;
      const list: any = box.getElementsByClassName('list')[0];
      // console.log(child);

      // let endX = 0;
      // let endY = 0;
      setTimeout(() => {
        startPosX = resultEl.offsetLeft;
        startPosY = resultEl.offsetTop + resultEl.offsetHeight / 2;
        canvas.width = box.offsetWidth;
        canvas.height = box.offsetHeight;
        const ctx = canvas.getContext('2d');

        for (let i = 0; i < list.children.length; i++) {
          const child = list.children[i];

          const endX = child.offsetWidth;
          const endY = child.offsetTop + child.offsetHeight / 2;
          this.curve.drawLine(
            ctx,
            startPosX,
            startPosY,
            endX,
            endY,
            .5,
            true
          );
        }
      }, 100);

    });

  }

  showModal(main_party, party) {
    this.service.getMainListItemMembers(main_party.id, party.id, party.persons_ids.join(',')).pipe(take(1)).subscribe((resp: any) => {
      // console.log(resp);
      resp.result.map(member => {
        if(member.from){
          member.from = member.from.split('-')[0];
        }
      });
      this.partiesIsOpen.emit({
        members: resp.result,
        party_logo: party.logo,
        party_title: party.title
      });
    });
  }


  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.subscriptions.add(
      this.service.getMainList().subscribe((resp: any) => {
        // console.log(resp)
        this.parties = resp.result;
        this.changeParties.emit(this.parties);
      })
    );
  }

  constructor(private curve: CurveService, private service: ElectionService) {
  }

}
