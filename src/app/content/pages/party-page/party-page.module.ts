import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PartyPageRoutingModule } from './party-page-routing.module';
import {PartyPageComponent} from './party-page.component';
import {TopComponent} from './components/top/top.component';
import {RightWidgetComponent} from './components/right-widget/right-widget.component';
import {FinancialHelpModule} from '../../templates/widgets/financial-help/financial-help.module';
import {RightWidgetToolbarModule} from '../../templates/widgets/right-widget-toolbar/right-widget-toolbar.module';
import {FixedSocialModule} from '../../templates/widgets/fixed-social/fixed-social.module';
import {MoreArticlesWidgetModule} from '../../templates/widgets/more-articles-widget/more-articles-widget.module';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {FixedWidgetModule} from '../../../directives/fixed-widget/fixed-widget.module';
import {PageMinHeightModule} from '../../../directives/page-min-height/page-min-height.module';
import {SheredTranslateModule} from '../../../modules/shered-translate.module';
import {LeaderComponent} from './components/leader/leader.component';
import {SingleOpitionWidgetModule} from '../../templates/widgets/single-opition-widget/single-opition-widget.module';
import {PartiesModalModule} from '../../templates/main-elements/parties-modal/parties-modal.module';
import {PartyCompositionComponent} from './components/party-composition/party-composition.component';
import {InfluenceComponent} from './components/influence/influence.component';
import { StatisticModule } from '../person-page/components/statistic/statistic.module';
import {LawsProblemsModule} from '../person-page/components/laws-problems/laws-problems.module';
import {MembersComponent} from './components/members/members.component';
import {AddArticleBtnModule} from '../../templates/elements/add-article-btn/add-article-btn.module';
import {MissingsComponent} from './components/missings/missings.component';
import {EmpyListStringModule} from '../../templates/elements/empy-list-string/empy-list-string.module';
import {SheredTooltipModule} from '../../../modules/shered-tooltip.module';
import {SimpleSelectModule} from '../../templates/textareas/simple-select/simple-select.module';

@NgModule({
  declarations: [
    PartyPageComponent,
    TopComponent,
    RightWidgetComponent,
    LeaderComponent,
    PartyCompositionComponent,
    InfluenceComponent,
    MembersComponent,
    MissingsComponent
  ],
  imports: [
    CommonModule,
    PartyPageRoutingModule,
    FinancialHelpModule,
    RightWidgetToolbarModule,
    FixedSocialModule,
    MoreArticlesWidgetModule,
    PerfectScrollbarModule,
    FixedWidgetModule,
    PageMinHeightModule,
    SheredTranslateModule,
    SingleOpitionWidgetModule,
    PartiesModalModule,
    StatisticModule,
    LawsProblemsModule,
    AddArticleBtnModule,
    EmpyListStringModule,
    SheredTooltipModule,
    SimpleSelectModule
  ]
})
export class PartyPageModule { }
