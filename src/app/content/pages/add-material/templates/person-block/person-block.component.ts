import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {SelectSearchComponent} from '../../../../templates/textareas/select-search/select-search.component';

@Component({
  selector: 'app-person-block',
  templateUrl: './person-block.component.html',
  styleUrls: ['./person-block.component.scss']
})
export class PersonBlockComponent implements OnInit {
  active = false;
  person: any;
  @Output() valueChange = new EventEmitter();
  @Output() openModal = new EventEmitter();
  @ViewChild(SelectSearchComponent, {static: false}) inpSearchComponent: SelectSearchComponent;

  @Input() set setPerson(person) {
    if (person) {
      // console.log(person);
      this.person = person;
    }
  }
  close() {
    if(this.active){
      this.active = false;
    }
    // console.log('123')
  }
  open() {
    // setTimeout(() => {
    setTimeout(() => {
      this.active = true;
      this.inpSearchComponent.switchInputs();
    }, 1);
    // });
  }

  apply(item) {
    // setTimeout(() => {
    //   console.log('123')
    this.person = item;
    this.valueChange.emit(this.person.id);
    // }, 2);
    this.active = false;
  }

  constructor() {
  }

  ngOnInit() {

  }

}
