import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTooltipModule} from '@angular/material';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatTooltipModule,
  ],
  exports: [MatTooltipModule]
})
export class SheredTooltipModule { }
