import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {take} from 'rxjs/operators';
import {CommonAppService} from '../../../../../services/common-app.service';
import {AppAuthService} from '../../../../../services/app-auth.service';
import {ArticleService} from '../../../article/services/article.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-popular-opinion-widget',
  templateUrl: './popular-opinion-widget.component.html',
  styleUrls: ['./popular-opinion-widget.component.scss']
})
export class PopularOpinionWidgetComponent implements OnInit {
  @Input() id: any;
  @Input() moodUrl: any;
  @Input() verityUrl: any;
  @Input() infoSlug: any = '';

  @Input() set setMood(mood) {
    if (Object.keys(mood).length && !mood.can_vote) {
      this.mood.can_vote = mood.can_vote;
      this.mood.false = mood.stats.false;
      this.mood.null = mood.stats.null;
      this.mood.true = mood.stats.true;
      this.prepearOpition(this.mood);
    } else {
      this.mood.can_vote = true;
      this.mood.total = null;
    }

  }

  @Input() set setVerity(mood) {
    if (Object.keys(mood).length && !mood.can_vote) {
      this.verity.can_vote = mood.can_vote;
      this.verity.false = mood.stats.false;
      this.verity.null = mood.stats.null;
      this.verity.true = mood.stats.true;
      this.prepearOpition(this.verity);
    } else {
      this.verity.can_vote = true;
      this.verity.total = null;
    }
  }

  @Output() changeWidget = new EventEmitter();


  mood: any = {
    total: 0,
    true: 0,
    false: 0,
    null: 0,
    true_width: 0,
    false_width: 0,
    null_width: 0,
    can_vote: true
  };
  verity: any = {
    total: 0,
    true: 0,
    false: 0,
    null: 0,
    true_width: 0,
    false_width: 0,
    null_width: 0,
    can_vote: true
  };

  prepearOpition(opition) {
    opition.total = opition.true + opition.false + opition.null;
    setTimeout(() => {
      opition.true_width = (opition.true * 100 / opition.total).toFixed(1);
      opition.false_width = (opition.false * 100 / opition.total).toFixed(1);
      opition.null_width = (opition.null * 100 / opition.total).toFixed(1);
      // console.log(opition)
    }, 100);
  }

  showInfo() {
    this.router.navigate([{outlets: {modals: 'info'}}],
      {
        skipLocationChange: true,
        state: {
          slug: this.infoSlug,
          isOpen: true,
          icon: 'info',
          title: 'info'
        }
      });
  }

  constructor(
    public commonApp: CommonAppService,
    private service: ArticleService,
    private router: Router,
    private authenticationService: AppAuthService
  ) {
  }

  checkOpitionMood(value) {
    const currentUser: any = this.authenticationService.currentUserValue;
    if (currentUser) {
      this.service.sendOpitionMood(this.id, value, currentUser.token, this.moodUrl).pipe(take(1)).subscribe((resp: any) => {
        // resp.result.can_vote = false;
        resp.result.true = resp.result.stats.true;
        resp.result.false = resp.result.stats.false;
        resp.result.null = resp.result.stats.null;
        this.mood = resp.result;
        this.prepearOpition(this.mood);
      });
    } else {
      this.router.navigate([{outlets: {modals: 'auth'}}], {skipLocationChange: true});
    }
  }

  checkOpitionVerity(value) {
    const currentUser: any = this.authenticationService.currentUserValue;
    if (currentUser) {
      this.service.sendOpitionVerity(this.id, value, currentUser.token, this.verityUrl).pipe(take(1)).subscribe((resp: any) => {
        resp.result.true = resp.result.stats.true;
        resp.result.false = resp.result.stats.false;
        resp.result.null = resp.result.stats.null;
        this.verity = resp.result;
        this.prepearOpition(this.verity);
        // console.log(this.verity);


        if (this.verity.true >= this.verity.false && this.verity.true >= this.verity.null) {
          this.changeWidget.emit({
            users_sum: this.verity.total,
            users_vote: 'for',
            users_vote_count: this.verity.true
          });
        }
        if (this.verity.false >= this.verity.true && this.verity.false >= this.verity.null) {
          this.changeWidget.emit({
            users_sum: this.verity.total,
            users_vote: 'against',
            users_vote_count: this.verity.false
          });
        }
        if (this.verity.null >= this.verity.false && this.verity.null >= this.verity.true) {
          this.changeWidget.emit({
            users_sum: this.verity.total,
            users_vote: 'abstained',
            users_vote_count: this.verity.null
          });
        }

      });
    } else {
      this.router.navigate([{outlets: {modals: 'auth'}}], {skipLocationChange: true});
    }

  }


  ngOnInit() {
  }

}
