import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {MarterialRootService} from '../service/marterial-root.service';
import {DateService} from '../../../../services/date.service';
import {PagePreloaderService} from '../../../templates/preloaders/page-preloader/page-preloader.service';
import {PlatformService} from '../../../../services/platform.service';
import {MetaService} from '../../../../services/meta.service';
import {ShareService} from '../../../../services/share.service';

@Component({
  selector: 'app-materials-root',
  templateUrl: './materials-root.component.html',
  styleUrls: ['./materials-root.component.scss']
})
export class MaterialsRootComponent implements OnInit, OnDestroy {
  @ViewChild('topEl', {static: true}) topEl: ElementRef;
  private subscriptions = new Subscription();
  filtersToggle = false;
  searchValue = '';
  lastPage = 1;
  showFilters = false;
  attracted: any = [];
  moreArticles: any = [];

  filterParams: any = {
    is_truth: 'true',
    page: 1,
    workplace_id: '',
    region_id: '',
    city_id: '',
    search: ''
  };

  articles: any = [];

  clearSearch() {
    this.searchValue = this.filterParams.search = '';
    this.updateList();
  }

  changePageHandler(page) {
    this.lastPage = this.filterParams.page;
    this.filterParams.page = page;
    this.updateList();
  }

  switchFilters(input) {
    this.showFilters = true;
    this.filtersToggle = !this.filtersToggle;
    if (this.filtersToggle) {
      setTimeout(() => {
        input.focus();
      });
    }
  }

  changeWorkPlace(item) {
    this.filterParams.workplace_id = item.id;
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }


  changeRegion(item) {
    this.filterParams.region_id = item.id;
    // this.filterParams.district_id = '';
    this.filterParams.city_id = '';
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  changeCity(item) {
    this.filterParams.city_id = item.id;
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  searchHandler() {
    this.filtersToggle = false;
    this.searchValue = this.filterParams.search;
    this.updateList();
  }

  changePageArrowHandler(value) {
    this.filterParams.page += value;
    this.updateList();
  }

  updateList() {
    this.service.getListNext(this.filterParams);
  }


  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  constructor(
    private appMeta: MetaService,
    private share: ShareService,
    private page_spinner: PagePreloaderService,
    private platform: PlatformService,
    private service: MarterialRootService,
    public dateService: DateService) {
  }

  ngOnInit() {
    // console.log(this.articles);
    this.subscriptions.add(this.service.getList(this.filterParams).subscribe((resp: any) => {
      this.filterParams.itemsPerPage = resp.result.per_page;
      this.filterParams.total = resp.result.total;
      this.filterParams.lastPage = resp.result.last_page;
      this.articles = resp.result.list;
      this.page_spinner.hide();
      // console.log(this.articles);

      const seo: any = resp.result.seo;
      // console.log(seo);
      // this.translate.get('election-page.seo').pipe(take(1)).subscribe((seo: any) => {
      this.appMeta.setMeta(
        seo.title,
        seo.description,
        seo.keywords,
        seo.img,
        this.share.langPath() + '/materials'
      );
      if (this.platform.check()) {
        this.subscriptions.add(this.service.getWidgetAttracted().subscribe((res: any) => {
          this.attracted = res.result;
        }));
        this.subscriptions.add(this.service.getMoreArticles().subscribe((res: any) => {
          this.moreArticles = res.result;
        }));

      }


    }));
    this.subscriptions.add(this.service.articles.subscribe((resp: any) => {
      this.filterParams.itemsPerPage = resp.result.per_page;
      this.filterParams.total = resp.result.total;
      this.filterParams.lastPage = resp.result.last_page;
      this.articles = resp.result.list;
      // console.log(resp);
      if (this.lastPage !== this.filterParams.page) {
        this.topEl.nativeElement.scrollIntoView({behavior: 'smooth', block: 'start'});
      }
    }));

  }

}
