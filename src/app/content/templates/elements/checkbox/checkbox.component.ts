import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements OnInit {
  checkbox: any;
  @Input() value: any;
  @Output() newValue = new EventEmitter();

  change() {
    this.newValue.emit(this.value);
  }

  constructor() {
  }

  ngOnInit() {
  }

}
