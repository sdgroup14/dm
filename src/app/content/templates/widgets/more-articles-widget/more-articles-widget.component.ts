import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {DateService} from '../../../../services/date.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-more-articles-widget',
  templateUrl: './more-articles-widget.component.html',
  styleUrls: ['./more-articles-widget.component.scss']
})
export class MoreArticlesWidgetComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  @Input() title: string;
  articles: any = [];
  checkInit = false;
  url: string;
  slug: string;

  @Input() set setArticles(data) {
    if (data.check) {
      // console.log(data.articles)
      // console.log(data.articles);
      this.articles = data.articles;
      setTimeout(() => {

        this.checkInit = true;
      }, 500);
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  constructor(public dateService: DateService) {
  }

  ngOnInit() {
  }

}
