import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InfoPageService {

  constructor(private http: HttpClient) {
  }

  get() {
    return this.http.get(environment.APIUrl + '/info', {});
  }

  getFile(url) {
    return this.http.get(url, {});
  }

}
