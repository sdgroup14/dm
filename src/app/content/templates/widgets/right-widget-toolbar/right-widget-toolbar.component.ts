import {Component, Input, OnInit} from '@angular/core';
import {CommonAppService} from '../../../../services/common-app.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-right-widget-toolbar',
  templateUrl: './right-widget-toolbar.component.html',
  styleUrls: ['./right-widget-toolbar.component.scss']
})
export class RightWidgetToolbarComponent implements OnInit {
  shareIsOpen = false;
  @Input() search?: boolean;
  @Input() share?: boolean;
  @Input() add_person?: boolean;
  @Input() play?: boolean;
  @Input() playUrl?: boolean;
  @Input() info?: boolean;

  onpenFeedback() {
    this.router.navigate([{outlets: {modals: 'feedback'}}], {skipLocationChange: true});
  }

  openVideo() {
    this.router.navigate([{outlets: {modals: 'info'}}], {
      skipLocationChange: true, state: {
        isOpen: true,
        url: this.playUrl,
        icon: 'play',
        title: 'training-video'
      }
    });
  }


  constructor(public service: CommonAppService, public router: Router) {
  }

  ngOnInit() {
  }

}
