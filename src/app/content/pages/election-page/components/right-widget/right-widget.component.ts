import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {PlatformService} from '../../../../../services/platform.service';

@Component({
  selector: 'app-right-widget',
  templateUrl: './right-widget.component.html',
  styleUrls: ['./right-widget.component.scss']
})
export class RightWidgetComponent implements OnInit, AfterViewInit {
  @Input() data: any = [];


  constructor(public platform: PlatformService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    if (this.platform.check()) {
      setTimeout(() => {
        try {
          (window['adsbygoogle'] = window["adsbygoogle"] || []).push({});
        } catch (e) {
          console.error(e);
        }
      }, 100);
    }
  }

}
