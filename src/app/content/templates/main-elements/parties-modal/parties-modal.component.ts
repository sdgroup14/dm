import {Component, EventEmitter, Input, OnInit, Output, Renderer2} from '@angular/core';
import {PlatformService} from '../../../../services/platform.service';

@Component({
  selector: 'app-parties-modal',
  templateUrl: './parties-modal.component.html',
  styleUrls: ['./parties-modal.component.scss']
})
export class PartiesModalComponent implements OnInit {
  isOpen = false;
  members = [];
  title = '';
  logo = '';

  @Output() close = new EventEmitter();

  @Input() set setVisibility(data) {
    this.isOpen = data.visibility;
    this.members = data.members;
    this.title = data.party_title;
    this.logo = data.party_logo;
    this.isOpen ? this.renderer.addClass(document.body, 'scrollOff') : this.renderer.removeClass(document.body, 'scrollOff');
  }

  constructor(private renderer: Renderer2, private platform: PlatformService) {
  }

  ngOnInit() {
    if (this.platform.check()) {
      // this.appCommon.infoVisibility.subscribe((bool: boolean) => {
      //   this.isOpen = bool;
      //   this.isOpen ? this.renderer.addClass(document.body, 'scrollOff') : this.renderer.removeClass(document.body, 'scrollOff');
      // });
    }


  }

}
