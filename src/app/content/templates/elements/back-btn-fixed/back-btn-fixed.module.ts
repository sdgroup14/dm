import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BackBtnFixedComponent} from './back-btn-fixed.component';

@NgModule({
  declarations: [BackBtnFixedComponent],
  imports: [
    CommonModule
  ],
  exports: [BackBtnFixedComponent]
})
export class BackBtnFixedModule { }
