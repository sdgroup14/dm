export class Person {

  constructor(public name,
              public sirname,
              public  party,
              public avatar,
              public rating,
              public vote_for,
              public  vote_against) {
    this.party = party;
    this.avatar = avatar;
    this.vote_for = vote_for;
    this.vote_against = vote_against;
    this.name = name;
    this.sirname = sirname;
  }

  getFullName() {
    return `${this.name} ${this.sirname}`;
  }
}
