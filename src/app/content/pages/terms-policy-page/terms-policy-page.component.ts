import {Component, OnDestroy, OnInit} from '@angular/core';
import {TermsPolicyPageService} from './services/terms-policy-page.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {PagePreloaderService} from '../../templates/preloaders/page-preloader/page-preloader.service';
import {take} from 'rxjs/operators';
import {MetaService} from '../../../services/meta.service';
import {ShareService} from '../../../services/share.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-terms-policy-page',
  templateUrl: './terms-policy-page.component.html',
  styleUrls: ['./terms-policy-page.component.scss']
})
export class TermsPolicyPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  data: any = [];
  url: any;
  title = '';

  constructor(
    private appMeta: MetaService,
    private share: ShareService,
    private translate: TranslateService,
    private page_spinner: PagePreloaderService,
    private service: TermsPolicyPageService,
    private route: ActivatedRoute) {
  }

  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.url = this.route.snapshot.data.url;
    // this.service.get(this.url);
    this.subscriptions.add(this.service.get(this.url).subscribe((resp: any) => {
      // console.log(resp);
      this.data = resp.result;
      this.page_spinner.hide();
      const translate_key = this.url === '/policy' ? 'page-privacy-policy' : 'page-user-agreement';
      const route = this.url === '/policy' ? '/privacy-policy' : '/user-agreement';
      this.translate.get(translate_key).pipe(take(1)).subscribe((data: any) => {
        const seo = data.seo;
        this.title = data.title;
        this.appMeta.setMeta(
          data.title + ' | democratium',
          seo.description,
          seo.keywords,
          seo.img,
          this.share.langPath() + route,
           true
        );
      });
      // this.subscriptions.add(this.service.getFile(resp.result.file).subscribe(res => {
      //   // console.log(res);
      //   this.data = res;
      //   this.page_spinner.hide();
      // }));
    }));
  }

}
