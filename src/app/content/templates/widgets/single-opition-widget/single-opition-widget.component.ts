import {Component, Input, OnInit} from '@angular/core';
import {CommonAppService} from '../../../../services/common-app.service';
import {ArticleService} from '../../../pages/article/services/article.service';
import {AppAuthService} from '../../../../services/app-auth.service';
import {take} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-single-opition-widget',
  templateUrl: './single-opition-widget.component.html',
  styleUrls: ['./single-opition-widget.component.scss']
})
export class SingleOpitionWidgetComponent implements OnInit {
  @Input() id: any;
  @Input() question: any;
  @Input() opitionUrl: any;
  @Input() infoSlug: any = '';

  @Input() set setOpition(data) {
    if (Object.keys(data).length && !data.can_vote) {
      this.mood.can_vote = data.can_vote;
      this.mood.false = data.stats.false;
      this.mood.true = data.stats.true;
      this.prepearOpition(this.mood);
    } else {
      this.mood.can_vote = true;
      this.mood.total = null;
    }
  }


  mood: any = {
    total: 0,
    true: 0,
    false: 0,
    null: 0,
    true_width: 0,
    false_width: 0,
    null_width: 0,
    can_vote: true
  };

  prepearOpition(opition) {
    opition.total = opition.true + opition.false;
    setTimeout(() => {
      opition.true_width = (opition.true * 100 / opition.total).toFixed(1);
      opition.false_width = (opition.false * 100 / opition.total).toFixed(1);
      // console.log(opition)
    }, 100);
  }

  showInfo() {
    this.router.navigate([{outlets: {modals: 'info'}}],
      {
        skipLocationChange: true,
        state: {
          slug: this.infoSlug,
          isOpen: true,
          icon: 'info',
          title: 'info'
        }
      });
  }


  constructor(
    public commonApp: CommonAppService,
    private service: ArticleService,
    private router: Router,
    private authenticationService: AppAuthService
  ) {
  }

  checkOpitionMood(value) {
    const currentUser: any = this.authenticationService.currentUserValue;
    if (currentUser) {
      this.service.sendOpitionMood(this.id, value, currentUser.token, this.opitionUrl).pipe(take(1)).subscribe((resp: any) => {
        // resp.result.can_vote = false;
        resp.result.true = resp.result.stats.true;
        resp.result.false = resp.result.stats.false;
        this.mood = resp.result;
        this.prepearOpition(this.mood);
      });
    } else {
      this.router.navigate([{outlets: {modals: 'auth'}}], {skipLocationChange: true});
      // this.commonApp.authModal.next(true);

    }
  }



  ngOnInit() {
  }


}
