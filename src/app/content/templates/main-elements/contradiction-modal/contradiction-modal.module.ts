import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ContradictionModalComponent} from './contradiction-modal.component';
import {BtnCloseModule} from '../../elements/btn-close/btn-close.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SheredTranslateModule} from '../../../../modules/shered-translate.module';

@NgModule({
  declarations: [ContradictionModalComponent],
  imports: [
    CommonModule,
    BtnCloseModule,
    FormsModule,
    ReactiveFormsModule,
    SheredTranslateModule
  ],
  exports: [ContradictionModalComponent]
})
export class ContradictionModalModule { }
