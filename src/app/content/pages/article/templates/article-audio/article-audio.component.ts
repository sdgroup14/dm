import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-article-audio',
  templateUrl: './article-audio.component.html',
  styleUrls: ['./article-audio.component.scss']
})
export class ArticleAudioComponent implements OnInit {
  @Input() content: any;
  constructor() {
  }

  ngOnInit() {
  }

}
