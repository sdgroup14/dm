import {Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountPageService {
  articles = new Subject();
  files = new Subject();

  constructor(private http: HttpClient) {
  }

  saveProfile(data) {
    const apiData = new FormData();
    for (const key in data) {
      apiData.append(key, data[key]);
    }
    return this.http.post(environment.GETApiUrl + '/user/profile', apiData, {});
  }

  getProfile() {
    return this.http.get(environment.GETApiUrl + '/user/profile', {});
  }

  getMessages() {
    return this.http.get(environment.GETApiUrl + '/messages/list', {});
  }

  checkNotif() {
    return this.http.get(environment.GETApiUrl + '/messages/check-read', {});
  }

  resetNotif() {
    return this.http.post(environment.GETApiUrl + '/messages/make-read', {}, {});
  }

  sendMessage(data) {
    return this.http.post(environment.GETApiUrl + '/messages', data, {});
  }

  saveRole(type, text) {
    return this.http.post(environment.GETApiUrl + '/confirm-messages', {type, text}, {});
  }

  sendFile(file, message_id, type) {
    const apiData = new FormData();
    // for (const key in data) {
    apiData.append('file', file);
    apiData.append('message_id', message_id);
    // }
    return this.http.post(environment.GETApiUrl + '/confirm-messages/file', apiData, {}).subscribe((resp: any) => {
      const _resp: any = resp;
      _resp.type = type;
      this.files.next(_resp);
    }, err => {
      err.type = type;
      // this.files.next(err);
    });

  }


  getArticles(params) {
    let urlParams = '?status=' + params.status;
    urlParams += params.page ? '&page=' + params.page : '?';
    // urlParams += params.workplace_id ? '&workplace_id=' + params.workplace_id : '';
    // urlParams += params.region_id ? '&region_id=' + params.region_id : '';
    // urlParams += params.city_id ? '&city_id=' + params.city_id : '';
    // urlParams += params.district_id ? '&district_id=' + params.district_id : '';
    // urlParams += params.workplace_ext_id ? '&workplace_ext_id=' + params.workplace_ext_id : '';
    // urlParams += params.search !== '' ? '&search=' + params.search : '';
    // urlParams += params.order_by !== '' ? '&order=' + params.order_by : '';
    // urlParams += params.direction !== '' ? '&direction=' + params.direction : '';
    return this.http.get(environment.GETApiUrl + '/materials/list' + urlParams, {});
  }

  getArticlesNext(params) {
    let urlParams = '?status=' + params.status;
    urlParams += params.page ? '&page=' + params.page : '?';
    return this.http.get(environment.GETApiUrl + '/materials/list' + urlParams, {}).subscribe(resp => {
      this.articles.next(resp);
    });
  }

  deleteArticles(id) {
    return this.http.delete(environment.GETApiUrl + '/materials/' + id, {});
  }

  getRole() {
    return this.http.get(environment.GETApiUrl + '/user/role', {});
  }

}
