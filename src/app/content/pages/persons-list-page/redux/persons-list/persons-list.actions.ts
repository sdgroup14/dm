import {createAction, props} from '@ngrx/store';
import {Person} from '../../models/person.model';

export const GetAll = createAction(
  '[Persons] - Get Persons',
  props<{ payload: Person[] }>()
);
