import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {take} from 'rxjs/operators';
import {CurveService} from '../../../../../services/curve.service';
import {ElectionService} from '../../../election-page/services/election.service';
import {PlatformService} from '../../../../../services/platform.service';

@Component({
  selector: 'app-party-composition',
  templateUrl: './party-composition.component.html',
  styleUrls: ['./party-composition.component.scss']
})
export class PartyCompositionComponent implements OnInit, OnDestroy {
  parties: any = [];
  isPlatform = false;
  @Input() mainParty: any = {};

  @Input() set setParties(data) {
    if (data.length) {
      this.parties = data;
      this.isPlatform = this.platform.check();
      data.map(party => {
        this.total += party.count;
      });
      setTimeout(() => {
        if (this.parties.length && this.isPlatform) {
          // console.log(this.parties);
          // console.log(this.parties);
          this.toggle();
        }
      }, 1);
    }
  }

  total = 0;

  @Output() partiesIsOpen = new EventEmitter();
  @Output() changeParties = new EventEmitter();
  @ViewChild('canvasEl', {static: false}) canvasEl: ElementRef;
  @ViewChild('resultEl', {static: false}) resultEl: ElementRef;
  private subscriptions = new Subscription();


  toggle(party?) {
    // console.log(this.parties)
    const canvas = this.canvasEl.nativeElement;
    const resultEl = this.resultEl.nativeElement;
    // this.service.getMainListItem(party.id).pipe(take(1)).subscribe((resp: any) => {
    // console.log(resp);
    // if (!party.groups) {
    //   party.groups = resp.result;
    //   party.member_count = 0;
    //   party.groups.map(it => {
    //     party.member_count += it.count;
    //   });
    //   // for (const key in resp.result) {
    //   //   party.groups.push()resp.result[key];
    //   //   // Use `key` and `value`
    //   // }
    // }
    // party.isOpen = true;
    // this.parties.map(p => {
    //   if (p.title !== party.title) {
    //     p.isOpen = false;
    //   }
    // });

    let startPosX = 0;
    let startPosY = 0;
    const box: any = resultEl.parentElement;
    const list: any = box.getElementsByClassName('list')[0];
    // console.log(child);

    // let endX = 0;
    // let endY = 0;
    setTimeout(() => {
      startPosX = resultEl.offsetLeft;
      startPosY = resultEl.offsetTop + resultEl.offsetHeight / 2;
      canvas.width = box.offsetWidth;
      canvas.height = box.offsetHeight;
      const ctx = canvas.getContext('2d');

      for (let i = 0; i < list.children.length; i++) {
        const child = list.children[i];

        const endX = child.offsetWidth;
        const endY = child.offsetTop + child.offsetHeight / 2;
        this.curve.drawLine(
          ctx,
          startPosX,
          startPosY,
          endX,
          endY,
          .5,
          true
        );
      }
    }, 100);
    //
    // });

  }

  showModal(party?) {
    this.service.getMainListItemMembers(this.mainParty.id, party.id, party.persons_ids.join(',')).pipe(take(1)).subscribe((resp: any) => {
      // console.log(resp);
      resp.result.map(member => {
        if (member.from) {
          member.from = member.from.split('-')[0];
        }
      });
      this.partiesIsOpen.emit({
        members: resp.result,
        party_logo: party.logo,
        party_title: party.title
      });
    });
  }


  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    // this.subscriptions.add(
    //   this.service.getMainList().subscribe((resp: any) => {
    //     // console.log(resp)
    //     this.parties = resp.result;
    //     this.changeParties.emit(this.parties);
    //
    //     this.toggle();
    //   })
    // );
  }

  constructor(public platform: PlatformService, private curve: CurveService, private service: ElectionService) {
  }

}
