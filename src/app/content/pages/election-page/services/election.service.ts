import {Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ElectionService {
  canVote = new Subject();

  constructor(private http: HttpClient) {
  }


  getMainList() {
    // console.log(environment.APIUrl + '/elections/list')
    return this.http.get(environment.APIUrl + '/elections/list', {});
  }

  getTags() {
    return this.http.get(environment.APIUrl + '/elections/tags?slug=vr2019', {});
  }

  voteHandler(slug, party_id) {
    return this.http.post(environment.GETApiUrl + '/vote/election-party', {slug, party_id});
  }

  canVoteHandler() {
    return this.http.get(environment.GETApiUrl + '/vote/election-party/can-vote?slug=vr2019', {});
    //   .subscribe(resp => {
    //   // this.canVote.next(resp);
    // });
  }

  statsOrVoteHandler() {
    return this.http.get(environment.APIUrl + '/elections/tags-stats?slug=vr2019', {});
    // return this.http.get(environment.GETApiUrl + '/vote/election-party/stats-or-vote?slug=vr2019', {})
    //   .subscribe(resp => {
    //   // this.canVote.next(resp);
    // });
  }

  getWidgetStats() {
    return this.http.get(environment.APIUrl + '/elections/party-vote-result?election_slug=vr2019', {});
    // return this.http.get(environment.GETApiUrl + '/vote/election-party/stats-or-vote?slug=vr2019', {})
    //   .subscribe(resp => {
    //   // this.canVote.next(resp);
    // });
  }
  //
  // getParties() {
  //   return this.http.get(environment.APIUrl + '/elections/list?election_slug=vr2019', {});
  //   // return this.http.get(environment.GETApiUrl + '/vote/election-party/stats-or-vote?slug=vr2019', {})
  //   //   .subscribe(resp => {
  //   //   // this.canVote.next(resp);
  //   // });
  // }


  getTagsItem(tags_id) {
    return this.http.post(environment.APIUrl + '/elections/party-by-tag', {tags_id});
  }


  getMainListItem(id) {
    return this.http.get(environment.APIUrl + '/parties/composition?id=' + id, {});
  }

  getMainListItemMembers(mainId, id, persons_ids) {
    return this.http.get(environment.APIUrl + '/elections/party-history-list?party_id=' + id + '&election_party_id=' + mainId + '&persons_ids=' + persons_ids, {});
  }


  getTagsBottom() {
    return this.http.get(environment.APIUrl + '/elections/tags-to-parties?slug=vr2019', {});
  }

}
