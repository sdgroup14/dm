import {Component, NgZone, OnInit, Renderer2} from '@angular/core';
import {CommonAppService} from '../../../../services/common-app.service';
import {AppAuthService} from '../../../../services/app-auth.service';
import {take} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-feedback-modal',
  templateUrl: './feedback-modal.component.html',
  styleUrls: ['./feedback-modal.component.scss']
})
export class FeedbackModalComponent implements OnInit {
  isOpen = false;
  error = false;
  disabled = false;
  model = {
    text: '',
    type: 'proposal'
  };

  submit(form) {
    if (!this.model.text) {
      this.error = true;
      return;
    }
    this.service.sendFeedback(this.model.text, this.model.type).pipe(take(1)).subscribe((resp: any) => {
      this.service.notification.next({
        type: 'succsess',
        text: resp.message
      });
      this.error = false;
      this.close();
    });
    // console.log(this.model);
    // form.submitted = false;
  }

  close() {
    this.isOpen = false;
    setTimeout(() => {
      this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
      this.renderer.removeClass(document.body, 'scrollOff');
    }, 150);
    // this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
    // this.service.feedbackModal.next(false);
    // setTimeout(() => {
    //   this.model = {
    //     text: '',
    //     type: 'proposal'
    //   };
    // }, 250);
  }

  constructor(private service: CommonAppService, private renderer: Renderer2, private auth: AppAuthService, private router: Router, private zone: NgZone) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.isOpen = true;
      this.renderer.addClass(document.body, 'scrollOff');
    });
    const currentUser = this.auth.currentUserValue;
    if (currentUser) {
      this.disabled = !currentUser ? true : false;
    }
    this.auth.currentUserSubject.subscribe(data => {
      if (data) {
        this.zone.run(() => {
          this.disabled = false;
        });
      } else {
        this.disabled = true;
      }
    });
    // this.service.feedbackModal.subscribe((bool: boolean) => {
    //   this.isOpen = bool;
    // });

  }

}
