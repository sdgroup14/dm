import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ElectionPageRoutingModule} from './election-page-routing.module';
import {ElectionPageComponent} from './election-page.component';
import {FixedSocialModule} from '../../templates/widgets/fixed-social/fixed-social.module';
import {RightWidgetComponent} from './components/right-widget/right-widget.component';
import {RightWidgetToolbarModule} from '../../templates/widgets/right-widget-toolbar/right-widget-toolbar.module';
import {CompositionListComponent} from './components/composition-list/composition-list.component';
import {ComparisonComponent} from './components/comparison/comparison.component';
import {OwnPartyComponent} from './components/own-party/own-party.component';
import {OnlineVotingComponent} from './components/online-voting/online-voting.component';
import {PageMinHeightModule} from '../../../directives/page-min-height/page-min-height.module';
import {FixedWidgetModule} from '../../../directives/fixed-widget/fixed-widget.module';
import {CurveService} from '../../../services/curve.service';
import {ElectionService} from './services/election.service';
import {PartiesModalModule} from '../../templates/main-elements/parties-modal/parties-modal.module';
import {TranslateModule} from '@ngx-translate/core';
import {OnlineVoting2Component} from './components/online-voting-2/online-voting2.component';

@NgModule({
  declarations: [
    ElectionPageComponent,
    RightWidgetComponent,
    CompositionListComponent,
    ComparisonComponent,
    OwnPartyComponent,
    OnlineVotingComponent,
    OnlineVoting2Component
  ],
  imports: [
    CommonModule,
    ElectionPageRoutingModule,
    FixedSocialModule,
    RightWidgetToolbarModule,
    PageMinHeightModule,
    FixedWidgetModule,
    PartiesModalModule,
    TranslateModule
  ],
  providers: [CurveService, ElectionService]
})
export class ElectionPageModule {
}
