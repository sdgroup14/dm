import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-block-dropdown',
  templateUrl: './block-dropdown.component.html',
  styleUrls: ['./block-dropdown.component.scss']
})
export class BlockDropdownComponent implements OnInit {
  isOpen = false;
  @Output() remove = new EventEmitter();


  close(del?) {
    this.isOpen = false;
    if (del) {
      this.remove.emit(true);
    }
  }

  open() {
    setTimeout(() => {
      this.isOpen = true;
    });
  }

  constructor() {
  }

  ngOnInit() {
  }

}
