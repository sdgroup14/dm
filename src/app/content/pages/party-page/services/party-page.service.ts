import {Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Subject} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PartyPageService {
  party = new Subject();

  constructor(private http: HttpClient) {
  }

  getParty(slug) {
    return this.http.get(environment.APIUrl + '/parties/by-slug?slug=' + slug, {});
  }

  // updateParty(slug) {
  //   return this.http.get(environment.APIUrl + '/parties/by-slug?slug=' + slug, {}).subscribe(data => {
  //     this.party.next(data);
  //   });
  // }


  getStats(id, ext) {
    return this.http.get(environment.APIUrl + '/parties/composition?id=' + id + '&workplace_ext_id=' + ext, {});
  }

  getMembers(id, ext) {
    return this.http.get(environment.APIUrl + '/parties/members?id=' + id + '&workplace_ext_id=' + ext, {});
  }

  getMoreArticlesWidget(slug) {
    const query = slug ? '?party_id=' + slug : '';
    return this.http.get(environment.APIUrl + '/materials/party' + query, {});
  }

  getStatistic(id, ext) {
    return this.http.get(environment.APIUrl + '/parties/stat-improve?party_id=' + id + '&workplace_ext_id=' + ext, {});
  }

  getMissings(id, ext) {
    return this.http.get(environment.APIUrl + '/parties/stat-votes?party_id=' + id + '&workplace_ext_id=' + ext, {});
  }

  getInfluence(id, ext) {
    return this.http.get(environment.APIUrl + '/parties/stat-influence?party_id=' + id + '&workplace_ext_id=' + ext, {});
  }

  getSample(id) {
    return this.http.get(environment.APIUrl + '/workplace-exts/dict-party?party_id=' + id, {});
  }

  getTop(id, ext) {
    return this.http.get(environment.APIUrl + '/parties/stat-main?party_id=' + id + '&workplace_ext_id=' + ext, {});
  }

  getLawProblems(id) {
    return this.http.get(environment.APIUrl + '/materials/convict-party?party_id=' + id, {});
  }


  canVote(token, id) {
    const apiOptions = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', token)
    };
    // const query = page ? '&page=' + page : '';
    return this.http.get(environment.GETApiUrl + '/vote/party-user/stats-or-vote?id=' + id, apiOptions);
  }

}
