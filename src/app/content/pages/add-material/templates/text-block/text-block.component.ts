import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-text-block',
  templateUrl: './text-block.component.html',
  styleUrls: ['./text-block.component.scss']
})
export class TextBlockComponent implements OnInit {
  text: any;
  editorOptions = {
    forced_root_block: '',
    inline: true,
    menubar: false,
    toolbar: false,
    contextmenu: false,
    plugins: ['quickbars link paste'],
    paste_as_text: true,
    quickbars_insert_toolbar: '',
    quickbars_selection_toolbar: 'bold quicklink'
  };

  @Input() lang: string;

  @Output() newValue = new EventEmitter();

  @Input() set changeValue(text) {
    if (text) {
      this.text = text;
      this.newValue.emit(this.text);
    }
  }


  constructor() {
  }

  ngOnInit() {
  }

}
