import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ArticleTagsComponent} from './article-tags.component';

@NgModule({
  declarations: [ArticleTagsComponent],
  imports: [
    CommonModule
  ],
  exports: [ArticleTagsComponent]
})
export class ArticleTagsModule { }
