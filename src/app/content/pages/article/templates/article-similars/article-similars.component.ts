import {Component, Input, OnInit} from '@angular/core';
import {DateService} from '../../../../../services/date.service';

@Component({
  selector: 'app-article-similars',
  templateUrl: './article-similars.component.html',
  styleUrls: ['./article-similars.component.scss']
})
export class ArticleSimilarsComponent implements OnInit {
  @Input() similars: any[];
  @Input() title: string;

  constructor(public dateService: DateService) {
  }

  // up(){
  //   document.body.scrollIntoView();
  // }

  ngOnInit() {
  }

}
