
import {TranslateLoader} from '@ngx-translate/core';
declare var require: any;
import {makeStateKey, StateKey, TransferState} from '@angular/platform-browser';
import {Observable} from 'rxjs';

const fs = require('fs');

export class TranslateServerLoader implements TranslateLoader {

  constructor(private transferState: TransferState) {
  }

  public getTranslation(lang: string): Observable<any> {

    return new Observable(observer => {

      const jsonData = JSON.parse(fs.readFileSync(`./dist/browser/assets/i18n/${lang}.json`, 'utf8'));

      // Here we save the translations in the transfer-state
      const key: StateKey<number> = makeStateKey<number>('transfer-translate-' + lang);
      this.transferState.set(key, jsonData);
      observer.next(jsonData);
      observer.complete();
    });
  }
}
