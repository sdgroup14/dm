import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-missings',
  templateUrl: './missings.component.html',
  styleUrls: ['./missings.component.scss']
})
export class MissingsComponent implements OnInit {
  missings: any;

  @Input() set setVoting(data) {
    if (Object.keys(data).length) {
      this.missings = data;
      // console.log(this.missings)
    }
  }

  constructor() {
  }

  ngOnInit() {
  }

}
