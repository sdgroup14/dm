import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-langs-widget',
  templateUrl: './langs-widget.component.html',
  styleUrls: ['./langs-widget.component.scss']
})
export class LangsWidgetComponent implements OnInit, AfterViewInit {
  @Input() set lang(lang) {
    const newLang = this.langs.filter(l => {
      return l.id === lang;
    })[0];
    this.switchLang(newLang);
  }

  @Output() switch = new EventEmitter();

  langs = [
    {
      title: 'укр',
      selected: true,
      id: 'ua'
    },
    {
      title: 'рус',
      selected: false,
      id: 'ru'
    }
  ];
  activeLang = '';

  switchLang(lang) {
    lang.selected = true;
    this.langs.map(l => {
      if (l.id !== lang.id) {
        l.selected = false;
      }
    });
    this.activeLang = lang.id;
    this.switch.emit(lang.id);
  }

  constructor() {
  }

  ngAfterViewInit() {
  }

  ngOnInit() {
  }

}
