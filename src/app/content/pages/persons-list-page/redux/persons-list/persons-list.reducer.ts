import {Action, createReducer, on} from '@ngrx/store';
import * as PersonsActions from './persons-list.actions';
import PersonsState, {initializeState} from './persons-list.state';

export const intialState = initializeState();

const reducer = createReducer(
  intialState,
  on(PersonsActions.GetAll, (state: PersonsState, {payload}) => {
    return {...state, Persons: payload};
  }),
);

export function PersonsReducer(state: PersonsState | undefined, action: Action) {
  return reducer(state, action);
}
