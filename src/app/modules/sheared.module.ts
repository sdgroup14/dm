import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ArticleAuthorComponent} from '../content/templates/elements/article-author/article-author.component';
import {SafeUrlPipeModule} from '../pipes/safe-url/safe-url-pipe.module';

@NgModule({
  declarations: [
    ArticleAuthorComponent,
  ],
  imports: [
    CommonModule,
    SafeUrlPipeModule
  ],
  exports: [
    CommonModule,
    PerfectScrollbarModule,
    ArticleAuthorComponent,
    SafeUrlPipeModule
  ]
})
export class ShearedModule {
}
