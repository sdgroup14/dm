import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LawRootComponent} from './law-root/law-root.component';

const routes: Routes = [
  {
    path: ':slug',
    component: LawRootComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LawPageRoutingModule { }
