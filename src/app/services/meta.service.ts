import {Inject, Injectable} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {environment} from '../../environments/environment';
import {Router} from '@angular/router';
import {DOCUMENT} from '@angular/common';
import {PlatformService} from './platform.service';
import {CookieService} from './cookies-service.service';

@Injectable({
  providedIn: 'root'
})
export class MetaService {

  constructor(private meta: Meta,
              private platform: PlatformService,
              private titleService: Title,
              private cookie: CookieService,
              private router: Router,
              // private renderer2: Renderer2,
              @Inject(DOCUMENT) private doc) {
  }

  setMeta(title, description, keywords, img, url, nofollow?) {
    let lang = this.cookie.get('applang');
    if (!lang) {
      const checkRU = this.router.url.split('/')[1];
      lang = checkRU === 'ru' ? 'ru' : 'uk';
    }
    this.titleService.setTitle(title);

    const link: HTMLLinkElement = this.doc.createElement('link');
    link.setAttribute('rel', 'canonical');
    const robotsContent = nofollow ? 'noindex, follow' : 'index, follow';
    let canonicalUrl = environment.domain + this.router.url;
    if (canonicalUrl.indexOf('?') > 0) {
      // robotsContent = 'noindex, follow';
      canonicalUrl = environment.domain + this.router.url.split('?')[0];
    }
    link.setAttribute('href', canonicalUrl);
    if (!this.platform.check()) {
      this.doc.head.appendChild(link);
    }

    this.meta.addTag({name: 'description', content: description});
    this.meta.addTag({name: 'keywords', content: keywords});
    this.meta.addTag({name: 'image_src', content: img});

    this.meta.addTag({name: 'robots', content: robotsContent});

    this.meta.addTag({name: 'twitter:site', content: '@democratium_net'});
    this.meta.addTag({name: 'twitter:card', content: 'summary_large_image'});

    this.meta.addTag({name: 'twitter:title', content: title});
    this.meta.addTag({name: 'twitter:description', content: description});
    this.meta.addTag({name: 'twitter:creator', content: '@democratium_net'});
    this.meta.addTag({name: 'twitter:image', content: img});
    this.meta.addTag({name: 'twitter:url', content: environment.domain + url});

    this.meta.addTag({property: 'og:type', content: 'website'});
    this.meta.addTag({property: 'og:title', content: title});
    this.meta.addTag({property: 'og:description', content: description});
    this.meta.addTag({property: 'og:url', content: environment.domain + url});
    // console.log(lang)
    this.meta.addTag({property: 'og:locale', content: lang + '_' + (lang === 'ru' ? 'ru' : 'ua').toUpperCase()});
    this.meta.addTag({property: 'og:site_name', content: 'democratium.net'});
    this.meta.addTag({property: 'og:image', content: img});
    this.meta.addTag({property: 'og:image:type', content: 'image/jpeg'});
    this.meta.addTag({property: 'og:image:width', content: '1200'});
    this.meta.addTag({property: 'og:image:height', content: '630'});
    this.meta.addTag({property: 'og:image:secure_url', content: img});


    this.meta.addTag({property: 'fb:app_id', content: '1387310921371182'});
  }

}
