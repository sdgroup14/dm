import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StatisticComponent} from './statistic.component';
import {SheredTranslateModule} from '../../../../../modules/shered-translate.module';
import {EmpyListStringModule} from '../../../../templates/elements/empy-list-string/empy-list-string.module';
import {SheredTooltipModule} from '../../../../../modules/shered-tooltip.module';

@NgModule({
  declarations: [StatisticComponent],
  imports: [
    CommonModule,
    SheredTranslateModule,
    EmpyListStringModule,
    SheredTooltipModule
  ],
  exports: [StatisticComponent]
})
export class StatisticModule { }
