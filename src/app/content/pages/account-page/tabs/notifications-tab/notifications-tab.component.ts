import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {PerfectScrollbarDirective} from 'ngx-perfect-scrollbar';
import {take} from 'rxjs/operators';
import {AccountPageService} from '../../services/account-page.service';
import {DateService} from '../../../../../services/date.service';
import {CommonAppService} from '../../../../../services/common-app.service';
import {Subscription} from 'rxjs';
import {PlatformService} from '../../../../../services/platform.service';

@Component({
  selector: 'app-notifications-tab',
  templateUrl: './notifications-tab.component.html',
  styleUrls: ['./notifications-tab.component.scss']
})
export class NotificationsTabComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  @ViewChild('messagesEl', {read: PerfectScrollbarDirective, static: false}) messagesEl: PerfectScrollbarDirective;

  text = '';
  messages: any = [];
  init = false;

  sendMessage() {
    const data = {
      type: 'user_cab',
      text: this.text
    };
    this.service.sendMessage(data).pipe(take(1)).subscribe((resp: any) => {
      console.log(resp);
      this.messages.push({
        side: 'user',
        text: this.text,
        created_at: Date.now() / 1000
      });
      console.log(this.messages);
      this.text = '';
      // setTimeout(() => {
      //   this.app_service.notification.next({
      //     type: 'succsess',
      //     text: resp.message
      //   });
      // }, 300);
      setTimeout(() => {
        this.messagesEl.update();
        this.messagesEl.scrollToY(99999999999);
      });
    });

  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  constructor(private service: AccountPageService, private platform: PlatformService, public date_s: DateService) {
  }

  ngOnInit() {
    this.service.getMessages().pipe(take(1)).subscribe((resp: any) => {
      // console.log(resp);
      // resp.result.data.map(it => {
      //   it.date =
      // })
      if (this.platform.check()) {
        this.messages = resp.result.data.reverse();
        this.init = true;
        setTimeout(() => {
          this.messagesEl.update();
          this.messagesEl.scrollToY(9999999999);
        });
      }

    });
    this.service.resetNotif().pipe(take(1)).subscribe((resp: any) => {
      // console.log(resp);
    });

  }

}
