import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {MarterialRootService} from '../../../pages/materials-page/service/marterial-root.service';
import {Subscription} from 'rxjs';
import {delay} from 'rxjs/operators';
import {PlatformService} from '../../../../services/platform.service';

@Component({
  selector: 'app-attracted-widget',
  templateUrl: './attracted-widget.component.html',
  styleUrls: ['./attracted-widget.component.scss']
})
export class AttractedWidgetComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  persons: any = [];
  init = false;

  @Input() set changeAttracted(array) {
    this.persons = array;
    setTimeout(() => {
      this.init = true;
    }, 500);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  constructor() {
  }

  ngOnInit() {
    // if (this.platform.check()) {
    //   this.subscriptions.add(this.service.getWidgetAttracted().pipe(delay(1)).subscribe((resp: any) => {
    //     this.persons = resp.result;

    //   }));
    // }


  }

}
