import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-add-block-btn',
  templateUrl: './add-block-btn.component.html',
  styleUrls: ['./add-block-btn.component.scss']
})
export class AddBlockBtnComponent implements OnInit {
  isOpen = false;
  @Output() addBlock = new EventEmitter();
  templateTexts: any = {};

  open() {
    setTimeout(() => {
      this.isOpen = true;
    });
  }

  close() {
    this.isOpen = false;
  }

  add(type) {

    this.translate.get('add-material-page.blocks').pipe(take(1)).subscribe(data => {
      const item: any = {
        type
      };
      if (type === 2) {
        item.text_ru = this.templateTexts.text_ru;
        item.text_ua = this.templateTexts.text_ua;
      } else if (type === 1) {
        item.h2_ru = this.templateTexts.h2_ru;
        item.h2_ua = this.templateTexts.h2_ua;
      } else if (type === 6) {
        item.cite_ua = this.templateTexts.cite_ua;
        item.cite_ru = this.templateTexts.cite_ru;
      } else if (type === 4 || type === 5 || type === 7 || type === 3) {
        item.caption_ru = this.templateTexts.caption_ru;
        item.caption_ua = this.templateTexts.caption_ua;
      }
      this.addBlock.emit(item);
      this.close();
    });

  }

  constructor(private translate: TranslateService) {
  }

  ngOnInit() {
    this.translate.get('add-material-page.blocks').pipe(take(1)).subscribe(data => {
      this.templateTexts = data;
    });
  }

}
