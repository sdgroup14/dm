import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PartyPageComponent} from './party-page.component';

const routes: Routes = [
  {
    path: ':slug',
    component: PartyPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PartyPageRoutingModule { }
