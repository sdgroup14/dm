import {Component, Input, OnInit} from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-back-btn-fixed',
  templateUrl: './back-btn-fixed.component.html',
  styleUrls: ['./back-btn-fixed.component.scss']
})
export class BackBtnFixedComponent implements OnInit {
  @Input() rotate?: boolean;

  constructor(private _location: Location) {
  }

  clickHandler() {
    if (this.rotate) {
      document.body.scrollIntoView({behavior: 'smooth', block: 'start'});
    } else {
      this._location.back();
    }
  }

  ngOnInit() {
  }

}
