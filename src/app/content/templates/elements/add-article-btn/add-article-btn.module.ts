import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AddArticleBtnComponent} from './add-article-btn.component';
import {SheredTranslateModule} from '../../../../modules/shered-translate.module';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [AddArticleBtnComponent],
  imports: [
    CommonModule,
    RouterModule,
    SheredTranslateModule
  ],
  exports: [AddArticleBtnComponent]
})
export class AddArticleBtnModule { }
