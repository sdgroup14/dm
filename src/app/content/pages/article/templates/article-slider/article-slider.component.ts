import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {PlatformService} from '../../../../../services/platform.service';

@Component({
  selector: 'app-article-slider',
  templateUrl: './article-slider.component.html',
  styleUrls: ['./article-slider.component.scss']
})
export class ArticleSliderComponent implements OnInit {
  // @Input() slider: any;
  @ViewChild('slider', {static: false}) slider;

  @Input() set content(block) {
    this.slides = block.slides;
    if(this.platform.check()){
      setTimeout(() => {
        this.slider.nativeElement.swiper.update();
      }, 10);
    }

  }

  i = 0;
  config = {
    observable: true,
    autoHeight: true,
    navigation: {
      nextEl: '.button-next',
      prevEl: '.button-prev'
    },
  };
  slides = [];

  constructor(private platform: PlatformService) {
  }

  ngOnInit() {

  }

}
