import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SheredTranslateModule} from '../../../../modules/shered-translate.module';
import {FinancialHelpComponent} from './financial-help.component';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [FinancialHelpComponent],
  imports: [
    CommonModule,
    SheredTranslateModule,
    RouterModule
  ],
  exports: [FinancialHelpComponent]
})
export class FinancialHelpModule {
}
