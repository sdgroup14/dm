import {Injectable} from '@angular/core';
import {CookieService} from './cookies-service.service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ShareService {

  openWindow(link, param?) {
    const w = 560;
    const h = 640;
    const left = (screen.width / 2) - (w / 2);
    const top = (screen.height / 2) - (h / 2);
    let _l = link;
    if (param) {
      _l = _l + encodeURIComponent('?' + param);
    }
    return window.open(_l, 'window', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
  }

  copyText(val, query?) {
    if (typeof val === 'boolean') {
      const url = window.location.href;
      const urlArray = url.split('?');
      console.log(urlArray);
      if (urlArray.length > 1) {
        val = urlArray[0];
      } else {
        val = url;
      }
    }
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    if (query) {
      selBox.value = val + '?' + query;
    }
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  langPath() {
    const path = '';
    const lang = this.cookie.get('applang');
    if (lang) {
      if (lang === 'uk') {
        return '';
      }
      return '/' + lang;
    } else {
      const url = this.router.url.split('/')[1];
      return url === '(ua)' ? '/' + url : '';
    }
    return path;
  }

  // encodeHandler(href) {
  //   return environment.domain + this._common.langPath() + '/article/' + encodeURI(href);
  // }

  constructor(private cookie: CookieService, private router: Router) {
  }
}
