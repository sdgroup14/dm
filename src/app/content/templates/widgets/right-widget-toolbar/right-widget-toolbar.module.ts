import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RightWidgetToolbarComponent} from './right-widget-toolbar.component';
import {SocialWidgetModule} from '../social-widget/social-widget.module';
import {BtnCloseModule} from '../../elements/btn-close/btn-close.module';
import {SheredTooltipModule} from '../../../../modules/shered-tooltip.module';
import {SheredTranslateModule} from '../../../../modules/shered-translate.module';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [RightWidgetToolbarComponent],
  imports: [
    CommonModule,
    SocialWidgetModule,
    BtnCloseModule,
    SheredTooltipModule,
    SheredTranslateModule,
    RouterModule
  ],
  exports: [RightWidgetToolbarComponent]
})
export class RightWidgetToolbarModule { }
