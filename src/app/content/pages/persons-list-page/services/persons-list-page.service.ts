import {Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {Person} from '../models/person.model';
import * as PersonsActions from '../redux/persons-list/persons-list.actions';
import {Store} from '@ngrx/store';
import PersonsState from '../redux/persons-list/persons-list.state';

@Injectable({
  providedIn: 'root'
})
export class PersonsListPageService {
  // persons = new Subject();

  constructor(private  http: HttpClient, private store: Store<PersonsState>) {
  }

  getList(params) {
    let urlParams = params.page ? '?page=' + params.page : '?';
    urlParams += params.workplace_id ? '&workplace_id=' + params.workplace_id : '';
    urlParams += params.region_id ? '&region_id=' + params.region_id : '';
    urlParams += params.party_id ? '&party_id=' + params.party_id : '';
    urlParams += params.city_id ? '&city_id=' + params.city_id : '';
    urlParams += params.district_id ? '&district_id=' + params.district_id : '';
    urlParams += params.search !== '' ? '&search=' + params.search : '';
    urlParams += params.order_by !== '' ? '&order=' + params.order_by : '';
    urlParams += params.direction !== '' ? '&direction=' + params.direction : '';
    return this.http.get(environment.APIUrl + '/persons/list' + urlParams, {})
      .pipe(
        map((resp: any) => resp.result))
      .subscribe(data => {
        const list: Person[] = [];
        data.list.map(p => list.push(new Person(
          p.name,
          p.sirname,
          p.party,
          p.avatar,
          p.rating,
          p.for,
          p.against
        )));
        this.store.dispatch(PersonsActions.GetAll({payload: list}));
      });
  }

}
