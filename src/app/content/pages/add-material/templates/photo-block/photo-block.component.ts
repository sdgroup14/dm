import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-photo-block',
  templateUrl: './photo-block.component.html',
  styleUrls: ['./photo-block.component.scss']
})
export class PhotoBlockComponent implements OnInit {
  @Input() descrRu: string;
  @Input() descrUa: string;
  @Input() lang: any;
  @Input() img: any;
  @Input() formats: any;
  @Input() maxSize: number;
  @Input() minWidth: number;

  @Output() newImg = new EventEmitter();
  @Output() newDescrRu = new EventEmitter();
  @Output() newDescrUa = new EventEmitter();

  error = '';

  getPhoto(e) {
    this.error = null;
    const file = e.target.files[0];
    if (file) {
      const fileExtension = file.name.replace(/^.*\./, '');
      const checkFormat = this.formats.filter(format => {
        return format === fileExtension.toLowerCase();
      });
      if (checkFormat.length) {
        if (file.size > this.maxSize) {
          this.translate.get('input-photo.size-error').pipe(take(1)).subscribe(data => {
            this.error = data + ' (' + this.maxSize / 1000000 + 'mb)';
          });
        } else {
          const reader = new FileReader();
          const that = this;
          reader.onload = (_e: any) => {
            const img = new Image();
            img.src = _e.target.result;

            img.onload = () => {
              const w = img.width;
              if (w < this.minWidth) {
                this.translate.get('input-photo.min-size-error').pipe(take(1)).subscribe(data => {
                  that.error = data + ' ' + this.minWidth + 'px';
                });
              } else {
                this.img = file;
                that.newImg.emit(file);
                e.target.value = '';
              }
            };
          };
          reader.readAsDataURL(file);
        }
      } else {
        this.translate.get('input-photo.formats-error').pipe(take(1)).subscribe(data => {
          this.error = data + ' (' + this.formats.join(', ') + ')';
        });
      }
    }
  }

  constructor(private translate: TranslateService) {
  }

  ngOnInit() {
  }

}
