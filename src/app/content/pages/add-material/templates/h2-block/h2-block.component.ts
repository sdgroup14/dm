import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-h2-block',
  templateUrl: './h2-block.component.html',
  styleUrls: ['./h2-block.component.scss']
})
export class H2BlockComponent implements OnInit {
  value = '';

  @Output() newValue = new EventEmitter();

  @Input() set changeValue(text) {
    if (text) {
      this.value = text;
      this.newValue.emit(this.value);
    }
  }

  constructor() { }

  ngOnInit() {
  }

}
