import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EmpyListStringComponent} from './empy-list-string.component';
import {SheredTranslateModule} from '../../../../modules/shered-translate.module';

@NgModule({
  declarations: [EmpyListStringComponent],
  imports: [
    CommonModule,
    SheredTranslateModule
  ],
  exports: [EmpyListStringComponent]
})
export class EmpyListStringModule { }
