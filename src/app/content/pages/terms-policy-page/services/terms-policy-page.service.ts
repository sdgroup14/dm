import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
// import {environment} from '../../../../../environments/environment';
import {Subject} from 'rxjs';
import {environment} from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TermsPolicyPageService {
  data = new Subject();

  constructor(private  http: HttpClient) {
    // this.get();
  }

  get(url) {
    return this.http.get(environment.APIUrl + url, {});
  }

  getFile(url) {
    return this.http.get(url, {});
  }


}
