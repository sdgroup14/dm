import { Injectable } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartyListPageService {
  persons = new Subject();

  constructor(private  http: HttpClient) { }

  getList(params) {
    let urlParams = params.page ? '?page=' + params.page : '?';
    // urlParams += params.workplace_id ? '&workplace_id=' + params.workplace_id : '';
    // urlParams += params.region_id ? '&region_id=' + params.region_id : '';
    // urlParams += params.party_id ? '&party_id=' + params.party_id : '';
    // urlParams += params.city_id ? '&city_id=' + params.city_id : '';
    urlParams += params.workplace_id ? '&workplace_id=' + params.workplace_id : '';
    urlParams += params.region_id ? '&region_id=' + params.region_id : '';
    urlParams += params.city_id ? '&city_id=' + params.city_id : '';
    urlParams += params.district_id ? '&district_id=' + params.district_id : '';
    urlParams += params.workplace_ext_id ? '&workplace_ext_id=' + params.workplace_ext_id : '';
    urlParams += params.search !== '' ? '&search=' + params.search : '';
    urlParams += params.order_by !== '' ? '&order=' + params.order_by : '';
    urlParams += params.direction !== '' ? '&direction=' + params.direction : '';
    // urlParams += params.is_improve !== '' ? '&is_improve=' + params.is_improve : '';
    return this.http.get(environment.APIUrl + '/parties/list' + urlParams, {});
  }

  getListNext(params) {
    let urlParams = params.page ? '?page=' + params.page : '?';
    // urlParams += params.workplace_id ? '&workplace_id=' + params.workplace_id : '';
    // urlParams += params.region_id ? '&region_id=' + params.region_id : '';
    // urlParams += params.party_id ? '&party_id=' + params.party_id : '';
    // urlParams += params.city_id ? '&city_id=' + params.city_id : '';
    urlParams += params.workplace_id ? '&workplace_id=' + params.workplace_id : '';
    urlParams += params.region_id ? '&region_id=' + params.region_id : '';
    urlParams += params.city_id ? '&city_id=' + params.city_id : '';
    urlParams += params.district_id ? '&district_id=' + params.district_id : '';
    urlParams += params.workplace_ext_id ? '&workplace_ext_id=' + params.workplace_ext_id : '';
    urlParams += params.search !== '' ? '&search=' + params.search : '';
    urlParams += params.order_by !== '' ? '&order=' + params.order_by : '';
    urlParams += params.direction !== '' ? '&direction=' + params.direction : '';
    // urlParams += params.is_improve !== '' ? '&is_improve=' + params.is_improve : '';
    return this.http.get(environment.APIUrl + '/parties/list' + urlParams, {}).subscribe(resp => {
      this.persons.next(resp);
    });
  }
}
