import {Component, OnDestroy, OnInit} from '@angular/core';
import {AccountPageService} from '../../services/account-page.service';
import {take} from 'rxjs/operators';
import {CommonAppService} from '../../../../../services/common-app.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';

const dataModel = ['nickname', 'email', 'birthday', 'photo'];

@Component({
  selector: 'app-profile-tab',
  templateUrl: './profile-tab.component.html',
  styleUrls: ['./profile-tab.component.scss']
})
export class ProfileTabComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  person: any = {};
  errors: any = {};

  submit() {
    this.router.navigate([{outlets: {modals: 'preloader'}}], {skipLocationChange: true});
    const api_data: any = {};
    for (const key in this.person) {
      const value = this.person[key];
      if (key === 'photo' && typeof value === 'string') {

      } else if (dataModel.indexOf(key) >= 0 && value !== undefined && value !== null && value) {
        api_data[key] = value;
        // apiData.append(option, article[key]); // тут подправить, если проверяется несколько объектов
      }
    }
    this.service.saveProfile(api_data).pipe(take(1)).subscribe((resp: any) => {
      // console.log(resp);
      setTimeout(() => {
        this.app_service.notification.next({
          type: 'succsess',
          text: resp.message
        });
        this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
      }, 300);
    }, error => {
      const errors = error.error.errors;
      // console.log(errors);
      for (const _error in errors) {
        this.errors[_error] = errors[_error];
      }
      setTimeout(() => {
        this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
      }, 300);
    });
  }
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  constructor(private service: AccountPageService, private router: Router, private app_service: CommonAppService) {
  }

  ngOnInit() {
    this.service.getProfile().pipe(take(1)).subscribe((resp: any) => {
      // console.log(resp);
      this.person = resp.result;
    });
  }

}
