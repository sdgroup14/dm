import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ElectionService} from '../../services/election.service';

@Component({
  selector: 'app-comparison',
  templateUrl: './comparison.component.html',
  styleUrls: ['./comparison.component.scss']
})
export class ComparisonComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  rows: any = [];
  parties: any = [];
  fullView = false;

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  constructor(private service: ElectionService) {
  }

  ngOnInit() {

    this.subscriptions.add(
      this.service.getTagsBottom().subscribe((resp: any) => {
        // console.log(resp);
        this.parties = resp.result.parties;
        // this.rows = resp.result.tags;
        for (const tag in resp.result.tags) {
          this.rows.push({
            title: tag,
            values: resp.result.tags[tag]
          });
        }
        // console.log(this.parties);
        // console.log(this.rows);
        // this.parties = resp.result;
      })
    );
  }

}
