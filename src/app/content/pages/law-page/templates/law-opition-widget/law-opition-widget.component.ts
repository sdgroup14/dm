import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {PlatformService} from '../../../../../services/platform.service';

// import {LawRootService} from '../../services/law-root.service';

@Component({
  selector: 'app-law-opition-widget',
  templateUrl: './law-opition-widget.component.html',
  styleUrls: ['./law-opition-widget.component.scss']
})
export class LawOpitionWidgetComponent implements OnInit, AfterViewInit {
  @ViewChild('canvas', {static: false}) canvas: ElementRef;
  @ViewChild('contentEl', {static: false}) contentEl: ElementRef;
  @ViewChild('row1', {static: false}) row1: ElementRef;
  @ViewChild('row2', {static: false}) row2: ElementRef;
  ctx: any;
  data: any = {};
  init = false;
  empty = true;

// top_percent =
  @Input() set newData(data) {
    if (data.data) {
      this.empty = false;
      // console.log(data);
      data.top_percent = Math.floor(data.for_count * 100 / data.vr_sum);
      data.bottom_percent = Math.floor(data.negative_count * 100 / data.vr_sum);

      this.data = data;
      this.init = true;
      if (this.platform.check()) {
        setTimeout(() => {
          const startX = 10;
          const startY = 28;

          const el1 = this.row1.nativeElement;
          const endX = 21;
          const endY1 = el1.offsetTop + (el1.offsetHeight / 2);

          const el2 = this.row2.nativeElement;
          const endY2 = el2.offsetTop + (el2.offsetHeight / 2);
          this.ctx = (<HTMLCanvasElement>this.canvas.nativeElement).getContext('2d');


          this.canvas.nativeElement.width = this.contentEl.nativeElement.offsetWidth;
          this.canvas.nativeElement.height = this.contentEl.nativeElement.offsetHeight;
          this.drawCurve(startX, startY, endX, endY1, .2);
          this.drawCurve(startX, startY, endX, endY2, .5);
        }, 100);
      }
    } else {
      this.empty = true;

    }
  }

  constructor(public platform: PlatformService) {
  }

  drawCurve(startX, startY, endX, endY, angle) {
    const dx = endX - startX;
    const delta = 1;
    this.ctx.strokeStyle = '#C8D0D2';
    this.ctx.lineWidth = 2;
    this.ctx.beginPath();
    this.ctx.moveTo(startX, startY);
    this.ctx.bezierCurveTo(startX, startY, endX - dx * delta, endY, endX, endY);
    this.ctx.stroke();
    const startXPos = endX + 20;
    const startYPos = endY - 5;
    const endXPos = endX;
    const endYPos = endY;
    const canvas_arrow = (fromx, fromy, tox, toy) => {
      const headlen = 7;
      this.ctx.lineTo(tox, toy);
      this.ctx.lineTo(tox - headlen * Math.cos(angle - Math.PI / 5), toy - headlen * Math.sin(angle - Math.PI / 5));
      this.ctx.moveTo(tox, toy);
      this.ctx.lineTo(tox - headlen * Math.cos(angle + Math.PI / 5), toy - headlen * Math.sin(angle + Math.PI / 5));
    };
    this.ctx.beginPath();
    canvas_arrow(startXPos, startYPos, endXPos, endYPos);
    this.ctx.strokeStyle = '#C8D0D2';
    this.ctx.lineWidth = 2;
    this.ctx.stroke();
  }

  ngAfterViewInit() {
  }

  ngOnInit() {
    // this.service.getWidgets(id)

  }

}
