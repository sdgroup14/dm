import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AddPersonPageRoutingModule} from './add-person-page-routing.module';
import {AddPersonPageComponent} from './add-person-page.component';
import {FormsModule} from '@angular/forms';
import {SheredTooltipModule} from '../../../modules/shered-tooltip.module';
import {SelectSearchModule} from '../../templates/textareas/select-search/select-search.module';
import {SelectWithGroupModule} from '../../templates/textareas/select-with-groups/select-with-group.module';
import {SheredTranslateModule} from '../../../modules/shered-translate.module';
import {FixedSocialModule} from '../../templates/widgets/fixed-social/fixed-social.module';
import {FixedWidgetModule} from '../../../directives/fixed-widget/fixed-widget.module';
import {LangsWidgetModule} from '../add-material/ui/langs-widget/langs-widget.module';
import {InputPhotoModule} from '../../templates/main-elements/add-person-modal/components/input-photo/input-photo.module';
import {DatePickerModule} from '../add-material/templates/datepicker-block/date-picker.module';

@NgModule({
  declarations: [AddPersonPageComponent],
  imports: [
    CommonModule,
    AddPersonPageRoutingModule,
    FormsModule,
    SheredTooltipModule,
    SelectSearchModule,
    SelectWithGroupModule,
    SheredTranslateModule,
    FixedSocialModule,
    FixedWidgetModule,
    LangsWidgetModule,
    InputPhotoModule,
    DatePickerModule
  ]
})
export class AddPersonPageModule {
}
