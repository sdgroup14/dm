import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MaterialsRootComponent} from './materials-root/materials-root.component';

const routes: Routes = [
  {
    path: '',
    component: MaterialsRootComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaterialsPageRoutingModule { }
