import {Component, Input, OnInit} from '@angular/core';
import {ShareService} from '../../../../services/share.service';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-social-widget',
  templateUrl: './social-widget.component.html',
  styleUrls: ['./social-widget.component.scss']
})
export class SocialWidgetComponent implements OnInit {
  @Input() direction?: boolean;
  shares: any = {
    fb: 'https://www.facebook.com/dialog/share?app_id=413681322735717&display=popup&href=' + environment.domain + this.share.langPath() + '/vr2019',
    tw: 'https://twitter.com/intent/tweet?url=' + environment.domain + this.share.langPath() + '/vr2019',
    tm: 'https://telegram.me/share/url?url=' + environment.domain + this.share.langPath() + '/vr2019',
    vb: 'viber://forward?text=' + environment.domain + this.share.langPath() + '/vr2019'
  };

  constructor(public share: ShareService) {
  }

  ngOnInit() {
  }

}
