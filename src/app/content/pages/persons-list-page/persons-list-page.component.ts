import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {DateService} from '../../../services/date.service';
import {PersonsListPageService} from './services/persons-list-page.service';
import {PagePreloaderService} from '../../templates/preloaders/page-preloader/page-preloader.service';
import {MetaService} from '../../../services/meta.service';
import {ShareService} from '../../../services/share.service';
import PersonsState from './redux/persons-list/persons-list.state';
import {select, Store} from '@ngrx/store';

@Component({
  selector: 'app-persons-list-page',
  templateUrl: './persons-list-page.component.html',
  styleUrls: ['./persons-list-page.component.scss']
})
export class PersonsListPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  @ViewChild('topEl', {static: true}) topEl: ElementRef;
  filtersToggle = false;
  searchValue = '';
  lastPage = 1;
  showFilters = false;

  filterParams: any = {
    page: 1,
    workplace_id: '',
    region_id: '',
    city_id: '',
    district_id: '',
    party_id: '',
    search: '',
    order_by: 'sirname',
    direction: 'asc'
  };


  persons: any = [];

  clearSearch() {
    this.searchValue = this.filterParams.search = '';
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  changePageHandler(page) {
    this.lastPage = this.filterParams.page;
    this.filterParams.page = page;
    this.updateList();
  }

  changeWorkPlace(item) {
    this.filterParams.workplace_id = item.id;
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  changeDistrict(item) {
    this.filterParams.district_id = item.id;
    this.filterParams.city_id = '';
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }


  changeRegion(item) {
    this.filterParams.region_id = item.id;
    this.filterParams.district_id = '';
    this.filterParams.city_id = '';
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  changeCity(item) {
    this.filterParams.city_id = item.id;
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  changeParty(item) {
    this.filterParams.party_id = item.id;
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }


  switchFilters(input) {
    this.showFilters = true;
    this.filtersToggle = !this.filtersToggle;
    if (this.filtersToggle) {
      setTimeout(() => {
        input.focus();
      });
    }
  }

  searchHandler() {
    this.filtersToggle = false;
    this.searchValue = this.filterParams.search;
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  filterHandler(type) {
    if (type === this.filterParams.order_by) {
      this.filterParams.direction = this.filterParams.direction === 'desc' ? 'asc' : 'desc';
    } else {
      this.filterParams.direction = 'asc';
      this.filterParams.order_by = type;
    }
    this.updateList();
  }

  changePageArrowHandler(value) {
    this.filterParams.page += value;
    this.updateList();
  }

  updateList() {
    this.service.getList(this.filterParams);
  }


  constructor(
    private appMeta: MetaService,
    private service: PersonsListPageService,
    public dateService: DateService,
    private share: ShareService,
    private page_spinner: PagePreloaderService,
    private store: Store<PersonsState>) {
    // store.select('personsPage').subscribe(resp => {
    //   console.log(resp)
    // })
  }

  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.service.getList(this.filterParams);

    // this.filterParams.itemsPerPage = resp.per_page;
    // this.filterParams.total = resp.total;
    // this.filterParams.lastPage = resp.last_page;
    // const seo: any = resp.seo;
    // this.appMeta.setMeta(
    //   seo.title,
    //   seo.description,
    //   seo.keywords,
    //   seo.img,
    //   this.share.langPath() + '/persons'
    // );
    // this.page_spinner.hide();

    this.persons = this.store.pipe(select('personsPage'));


  }

}
