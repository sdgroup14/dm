import {Directive, ElementRef, HostListener, Renderer2} from '@angular/core';

@Directive({
  selector: '[appFixedWidget]'
})
export class FixedWidgetDirective {
  fixed = false;

  @HostListener('document:scroll', ['$event']) onScroll(e) {
    const fixedWrap = document.getElementsByClassName('fixed_wrap')[0];
    if (this.element) {
      const page: any = document.getElementsByClassName('page')[0].getElementsByClassName('container')[0];
      const stopPositionBottom = page.offsetTop + page.clientHeight + (window.innerHeight - this.element.nativeElement.offsetHeight) - 20;
      if (stopPositionBottom - (window.pageYOffset + window.innerHeight) < 0) {
        this.renderer.setStyle(this.element.nativeElement, 'position', 'absolute');
        this.renderer.setStyle(this.element.nativeElement, 'right', 0);
        this.renderer.setStyle(this.element.nativeElement, 'bottom', 0);
        this.renderer.setStyle(this.element.nativeElement, 'top', 'auto');
      } else {
        if (0 >= fixedWrap.getBoundingClientRect().top - 20) {
          this.renderer.setStyle(this.element.nativeElement, 'position', 'fixed');
          this.renderer.setStyle(this.element.nativeElement, 'top', '20px');
          this.renderer.setStyle(this.element.nativeElement, 'right', 'auto');
          this.renderer.setStyle(this.element.nativeElement, 'bottom', 'auto');
        } else {
          this.renderer.setStyle(this.element.nativeElement, 'position', 'relative');
          this.renderer.setStyle(this.element.nativeElement, 'top', 'auto');

        }
      }
    }
  }

  constructor(private element: ElementRef, private renderer: Renderer2) {
  }

}
