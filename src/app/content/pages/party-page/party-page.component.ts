import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {PartyPageService} from './services/party-page.service';
import {take} from 'rxjs/operators';
import {AppAuthService} from '../../../services/app-auth.service';
import {PagePreloaderService} from '../../templates/preloaders/page-preloader/page-preloader.service';
import {PlatformService} from '../../../services/platform.service';
import {MetaService} from '../../../services/meta.service';
import {ShareService} from '../../../services/share.service';

@Component({
  selector: 'app-party-page',
  templateUrl: './party-page.component.html',
  styleUrls: ['./party-page.component.scss']
})
export class PartyPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  modalData: any = {};
  stats: any = {};
  parties: any = [];
  members: any = [];
  top: any = {};
  head: any = {};
  static: any = {};
  missings: any = {};
  influence: any = [];
  partyId: any;
  slug: any;
  statsCheck = false;
  articles: any = [];
  checkHttpArticles = false;
  opition: any = {};
  loadOpition = false;
  sampleList: any = [];
  lawsProblemsList: any = [];

  switchSample(sample) {
    // console.log(sample);
    this.partyInitHandler({id: this.partyId}, sample.type);
  }

  openModal(data) {
    this.modalData = data;
    this.modalData.visibility = true;
  }

  closeModal() {
    this.modalData.visibility = true;
    setTimeout(() => {
      this.modalData = {};
    }, 300);
  }

  constructor(
    private appMeta: MetaService,
    private share: ShareService,
    private service: PartyPageService,
    private route: ActivatedRoute,
    private page_spinner: PagePreloaderService,
    private platform: PlatformService,
    private router: Router,
    private zone: NgZone,
    private auth: AppAuthService) {
  }

  showInfo(_slug) {
    this.router.navigate([{outlets: {modals: 'info'}}],
      {
        skipLocationChange: true,
        state: {
          slug: _slug,
          isOpen: true,
          icon: 'info',
          title: 'info'
        }
      });
  }

  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  partyInitHandler(resp, ext) {
    this.subscriptions.add(this.service.getTop(resp.id, ext).subscribe((res: any) => {
      // console.log(res);
      this.head = res.result.head;
      this.top.rating = res.result.rating;
    }));

    this.subscriptions.add(this.service.getStats(resp.id, ext).subscribe((res: any) => {
      this.parties = res.result;
      this.statsCheck = true;
      // console.log(data.result);
      // this.totalMissings = Math.floor(this.stats.voting.absent * 100 / this.stats.voting.laws_count);
    }));


    this.subscriptions.add(this.service.getMembers(resp.id, ext).subscribe((res: any) => {
      this.members = res.result;
      this.page_spinner.hide();
      // console.log(data.result);
      // this.totalMissings = Math.floor(this.stats.voting.absent * 100 / this.stats.voting.laws_count);
    }));
    if (this.platform.check()) {
      this.subscriptions.add(this.service.getMoreArticlesWidget(resp.id).subscribe((res: any) => {
        // console.log(resp);
        // if (resp) {
        this.checkHttpArticles = true;
        this.articles = res.result;
        // }
      }));
    }

    this.subscriptions.add(this.service.getStatistic(resp.id, ext).subscribe((res: any) => {
      // console.log(resp);
      // if (resp) {
      // this.checkHttpArticles = true;
      this.static = res.result;
      // }
    }));
    this.subscriptions.add(this.service.getMissings(resp.id, ext).subscribe((res: any) => {
      // console.log(resp);
      // if (resp) {
      // this.checkHttpArticles = true;
      // res.result.not_vote_percent = Math.floor(res.result.not_vote_percent);
      // res.result.absent_percent = Math.floor(res.result.absent_percent);
      this.missings = res.result;
      if (this.missings) {
        this.missings.head = this.missings.not_vote * 100 / this.missings.votes;
      }
      // this.missings.head = +().toFixed(1)) + ((missings.absent * 100 / missings.votes).toFixed(1))
      // }
    }));
    this.subscriptions.add(this.service.getInfluence(resp.id, ext).subscribe((res: any) => {
      // console.log(resp);
      // if (resp) {
      // this.checkHttpArticles = true;
      // res.result.not_vote_percent = Math.floor(res.result.not_vote_percent);
      // res.result.absent_percent = Math.floor(res.result.absent_percent);
      this.influence = res.result;
      // }
    }));


    this.auth.currentUserSubject.subscribe(_data => {
      // console.log(data);
      if (_data) {
        // console.log(data)
        this.zone.run(() => {
          this.service.canVote(_data.token, resp.id).pipe(take(1)).subscribe((mood: any) => {
            // console.log(mood);
            if (!mood.result.can_vote) {
              this.opition = mood.result;
            }
            this.loadOpition = true;
          });
        });
      } else {
        // this.canVote = true;
        this.opition = {
          can_vote: true
        };
        this.loadOpition = true;

        // this.user = null;
      }
    });
  }

  ngOnInit() {
    this.subscriptions.add(
      this.route.params.subscribe(params => {
        if (params.slug) {
          this.slug = params.slug;
          this.subscriptions.add(
            this.service.getParty(params.slug).subscribe((_resp: any) => {
              const resp = _resp.result;
              // this.person = resp;
              // console.log(resp);
              this.head = resp.head;
              this.top = resp;
              this.partyId = resp.id;


              const seo: any = resp.seo;
              // console.log(seo);
              // this.translate.get('election-page.seo').pipe(take(1)).subscribe((seo: any) => {
              this.appMeta.setMeta(
                resp.title + ' | democratium',
                seo.description,
                seo.keywords + ', ' + resp.title,
                seo.img,
                this.share.langPath() + '/party/' + params.slug
              );
              // this.material_id = resp.id;
              // this.author = resp.author;
              // this.person = resp.person;
              // this.workplace = resp.workplace;
              // this.title = resp.title;
              // this.pub_time = resp.pub_time;
              // this.blocks = resp.blocks;
              // this.tags = resp.tags;
              //
              // this.subscriptions.add(this.service.getPartyHistory(resp.id).subscribe((data: any) => {
              //   this.history = data.result;
              // }));
              //
              //


              this.subscriptions.add(this.service.getSample(resp.id).subscribe((data: any) => {
                // this.parties = data.result;
                // this.statsCheck = true;
                // console.log(data.result);
                this.sampleList = [];


                data.result.map(it => {
                  this.sampleList.push({
                    title: it.title,
                    type: it.id,
                    selected: it.is_def
                  });
                });

                let ext;
                if (this.sampleList.length === 1) {
                  this.sampleList[0].selected = true;
                }

                ext = this.sampleList.filter(it => {
                  return it.selected;
                })[0].type;


                this.partyInitHandler(resp, ext);
              }));

              this.subscriptions.add(this.service.getLawProblems(resp.id).subscribe((data: any) => {
                // console.log(data);
                this.lawsProblemsList = data.result;
              }));


            })
          );
        }
      })
    );

    // this.subscriptions.add(
    //   this.service.party.subscribe(resp => {
    //     // this.partyInitHandler(resp);
    //     // console.log()
    //   })
    // );
  }


}
