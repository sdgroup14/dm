import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MoreArticlesWidgetComponent} from './more-articles-widget.component';
import {RouterModule} from '@angular/router';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {SheredTranslateModule} from '../../../../modules/shered-translate.module';
import {NewsHtmlTemplateModule} from '../../preloaders/news-html-template/news-html-template.module';
import {AddArticleBtnModule} from '../../elements/add-article-btn/add-article-btn.module';
import {EmpyListStringModule} from '../../elements/empy-list-string/empy-list-string.module';

@NgModule({
  declarations: [MoreArticlesWidgetComponent],
  imports: [
    CommonModule,
    RouterModule,
    PerfectScrollbarModule,
    SheredTranslateModule,
    NewsHtmlTemplateModule,
    AddArticleBtnModule,
    EmpyListStringModule
  ],
  exports: [MoreArticlesWidgetComponent]
})
export class MoreArticlesWidgetModule { }
