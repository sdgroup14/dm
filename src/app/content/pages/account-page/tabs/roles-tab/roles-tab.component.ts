import {Component, OnDestroy, OnInit} from '@angular/core';
import {AccountPageService} from '../../services/account-page.service';
import {take} from 'rxjs/operators';
import {Subscription} from 'rxjs';
import {CommonAppService} from '../../../../../services/common-app.service';
import {AppAuthService} from '../../../../../services/app-auth.service';

@Component({
  selector: 'app-roles-tab',
  templateUrl: './roles-tab.component.html',
  styleUrls: ['./roles-tab.component.scss']
})
export class RolesTabComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  files1: any = [];
  files2: any = [];
  isOpen1 = false;
  isOpen2 = false;
  id1: any;
  id2: any;
  role = '';

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  constructor(private service: AccountPageService, private app_service: CommonAppService, private auth: AppAuthService) {
  }

  save(text, type) {
    // console.log(text, type);
    // console.log(this.files1);
    this.service.saveRole(type, text).pipe(take(1)).subscribe((resp: any) => {
      // console.log(resp);
      this.id1 = resp.result.id;
      this.id2 = resp.result.id;

      if (type === 'confirmed') {
        if (this.files1.length) {
          this.service.sendFile(this.files1[0], this.id1, type);
        }
      } else {
        if (this.files2.length) {
          this.service.sendFile(this.files2[0], this.id2, type);
        }
      }

    });
  }

  i1: any = 0;
  i2: any = 0;

  ngOnInit() {
    this.subscriptions.add(this.service.files.subscribe((resp: any) => {
        // console.log(resp);
        if (resp.type === 'confirmed') {
          this.i1++;
          if (this.files1.length - 1 >= this.i1) {
            this.service.sendFile(this.files1[this.i1], this.id1, resp.type);
          } else {
            // console.log('123');
            this.files1 = [];
            this.i1 = 0;
            this.id1 = 0;
            this.app_service.notification.next({
              type: 'succsess', text: resp.message
            });
          }
        } else {
          this.i2++;
          // console.log();
          if (this.files2.length - 1 >= this.i2) {
            // console.log(this.files2);
            this.service.sendFile(this.files2[this.i2], this.id2, resp.type);
          } else {
            this.files2 = [];
            this.i2 = 0;
            this.id2 = 0;
            this.app_service.notification.next({
              type: 'succsess', text: resp.message
            });
          }
        }
      }
    ));

    // const token_data = this.auth.currentUserValue;
    // this.role = token_data.user.role;
    this.subscriptions.add(this.service.getRole().subscribe((res: any) => {
      // console.log(res);
      this.role = res.result.role;
    }));
  }

}
