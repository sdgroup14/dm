import {Component, OnInit, Renderer2} from '@angular/core';
import {CommonAppService} from '../../../../services/common-app.service';
import {GoogleTranslateService} from '../../../../services/google-translate.service';
import {take} from 'rxjs/operators';
import {empty, forkJoin, of} from 'rxjs';
import {Router} from '@angular/router';

// import {take} from 'rxjs/operators';
// const topDataModel = ['person', 'region', 'district', 'city', 'workplace', 'title_ru', 'title_ua', 'pub_time', 'id', 'cover', 'sources', 'crime'];
const saveApiModel = [
  'sirname_ru',
  'sirname_ua',
  'name_ru',
  'name_ua',
  'birthday',
  'workplace_id',
  'position_id',
  'region_id',
  'district_id',
  'city_id',
  'party_id',
  'photo'];

@Component({
  selector: 'app-add-person-modal',
  templateUrl: './add-person-modal.component.html',
  styleUrls: ['./add-person-modal.component.scss']
})
export class AddPersonModalComponent implements OnInit {
  isOpen = false;
  error = false;
  disabled = false;
  lang = 'ua';
  person: any = {};
  errors: any = {};

  switchLang(lang) {
    this.lang = lang;
  }

  sirname: any;

  translateHandler(key) {
    const diff_lang = this.lang === 'ua' ? 'ru' : 'ua';
    if (!this.person[key + '_' + diff_lang] && this.person[key + '_' + this.lang]) {
      return this.google.translate(
        this.person[key + '_' + this.lang],
        this.lang === 'ua' ? 'uk' : 'ru',
        this.lang !== 'ua' ? 'uk' : 'ru'
      );
      //   .subscribe(text => {
      //   const translate_key = this.lang !== 'ua' ? 'ua' : 'ru';
      //   this.person[key + '_' + translate_key] = text;
      // });
    }
    return of('');
  }

  translate() {
    forkJoin([this.translateHandler('sirname').pipe(take(1)), this.translateHandler('name').pipe(take(1))]).pipe(take(1)).subscribe(data => {
      // console.log(data);
      const translate_key = this.lang !== 'ua' ? 'ua' : 'ru';
      if (data[0]) {
        this.person['sirname_' + translate_key] = data[0];
      }
      if (data[1]) {
        this.person['name_' + translate_key] = data[1];
      }

      // console.log(this.person);
    });
  }

  close() {
    if (this.isOpen) {
      this.isOpen = false;
      setTimeout(() => {
        this.router.navigate([{outlets: {modals: null}}], {skipLocationChange: true});
        this.renderer.removeClass(document.body, 'scrollOff');
      }, 150);
    }
  }

  changeDistrict(item) {
    this.person.district_id = item.id;
    this.person.city_id = '';
    this.errors.district_id = null;
    // console.log(this.person.district_id)
    // this.updateList();
  }


  changeRegion(item) {
    this.person.region_id = item.id;
    this.person.district_id = '';
    this.person.city_id = '';
    this.errors.region_id = null;
    // this.updateList();
  }

  submit() {
    forkJoin([this.translateHandler('sirname').pipe(take(1)), this.translateHandler('name').pipe(take(1))]).pipe(take(1)).subscribe(data => {
      // console.log(data);
      const translate_key = this.lang !== 'ua' ? 'ua' : 'ru';
      if (data[0]) {
        this.person['sirname_' + translate_key] = data[0];
      }
      if (data[1]) {
        this.person['name_' + translate_key] = data[1];
      }
      const api_data: any = {};
      for (const key in this.person) {
        const value = this.person[key];
        if (key === 'cover' && typeof value === 'string') {

        } else if (saveApiModel.indexOf(key) >= 0 && value !== undefined && value !== null && value) {
          api_data[key] = value;
          // apiData.append(option, article[key]); // тут подправить, если проверяется несколько объектов
        }
      }
      this.service.addPerson(api_data).pipe(take(1)).subscribe((resp: any) => {
        // console.log(resp);
        this.close();
        this.service.newPerson.next(resp.result);
        setTimeout(() => {
          this.person = {};
          this.errors = {};
        }, 200);
      }, error => {
        const errors = error.error.errors;
        console.log(errors);
        for (const _error in errors) {
          this.errors[_error] = errors[_error];
        }
      });
      // console.log(this.person);
    });
    // console.log(this.person);
    // this.translate();


  }

  constructor(private service: CommonAppService, private renderer: Renderer2, private router: Router, public google: GoogleTranslateService) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.isOpen = true;
      this.renderer.addClass(document.body, 'scrollOff');
    });
    // console.log('345');
    // this.service.addPersonModal.subscribe((bool: boolean) => {
    //   // console.log('1234444444');
    //   this.isOpen = bool;
    //   // console.log(this.isOpen)
    //   this.isOpen ? this.renderer.addClass(document.body, 'scrollOff') : this.renderer.removeClass(document.body, 'scrollOff');
    // });

    // setTimeout(() => {
    //   this.service.modalIsOpen.next(true);
    // }, 5000);
  }

}
