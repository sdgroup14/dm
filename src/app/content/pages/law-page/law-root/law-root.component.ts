import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
// import {take} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {LawRootService} from '../services/law-root.service';
import {DateService} from '../../../../services/date.service';
import {take} from 'rxjs/operators';
import {AppAuthService} from '../../../../services/app-auth.service';
import {AccountPageService} from '../../account-page/services/account-page.service';
import {PagePreloaderService} from '../../../templates/preloaders/page-preloader/page-preloader.service';
import {PlatformService} from '../../../../services/platform.service';
import {MetaService} from '../../../../services/meta.service';
import {ShareService} from '../../../../services/share.service';

@Component({
  selector: 'app-law-root',
  templateUrl: './law-root.component.html',
  styleUrls: ['./law-root.component.scss']
})
export class LawRootComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  showLaw = false;
  law_id = null;
  law: any = {};
  loadOpition = false;
  opitionMood: any = {};
  opitionVerity: any = {};
  bottomListResp: any = {};
  lawOpitionWidget: any = {};
  lawRatioWidget: any = {};
  material_id = 0;
  modalItems: any = [];
  specialistComments: any = [];
  role: any = '';

  // canVote = true;

  textHandler() {
    if (!this.law.full_text) {
      this.service.getLawFullText(this.law.file).subscribe((resp: any) => {
        // console.log(resp);
        this.law.full_text = resp.data;
      });
    }
    this.showLaw = !this.showLaw;
    // getLawFullText
  }

  showInfo() {
    this.router.navigate([{outlets: {modals: 'info'}}],
      {
        skipLocationChange: true,
        state: {
          slug: 'law.collision',
          isOpen: true,
          icon: 'info',
          title: 'info'
        }
      });
  }

  checkAuth() {
    const currentUser = this.auth.currentUserValue;
    if (currentUser) {
      return;
    }
    this.router.navigate([{outlets: {modals: 'auth'}}], {skipLocationChange: true});
  }

  changePage(page) {
    this.subscriptions.add(this.service.getBottomList(5, page).subscribe((data: any) => {
      this.bottomListResp.list = this.bottomListResp.list.concat(data.result.list);
      this.bottomListResp.page = data.result.current_page;
      // this.bottomListResp.current_page = data.result.current_page;
      // console.log(this.bottomListResp.list);
    }));
  }

  getVotes() {
    this.service.getVotes(this.law_id).pipe(take(1)).subscribe((resp: any) => {
      // console.log(resp);
      this.modalItems = resp.result;
      this.router.navigate([{outlets: {modals: 'law-votes-modal'}}], {skipLocationChange: true, state: {items: this.modalItems, law_id: this.law_id}});
    });
  }

  changeRatio(data) {
    this.subscriptions.add(this.service.getWidgetsRatio(this.law_id).subscribe((res: any) => {
      // this.subscriptions.add(this.service.getWidgets(this.law_id).subscribe((data: any) => {
      // this.bottomListResp = data.result;
      if (res.result.data) {
        this.lawRatioWidget = res.result;
      }
      // console.log(this.lawRatioWidget)
    }));
    // const last_data = this.lawRatioWidget;
    // this.lawRatioWidget = {};
    // // data: true
    // // gov_percent: 76
    // // is_gov_adopted: true
    // // is_users_adopted: true
    // // users_percent: 50
    // setTimeout(() => {
    //   console.log(last_data)
    //   this.lawRatioWidget = last_data;
    //   this.lawRatioWidget.users_sum = data.users_sum;
    //   this.lawRatioWidget.users_vote = data.users_vote;
    //   this.lawRatioWidget.users_vote_count = data.users_vote_count;
    // }, 100);

    // users_sum: this.verity.total,
    //   users_vote: 'for',
    //   users_vote_count: this.verity.true
  }

  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  constructor(
    private appMeta: MetaService,
    private share: ShareService,
    private route: ActivatedRoute,
    private service: LawRootService,
    private zone: NgZone,
    private page_spinner: PagePreloaderService,
    private platform: PlatformService,
    private router: Router,
    private auth: AppAuthService,
    private account_service: AccountPageService,
    public dateService: DateService) {
  }


  // user: any = {};

  ngOnInit() {
    const token_data: any = this.auth.currentUserValue;

    if (token_data) {
      // this.user = token_data.user;
      this.subscriptions.add(this.account_service.getRole().subscribe((res: any) => {
        // console.log(res);
        this.role = res.result.role;
      }));
    }

    this.subscriptions.add(
      this.route.params.subscribe(params => {
        if (params.slug) {
          this.subscriptions.add(this.service.getLaw(params.slug).subscribe((resp: any) => {
            this.law_id = resp.result.id;
            // console.log(resp);
            this.law = resp.result;
            // console.log(this.law)
            // this.author = resp.author;
            // this.person = resp.person;
            // this.workplace = resp.workplace;
            // this.title = resp.title;
            // this.pub_time = resp.pub_time;
            // this.blocks = resp.blocks;
            // this.tags = resp.tags;

            const seo: any = resp.result.seo;
            // console.log(seo);
            // this.translate.get('election-page.seo').pipe(take(1)).subscribe((seo: any) => {
            this.appMeta.setMeta(
              seo.title + ' ' + this.law.title,
              seo.description,
              seo.keywords,
              seo.img,
              this.share.langPath() + '/law' + '/' + params.slug
            );

            this.subscriptions.add(this.service.getBottomList(this.law_id).subscribe((data: any) => {
              this.bottomListResp = data.result;
              this.page_spinner.hide();
            }));





            if(this.platform.check()){
              this.subscriptions.add(this.service.getWidgetOpition(this.law_id).subscribe((res: any) => {
                // this.subscriptions.add(this.service.getWidgets(this.law_id).subscribe((data: any) => {
                // this.bottomListResp = data.result;
                if (res.result.data) {
                  this.lawOpitionWidget = res.result;
                }
                // console.log(this.lawOpitionWidget)
              }));

              this.subscriptions.add(this.service.getWidgetsRatio(this.law_id).subscribe((res: any) => {
                // this.subscriptions.add(this.service.getWidgets(this.law_id).subscribe((data: any) => {
                // this.bottomListResp = data.result;
                if (res.result.data) {
                  this.lawRatioWidget = res.result;
                }
                // console.log(this.lawRatioWidget)
              }));
              this.subscriptions.add(this.service.getSpecialisComments(this.law_id).subscribe((res: any) => {
                // // this.subscriptions.add(this.service.getWidgets(this.law_id).subscribe((data: any) => {
                // // this.bottomListResp = data.result;
                // if (!res.result.hasOwnProperty('data')) {
                //   this.lawRatioWidget = res.result;
                // }
                // console.log(res)
                this.specialistComments = res.result.list.reverse();
                // console.log(this.specialistComments)
              }));

              this.auth.currentUserSubject.subscribe(data => {
                // console.log(data);
                if (data) {
                  // console.log(data)
                  this.zone.run(() => {
                    this.service.canVoteMood(data.token, this.law_id).pipe(take(1)).subscribe((mood: any) => {
                      // console.log(mood);
                      if (!mood.result.can_vote) {
                        this.opitionMood = mood.result;
                      }
                      this.loadOpition = true;
                    });
                    this.service.canVoteVerity(data.token, this.law_id).pipe(take(1)).subscribe((mood: any) => {
                      // console.log(mood);
                      if (!mood.result.can_vote) {
                        this.opitionVerity = mood.result;
                      }
                      this.loadOpition = true;
                    });
                  });
                } else {
                  // this.canVote = true;
                  this.opitionMood = {
                    can_vote: true
                  };
                  this.loadOpition = true;

                  this.opitionVerity = {
                    can_vote: true
                  };
                  this.loadOpition = true;

                  // this.user = null;
                }
              });
            }


            // const currentUser: any = this.auth.currentUserValue;
            //
            // if (currentUser) {
            //   this.service.canVoteMood(currentUser.token, this.law_id).pipe(take(1)).subscribe((mood: any) => {
            //     console.log(mood);
            //     // this.loadOpition = true;
            //     // this.opitionMood = mood.result;
            //   });
            //   // this.service.canVoteVerity(currentUser.token, this.material_id).pipe(take(1)).subscribe((mood: any) => {
            //   //   // console.log(mood);
            //   //   this.loadOpition = true;
            //   //   this.opitionVerity = mood.result;
            //   // });
            //
            // }

          }));
        }
      })
    );


    // }
  }

}
