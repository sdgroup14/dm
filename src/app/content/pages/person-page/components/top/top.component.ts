import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.scss']
})
export class TopComponent implements OnInit {
  @Input() p: any = {};

  @Input() set person(data) {
    // console.log(data)
    if (data && data.hasOwnProperty('birthday')) {
      const years_old = new Date().getFullYear() - new Date(data.birthday * 1000).getFullYear();
      data.years_old = years_old;
      data.years_old_lastNumber = data.years_old.toString()[1];
      this.p = data;
    }
  }

  constructor() {
  }

  ngOnInit() {
  }

}
