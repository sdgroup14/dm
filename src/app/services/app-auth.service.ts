import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {PlatformService} from './platform.service';
import {TranslateService} from '@ngx-translate/core';
import {CommonAppService} from './common-app.service';
import {CookieService} from './cookies-service.service';

@Injectable({
  providedIn: 'root'
})
export class AppAuthService {
  public currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(private http: HttpClient,
              private translate: TranslateService,
              public _common: CommonAppService,
              private router: Router,
              private platform: PlatformService,
              private cookie: CookieService) {
    if (this.cookie.get('appToken')) {
      this.currentUserSubject = new BehaviorSubject(JSON.parse(this.cookie.get('appToken')));
    } else {
      this.currentUserSubject = new BehaviorSubject(null);
    }
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  public refreshToken() {
    const apiOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json')
    };
    const _refresh = this.currentUserValue.refresh;
    const _sign = this.currentUserValue.sign;
    this.currentUserSubject.next(null);
    return this.http.post<any>(`${environment.GETApiUrl}/auth/refresh`, JSON.stringify({refresh_token: _refresh, sign: _sign}), apiOptions);
  }

  loginWithSocial(token: string, provider: string, secret?: string) {
    const params = secret ? '?oauth_token=' + token + '&oauth_verifier=' + secret : '?token=' + token;
    return this.http.post<any>(environment.GETApiUrl + '/auth/' + provider + params, JSON.stringify({}))
      .subscribe((data: any) => {
        const resp = data.result;
        if (resp) {
          this.translate.get('notification-modal.login-success-text').subscribe(text => {
            this._common.notification.next({
              type: 'succsess',
              text: text + resp.user.name
            });
          });
          this._common.login.next(false);
          const d_final = new Date(resp.refresh_exp * 1000);
          this.cookie.put('appToken', JSON.stringify(resp), {
            expires: d_final
          });
          this.currentUserSubject.next(resp);
          if (provider === 'email') {
            this.router.navigate(['/']);
          }
          // console.log(localStorage.getItem('appRedirect'))
          if (localStorage.getItem('appRedirect')) {
            this.router.navigate([localStorage.getItem('appRedirect')]);
            localStorage.removeItem('appRedirect');
            localStorage.removeItem('authProvider');
          }
        }

        return resp.result;
      });
  }

  loginWithEmail(email, agreement) {
    const apiOptions = {
      headers: new HttpHeaders().set('Accept', 'application/json')
    };

    return this.http.post(environment.GETApiUrl + '/auth/send-email', {email, agreement}, apiOptions);
  }

  getUser() {
    const token = this.currentUserValue.token;
    const apiOptions = {
      headers: new HttpHeaders().set('Authorization', token)
    };
    return this.http.get(environment.GETApiUrl + '/user', apiOptions);
  }

  public clear() {
    this.cookie.remove('appToken');
    this.currentUserSubject.next(null);
    if (this.router.url.search('account') !== -1 || this.router.url.search('edit') !== -1) {
      this.router.navigate(['/']);
    }
  }

  checkExpares() {
    if (this.currentUserValue) {
      const timestamp_refresh = this.currentUserValue.refresh_exp;
      const now = Date.now();
      const exp: any = new Date(timestamp_refresh * 1000);
      if (now > timestamp_refresh * 1000) {
        this.clear();
      }
      // const timestamp = this.currentUserValue.token_exp;
      // if (now > timestamp * 1000) {
      //
      // }
    }
  }

  logout() {
    const apiOptions = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', this.currentUserValue.token)
    };
    return this.http.post<any>(
      `${environment.GETApiUrl}/auth/logout`,
      {sign: this.currentUserValue.sign, refresh: this.currentUserValue.refresh},
      apiOptions
    ).subscribe(() => {
      this.clear();
    }, () => {
      this.clear();
    });


  }
}
