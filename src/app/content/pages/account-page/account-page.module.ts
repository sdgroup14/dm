import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AccountPageRoutingModule} from './account-page-routing.module';
import {AccountPageComponent} from './account-page.component';
import {MaterialsTabComponent} from './tabs/materials-tab/materials-tab.component';
import {NotificationsTabComponent} from './tabs/notifications-tab/notifications-tab.component';
import {ProfileTabComponent} from './tabs/profile-tab/profile-tab.component';
import {RolesTabComponent} from './tabs/roles-tab/roles-tab.component';
import {InputPhotoComponent} from './components/input-photo/input-photo.component';
import {SafeUrlPipeModule} from '../../../pipes/safe-url/safe-url-pipe.module';
import {FormsModule} from '@angular/forms';
import {DatePickerModule} from '../add-material/templates/datepicker-block/date-picker.module';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {AccountPageService} from './services/account-page.service';
import {BlockDropdownModule} from '../add-material/ui/block-dropdown/block-dropdown.module';
import {AddArticleBtnModule} from '../../templates/elements/add-article-btn/add-article-btn.module';
import {SimpleSelectModule} from '../../templates/textareas/simple-select/simple-select.module';
import {SheredPaginationModule} from '../../../modules/shered-pagination.module';
import {FormComponent} from './components/form/form.component';
import {EmpyListStringModule} from '../../templates/elements/empy-list-string/empy-list-string.module';
import {SheredTranslateModule} from '../../../modules/shered-translate.module';
import {SheredTooltipModule} from '../../../modules/shered-tooltip.module';

@NgModule({
  declarations: [
    AccountPageComponent,
    MaterialsTabComponent,
    NotificationsTabComponent,
    ProfileTabComponent,
    RolesTabComponent,
    InputPhotoComponent,
    FormComponent
  ],
  imports: [
    CommonModule,
    AccountPageRoutingModule,
    SafeUrlPipeModule,
    FormsModule,
    DatePickerModule,
    PerfectScrollbarModule,
    BlockDropdownModule,
    AddArticleBtnModule,
    SimpleSelectModule,
    SheredPaginationModule,
    EmpyListStringModule,
    SheredTranslateModule,
    SheredTooltipModule
  ],
  providers: [AccountPageService]
})
export class AccountPageModule {
}
