import {Component, EventEmitter, Input, NgZone, OnInit, Output, Renderer2} from '@angular/core';
import {CommonAppService} from '../../../../services/common-app.service';
import {PlatformService} from '../../../../services/platform.service';
import {AppAuthService} from '../../../../services/app-auth.service';
import {CookieService} from '../../../../services/cookies-service.service';
import {Router} from '@angular/router';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-auth-modal',
  templateUrl: './auth-modal.component.html',
  styleUrls: ['./auth-modal.component.scss']
})

export class AuthModalComponent implements OnInit {
  isOpen = false;
  // @Output() closeModal = new EventEmitter();
  checkbox: any = false;
  model = {
    email: ''
  };
  isPost = false;
  errors: any = {};

  // @Input() set setVisibility(bool) {
  //   if (this.platform.check()) {
  //     this.isOpen = bool;
  //     this.isOpen ? this.renderer.addClass(document.body, 'scrollOff') : this.renderer.removeClass(document.body, 'scrollOff');
  //   }
  // }

  signInWithGoogle() {
    this.router.navigate([{outlets: {modals: null}}]);
    // console.log(window.location.pathname)
    localStorage.setItem('appRedirect', window.location.pathname);
    localStorage.setItem('authProvider', 'google');
    // console.log(
    //   localStorage.getItem('appRedirect'))
    window.location.href = 'https://accounts.google.com/o/oauth2/v2/auth?' +
      'client_id=539687069634-rj569qq7kfsc5m275cg7rde8s54fhmdb.apps.googleusercontent.com&' +
      'response_type=token&' +
      'scope=profile&' +
      'redirect_uri=https://democratium.net/auth';

  }

  signInWithFB() {
    this.router.navigate([{outlets: {modals: null}}]);
    localStorage.setItem('appRedirect', window.location.pathname);
    localStorage.setItem('authProvider', 'facebook');
    // alert(localStorage.getItem('appRedirect'))
    window.location.href = 'https://www.facebook.com/v4.0/dialog/oauth?' +
      'client_id=1387310921371182&' +
      'response_type=token&' +
      'redirect_uri=https://democratium.net/auth&' +
      'auth_type=rerequest&' +
      'scope=email';
  }

  signInWithTwitter() {
    this.router.navigate([{outlets: {modals: null}}]);
    localStorage.setItem('appRedirect', window.location.pathname);
    localStorage.setItem('authProvider', 'twitter');
    this.service.authTwitter().pipe(take(1)).subscribe((resp: any) => {
      window.location.href = 'https://api.twitter.com/oauth/authenticate?oauth_token=' + resp.result.oauth_token;
    });
  }

  close() {
    this.isOpen = false;
    setTimeout(() => {
      this.router.navigate([{outlets: {modals: null}}]);
      this.renderer.removeClass(document.body, 'scrollOff');
    }, 150);
  }

  submit() {
    this.appAuth.loginWithEmail(this.model.email, this.checkbox).subscribe((resp: any) => {
      if (!resp.hasOwnProperty('errors')) {
        // this.isOpen = false;
        // this.closeModal.emit(this.isOpen);
        this.common.notification.next({
          type: 'succsess',
          text: resp.result.msg
        });
        this.close();
        this.model.email = '';
        this.checkbox = false;
      } else {
        this.errors.email = resp.errors.email;
      }
    }, error => {
      const data = error.error.errors;
      for (const _error in data) {
        this.errors[_error] = data[_error][0];
      }
      console.log(this.errors);
    });
  }


  constructor(
    private renderer: Renderer2,
    public service: CommonAppService,
    private platform: PlatformService,
    private cookie: CookieService,
    private router: Router,
    public  common: CommonAppService,
    // private zone: NgZone,
    public appAuth: AppAuthService) {
  }


  ngOnInit() {
    setTimeout(() => {
      this.isOpen = true;
      this.renderer.addClass(document.body, 'scrollOff');
    });
    // this.appAuth.currentUserSubject.subscribe(data => {
    //   this.zone.run(() => {
    //     this.isOpen = false;
    //
    //     this.closeModal.emit(this.isOpen);
    //     this.isOpen ? this.renderer.addClass(document.body, 'scrollOff') : this.renderer.removeClass(document.body, 'scrollOff');
    //   });
    // });

    // this.common.authModal.subscribe((bool: boolean) => {
    //   this.isOpen = bool;
    //   this.closeModal.emit(this.isOpen);
    //   this.isOpen ? this.renderer.addClass(document.body, 'scrollOff') : this.renderer.removeClass(document.body, 'scrollOff');
    // });
  }

}
