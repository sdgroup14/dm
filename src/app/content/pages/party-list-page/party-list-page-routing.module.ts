import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PartyListPageComponent} from './party-list-page.component';

const routes: Routes = [
  {
    path: '',
    component: PartyListPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PartyListPageRoutingModule { }
