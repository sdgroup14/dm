import {Component, OnDestroy, OnInit} from '@angular/core';
import {DateService} from '../../../../../services/date.service';
import {Subscription} from 'rxjs';
import {AccountPageService} from '../../services/account-page.service';
import {take} from 'rxjs/operators';
import {CommonAppService} from '../../../../../services/common-app.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-materials-tab',
  templateUrl: './materials-tab.component.html',
  styleUrls: ['./materials-tab.component.scss']
})
export class MaterialsTabComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  lastPage = 1;
  lastStatus = '';
  articles: any = [];
  init = false;
  lists: any = [
    // {
    //   title: 'черновик',
    //   type: 'draft',
    //   selected: true
    // },
    // {
    //   title: 'на рассмотрении',
    //   type: 'review',
    //   selected: false
    // },
    // {
    //   title: 'опубликованные',
    //   type: 'publish',
    //   selected: false
    // }
  ];

  filterParams: any = {
    page: 1,
    status: 'draft'
    // workplace_id: '',
    // region_id: '',
    // city_id: '',
    // party_id: '',
    // law_workplace_id: '',
    // workplace_id: '4',
    // region_id: '',
    // city_id: '',
    // search: '',
    // order_by: 'title',
    // direction: 'asc',
    // is_improve: false
  };

  changePageHandler(page) {
    this.lastPage = this.filterParams.page;
    this.filterParams.page = page;
    this.updateList();
  }

  changeList(it) {
    // console.log(it);
    this.filterParams.status = it.type;
    this.service.getArticlesNext(this.filterParams);
  }

  updateList() {
    this.service.getArticlesNext(this.filterParams);
  }

  changePageArrowHandler(value) {
    this.filterParams.page += value;
    this.updateList();
  }

  deleteArticle(article) {
    this.service.deleteArticles(article.id).pipe(take(1)).subscribe((resp: any) => {
      const index = this.articles.indexOf(article);
      this.articles.splice(index, 1);
      console.log(resp);
      setTimeout(() => {
        this.app_service.notification.next({
          type: 'succsess',
          text: resp.message
        });
        // this.app_service.preloader.next(false);
      }, 300);
    }, error => {
      console.log(error);
      this.app_service.notification.next({
        type: 'warning',
        text: error.error.message
      });
    });
    // console.log(block);
    // if (block.hasOwnProperty('id')) {
    //   this.service.deleteBlock(block.id).subscribe(resp => {
    //     const index = this.blocks.indexOf(block);
    //     this.blocks.splice(index, 1);
    //   });
    // } else {
    //   const index = this.blocks.indexOf(block);
    //   this.blocks.splice(index, 1);
    // }
  }

  constructor(public date_s: DateService, private translate: TranslateService, private service: AccountPageService, private app_service: CommonAppService) {
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.subscriptions.add(this.translate.get('account-page.tab-materials.select-items').subscribe(content => {
      this.lists = content;
    }));
    this.lastStatus = this.filterParams.status;
    this.subscriptions.add(this.service.getArticles(this.filterParams).subscribe((resp: any) => {
      this.filterParams.itemsPerPage = resp.result.per_page;
      this.filterParams.total = resp.result.total;
      this.filterParams.lastPage = resp.result.last_page;
      this.articles = resp.result.data;
      this.init = true;
      // console.log();

    }));
    this.subscriptions.add(this.service.articles.subscribe((resp: any) => {
      this.filterParams.itemsPerPage = resp.result.per_page;
      this.filterParams.total = resp.result.total;
      this.filterParams.lastPage = resp.result.last_page;
      this.articles = resp.result.data;
      // console.log(this.articles);
    }));
  }

}
