import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ElectionService} from '../../services/election.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-own-party',
  templateUrl: './own-party.component.html',
  styleUrls: ['./own-party.component.scss']
})
export class OwnPartyComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  consignmentImgs: any = [];

  tags: any = [];

  activeTop = false;
  showError = false;

  change(tag) {
    tag.selected = !tag.selected;
    // this.activeTag = tag;

    const choosenTags = [];
    this.tags.map(t => {
      if (t.selected) {
        choosenTags.push(t.eltag_id);
      }
    });
    this.service.getTagsItem(choosenTags.join(',')).pipe(take(1)).subscribe((resp: any) => {
      this.consignmentImgs = resp.result;
      if (resp.result.length) {
        this.activeTop = true;
        this.showError = false;

      } else {
        // this.activeTop = false;
        if (choosenTags.length) {
          this.showError = true;
          this.activeTop = true;
        } else {
          this.showError = false;
          this.activeTop = false;
        }
      }

      // console.log(resp);
    });
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  constructor(private service: ElectionService) {
  }

  ngOnInit() {

    this.subscriptions.add(
      this.service.getTags().subscribe((resp: any) => {
        // console.log(resp);
        this.tags = resp.result;
        // this.tags[0].selected = true;
        // this.activeTag = this.tags[0];
      })
    );
  }

}
