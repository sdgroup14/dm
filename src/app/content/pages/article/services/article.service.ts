import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(
    private  http: HttpClient
  ) {
  }

  getArticle(slug) {
    return this.http.get(environment.APIUrl + '/materials?slug=' + slug, {});
  }

  getSimilars(slug) {
    return this.http.get(environment.APIUrl + '/materials/similar?slug=' + slug, {});
  }

  getArticles(id, person_id) {
    return this.http.get(environment.APIUrl + '/materials/person?current_id=' + id + '&person_id=' + person_id, {});
  }


  getPersonWidget(url, slug?) {
    const query = slug ? '?id=' + slug : '';
    return this.http.get(environment.APIUrl + url + query, {});
  }


  // canVoteMood(token, material_id) {
  //   const apiOptions = {
  //     headers: new HttpHeaders()
  //       .set('Content-Type', 'application/json')
  //       .set('Accept', 'application/json')
  //       .set('Authorization', token)
  //   };
  //   return this.http.post(environment.GETApiUrl + '/mood/material/can-vote', {material_id}, apiOptions);
  // }

  canVoteMood(token, id) {
    const apiOptions = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', token)
    };
    return this.http.get(environment.GETApiUrl + '/vote/material-user-improve/stats-or-vote?id=' + id, apiOptions);
  }


  canVoteVerity(token, id) {
    const apiOptions = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', token)
    };
    return this.http.get(environment.GETApiUrl + '/vote/material-user-mark/stats-or-vote?id=' + id, apiOptions);
  }


  sendOpitionMood(id, vote, token, moodUrl) {
    // console.log(token);
    const apiOptions = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', token)
    };
    return this.http.post(environment.GETApiUrl + moodUrl, {id, vote}, apiOptions);
  }

  sendOpitionVerity(id, vote, token, url) {
    // console.log(token);
    const apiOptions = {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Accept', 'application/json')
        .set('Authorization', token)
    };
    return this.http.post(environment.GETApiUrl + url, {id, vote}, apiOptions);
  }


}
