import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FeedbackModalComponent} from './feedback-modal.component';
import {BtnCloseModule} from '../../elements/btn-close/btn-close.module';
import {SheredTranslateModule} from '../../../../modules/shered-translate.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [FeedbackModalComponent],
  imports: [
    CommonModule,
    BtnCloseModule,
    FormsModule,
    ReactiveFormsModule,
    SheredTranslateModule
  ],
  exports: [FeedbackModalComponent]
})
export class FeedbackModalModule {
}
