import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {DateService} from '../../../services/date.service';
import {PartyListPageService} from './services/party-list-page.service';
import {PagePreloaderService} from '../../templates/preloaders/page-preloader/page-preloader.service';
import {MetaService} from '../../../services/meta.service';
import {ShareService} from '../../../services/share.service';

@Component({
  selector: 'app-party-list-page',
  templateUrl: './party-list-page.component.html',
  styleUrls: ['./party-list-page.component.scss']
})
export class PartyListPageComponent implements OnInit, OnDestroy {
  @ViewChild('topEl', {static: true}) topEl: ElementRef;
  private subscriptions = new Subscription();
  filtersToggle = false;
  searchValue = '';
  lastPage = 1;
  showFilters = false;

  filterParams: any = {
    page: 1,
    // workplace_id: '',
    // region_id: '',
    // city_id: '',
    // party_id: '',
    law_workplace_id: '',
    workplace_id: '4',
    region_id: '',
    city_id: '',
    search: '',
    order_by: 'members',
    direction: 'desc',
    is_improve: false
  };

  isRegion = false;
  isCity = false;
  isDistrict = false;
  isExt = true;
  parties: any = [];

  clearSearch() {
    this.searchValue = this.filterParams.search = '';
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  changePageHandler(page) {
    this.lastPage = this.filterParams.page;
    this.filterParams.page = page;
    this.updateList();
  }

  switchFilters(input) {
    this.showFilters = true;
    this.filtersToggle = !this.filtersToggle;
    if (this.filtersToggle) {
      setTimeout(() => {
        input.focus();
      });
    }
  }

  searchHandler() {
    this.filtersToggle = false;
    this.searchValue = this.filterParams.search;
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  filterHandler(type) {
    if (type === this.filterParams.order_by) {
      this.filterParams.direction = this.filterParams.direction === 'desc' ? 'asc' : 'desc';
    } else {
      this.filterParams.direction = 'asc';
      this.filterParams.order_by = type;
    }
    this.updateList();
  }

  changeWorkplace(item) {
    this.isRegion = false;
    this.isCity = false;
    this.isDistrict = false;
    this.isExt = false;
    this.lastPage = this.filterParams.page = 1;
    setTimeout(() => {
      // console.log(item);
      this.isRegion = item.is_region;
      this.isCity = item.is_city;
      this.isDistrict = item.is_district;
      this.isExt = item.is_ext;
      this.filterParams.workplace_id = item.id;
      this.filterParams.city_id = '';
      this.filterParams.region_id = '';
      this.filterParams.district_id = '';
      this.filterParams.workplace_ext_id = '';
      this.updateList();
    }, 1);
    // this.extend = [];
    // this.filterParams.city_id = '';
    // this.filterParams.law_workplace_id = '';
    // this.filterParams.year = '';
    // this.filterParams.workplace_id = item.id;
    // this.changeYears(2014, 2019);
    // if (item.extend.length) {
    //   this.extend = item.extend;
    // } else {
    //
    // }
  }

  changeDistrict(item) {
    this.filterParams.district_id = item.id;
    this.filterParams.city_id = '';
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }


  changeRegion(item) {
    this.filterParams.region_id = item.id;
    this.filterParams.district_id = '';
    this.filterParams.city_id = '';
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  changeCity(item) {
    this.filterParams.city_id = item.id;
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  changeExt(item) {
    this.filterParams.workplace_ext_id = item.id;
    this.lastPage = this.filterParams.page = 1;
    this.updateList();
  }

  changePageArrowHandler(value) {
    this.filterParams.page += value;
    this.updateList();
  }

  updateList() {
    this.service.getListNext(this.filterParams);
  }


  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  constructor(
    private appMeta: MetaService,
    private share: ShareService,
    private page_spinner: PagePreloaderService,
    private service: PartyListPageService,
    public dateService: DateService) {
  }

  ngOnInit() {
    // console.log(this.articles);
    this.subscriptions.add(this.service.getList(this.filterParams).subscribe((resp: any) => {
      this.filterParams.itemsPerPage = resp.result.per_page;
      this.filterParams.total = resp.result.total;
      this.filterParams.lastPage = resp.result.last_page;
      this.parties = resp.result.list;
      this.page_spinner.hide();

      const seo: any = resp.result.seo;
      // console.log(seo);
      // this.translate.get('election-page.seo').pipe(take(1)).subscribe((seo: any) => {
      this.appMeta.setMeta(
        seo.title,
        seo.description,
        seo.keywords,
        seo.img,
        this.share.langPath() + '/parties'
      );
    }));
    this.subscriptions.add(this.service.persons.subscribe((resp: any) => {
      this.filterParams.itemsPerPage = resp.result.per_page;
      this.filterParams.total = resp.result.total;
      this.filterParams.lastPage = resp.result.last_page;
      this.parties = resp.result.list;
      if (this.lastPage !== this.filterParams.page) {
        this.lastPage = this.filterParams.page;
        this.topEl.nativeElement.scrollIntoView({behavior: 'smooth', block: 'start'});
      }
      // console.log(this.articles);
    }));

  }

}
