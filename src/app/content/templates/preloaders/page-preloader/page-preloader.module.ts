import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PagePreloaderComponent} from './page-preloader.component';

@NgModule({
  declarations: [PagePreloaderComponent],
  imports: [
    CommonModule
  ],
  exports: [PagePreloaderComponent]
})
export class PagePreloaderModule { }
