import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MobileShareWidgetComponent} from './mobile-share-widget.component';
import {SocialWidgetModule} from '../../widgets/social-widget/social-widget.module';
import {BackBtnFixedModule} from '../../elements/back-btn-fixed/back-btn-fixed.module';

@NgModule({
  declarations: [MobileShareWidgetComponent],
  imports: [
    CommonModule,
    SocialWidgetModule,
    BackBtnFixedModule
  ],
  exports: [MobileShareWidgetComponent]
})
export class MobileShareWidgetModule { }
