import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NewsHtmlTemplateComponent} from './news-html-template.component';

@NgModule({
  declarations: [NewsHtmlTemplateComponent],
  imports: [
    CommonModule
  ], exports: [NewsHtmlTemplateComponent]
})
export class NewsHtmlTemplateModule {
}
