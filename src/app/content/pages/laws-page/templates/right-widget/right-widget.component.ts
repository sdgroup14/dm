import {AfterViewInit, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {LawsRootService} from '../../service/laws-root.service';
import {Subscription} from 'rxjs';
import {PlatformService} from '../../../../../services/platform.service';

@Component({
  selector: 'app-right-widget',
  templateUrl: './right-widget.component.html',
  styleUrls: ['./right-widget.component.scss']
})
export class RightWidgetComponent implements AfterViewInit, OnDestroy {
  private subscriptions = new Subscription();
  adoption: any = {};
  vote_compartion: any = {};
  @Input() opitionWidget: any = {};
  @Input() ratioWidget: any = {};

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  constructor(private service: LawsRootService, public platform: PlatformService) {
  }

  ngAfterViewInit() {
    if (this.platform.check()) {
      setTimeout(() => {
        try {
          (window['adsbygoogle'] = window["adsbygoogle"] || []).push({});
        } catch (e) {
          console.error(e);
        }
      }, 100);
    }
  }

  // @Input() data;

}
