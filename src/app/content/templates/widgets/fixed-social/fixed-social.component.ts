import {Component, ElementRef, HostListener, Input, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-fixed-social',
  templateUrl: './fixed-social.component.html',
  styleUrls: ['./fixed-social.component.scss']
})
export class FixedSocialComponent implements OnInit {
  @ViewChild('el', {static: true}) el: ElementRef;
  fixed = false;
  @Input() social: boolean;
  @Input() rotate: boolean;

  @HostListener('document:scroll', ['$event']) onScroll(e) {
    if (this.el) {
      if (0 >= this.el.nativeElement.getBoundingClientRect().top - 20) {
        this.fixed = true;
      } else {
        this.fixed = false;
      }
    }
  }

  constructor() {
  }

  ngOnInit() {
  }

}
