import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-article-fb-embed',
  templateUrl: './article-fb-embed.component.html',
  styleUrls: ['./article-fb-embed.component.scss']
})
export class ArticleFbEmbedComponent implements OnInit {
  @Input() content: any;
  constructor() { }

  ngOnInit() {
  }

}
