import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {take} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-input-photo',
  templateUrl: './input-photo.component.html',
  styleUrls: ['./input-photo.component.scss']
})
export class InputPhotoComponent implements OnInit {
  @Input() img: any;
  @Input() formats: any;
  @Input() maxSize: number;
  @Input() minWidth: number;

  @Output() newImg = new EventEmitter();

  error = '';

  getPhoto(e) {
    this.error = null;
    const file = e.target.files[0];
    if (file) {
      const fileExtension = file.name.replace(/^.*\./, '');
      const checkFormat = this.formats.filter(format => {
        return format === fileExtension.toLowerCase();
      });
      if (checkFormat.length) {
        if (file.size > this.maxSize) {
          this.translate.get('input-photo.size-error').pipe(take(1)).subscribe(data => {
            this.error = data + ' (' + this.maxSize / 1000000 + 'мб)';
          });
        } else {
          const reader = new FileReader(); // CREATE AN NEW INSTANCE.
          const that = this;
          reader.onload = (_e: any) => {
            const img = new Image();
            img.src = _e.target.result;

            img.onload = () => {
              const w = img.width;
              if (w < this.minWidth) {
                this.translate.get('input-photo.size-error').pipe(take(1)).subscribe(data => {
                  that.error = data + ' ' + this.minWidth + 'px';
                });
              } else {
                this.img = file;
                that.newImg.emit(file);
                e.target.value = '';
              }
            };
          };
          reader.readAsDataURL(file);
        }
      } else {
        this.translate.get('input-photo.size-error').pipe(take(1)).subscribe(data => {
          this.error = data + ' (' + this.formats.join(', ') + ')';
        });
      }
    }
  }

  constructor(private translate: TranslateService) {
  }

  ngOnInit() {
  }

}
