import {AfterViewInit, Directive, ElementRef, Renderer2} from '@angular/core';
import {PlatformService} from '../../services/platform.service';

@Directive({
  selector: '[appPageMinHeight]'
})
export class PageMinHeightDirective implements AfterViewInit {

  constructor(private element: ElementRef, private platform: PlatformService, private renderer: Renderer2) {
    // if (this.platform.check()) {
    // this.renderer.setStyle(this.element.nativeElement, 'opacity', 0);
    // }
  }

  ngAfterViewInit() {
    if (this.platform.check()) {
      setTimeout(() => {
        const page: any = document.getElementsByClassName('page')[0];
        // console.log(page)
        const widgetHeight = this.element.nativeElement.offsetHeight;
        // console.log(widgetHeight)
        // console.log(widgetHeight)
        page.style.minHeight = widgetHeight + 'px';
        page.style.boxSizing = 'content-box';
        // this.renderer.setStyle(this.element.nativeElement, 'opacity', 1);
      }, 1);
      setTimeout(() => {
        const page: any = document.getElementsByClassName('page')[0];
        // console.log(page)
        const widgetHeight = this.element.nativeElement.offsetHeight;
        // console.log(widgetHeight)
        // console.log(widgetHeight)
        page.style.minHeight = widgetHeight + 'px';
        page.style.boxSizing = 'content-box';
        // this.renderer.setStyle(this.element.nativeElement, 'opacity', 1);
      }, 1500);

    }

  }

}
