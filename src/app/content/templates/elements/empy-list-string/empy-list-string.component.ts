import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-empy-list-string',
  templateUrl: './empy-list-string.component.html',
  styleUrls: ['./empy-list-string.component.scss']
})
export class EmpyListStringComponent implements OnInit {
  @Input() text: string;

  constructor() {
  }

  ngOnInit() {
  }

}
