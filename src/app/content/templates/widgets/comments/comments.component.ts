import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {AppAuthService} from '../../../../services/app-auth.service';
import {CommonAppService} from '../../../../services/common-app.service';
import {PerfectScrollbarDirective} from 'ngx-perfect-scrollbar';
import {take} from 'rxjs/operators';
import {DateService} from '../../../../services/date.service';
import {LawRootService} from '../../../pages/law-page/services/law-root.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  @ViewChild('messagesEl', {read: PerfectScrollbarDirective, static: false}) messagesEl: PerfectScrollbarDirective;
  text = '';
  isChat = true;
  cmnts: any = [];
  @Input() position?: boolean;
  @Input() title: string;
  @Input() law_id: any;

  @Input() set comments(list) {
    this.cmnts = list;
    setTimeout(() => {
      this.messagesEl.update();
      this.messagesEl.scrollToY(99999999999);
    });
  }

  user: any = {};

  inputShow() {
    const token_data: any = this.auth.currentUserValue;
    if (token_data) {
      this.user = token_data.user;
      // console.log(this.user);
      // this.isChat = true;
    } else {
      // this.isChat = false;
      this.user = {};
      // this.commonApp.authModal.next(true);
    }
  }

  checkAuth() {
    const token_data: any = this.auth.currentUserValue;
    if (!token_data) {
      this.router.navigate([{outlets: {modals: 'auth'}}], {skipLocationChange: true});
      // this.user = token_data.user;
      // console.log(this.user);
      // this.isChat = true;
    }
  }

  sendMessage() {
    const data = {
      law_id: this.law_id,
      text: this.text
    };
    this.service.sendMessage(data).pipe(take(1)).subscribe((resp: any) => {
      console.log(resp);
      this.cmnts.push({
        user: this.user,
        text: this.text,
        created_at: resp.result.created_at
      });
      // console.log(this.messages);
      this.text = '';
      // setTimeout(() => {
      //   this.app_service.notification.next({
      //     type: 'succsess',
      //     text: resp.message
      //   });
      // }, 300);
      setTimeout(() => {
        this.messagesEl.update();
        this.messagesEl.scrollToY(99999999999);
      });
    });

  }

  constructor(public commonApp: CommonAppService, private router: Router, public date_s: DateService, private auth: AppAuthService, private service: LawRootService) {
  }

  ngOnInit() {
    // this.inputShow();

    this.auth.currentUserSubject.subscribe(data => {
      this.inputShow();
    });
  }

}
