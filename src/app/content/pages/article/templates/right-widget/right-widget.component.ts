import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {PlatformService} from '../../../../../services/platform.service';

@Component({
  selector: 'app-right-widget',
  templateUrl: './right-widget.component.html',
  styleUrls: ['./right-widget.component.scss']
})
export class RightWidgetComponent implements OnInit, AfterViewInit {
  @Input() slug: string;
  @Input() articles: any = [];
  // @ViewChild('el', {static: true}) el;
  // fixed = false;

  // @HostListener('document:scroll', ['$event']) onScroll(e) {
  //   if (this.el) {
  //     if (0 >= this.el.nativeElement.getBoundingClientRect().top - 20) {
  //       this.fixed = true;
  //     } else {
  //       this.fixed = false;
  //     }
  //   }
  // }

  constructor(public platform: PlatformService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    if (this.platform.check()) {
      setTimeout(() => {
        try {
          (window['adsbygoogle'] = window["adsbygoogle"] || []).push({});
        } catch (e) {
          console.error(e);
        }
      }, 100);
    }
  }н

}
