import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {take} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-video-block',
  templateUrl: './video-block.component.html',
  styleUrls: ['./video-block.component.scss']
})
export class VideoBlockComponent implements OnInit {
  @Input() descrRu: string;
  @Input() descrUa: string;
  @Input() lang: any;


  @Input() video: any;
  @Input() formats: any;
  @Input() maxSize: number;

  @Output() newVideo = new EventEmitter();
  @Output() newDescrRu = new EventEmitter();
  @Output() newDescrUa = new EventEmitter();
  error = '';

  getVideo(e) {
    this.error = null;
    const file = e.target.files[0];
    if (file) {
      const fileExtension = file.name.replace(/^.*\./, '');
      const checkFormat = this.formats.filter(format => {
        return format === fileExtension.toLowerCase();
      });
      if (checkFormat.length) {
        if (file.size > this.maxSize) {
          this.translate.get('audio-block.error-size').pipe(take(1)).subscribe(data => {
            this.error = data;
          });
        } else {
          const reader = new FileReader(); // CREATE AN NEW INSTANCE.
          const that = this;
          reader.onload = (_e: any) => {
            this.video = file;
            that.newVideo.emit(file)
          };
          reader.readAsDataURL(file);
        }
      } else {
        this.translate.get('input-photo.formats-error').pipe(take(1)).subscribe(data => {
          this.error = data + this.formats.join(', ') + ')';
        });
      }
    }
  }

  constructor(private translate: TranslateService) {
  }

  ngOnInit() {
  }

}
