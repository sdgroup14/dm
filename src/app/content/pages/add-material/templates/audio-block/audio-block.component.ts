import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-audio-block',
  templateUrl: './audio-block.component.html',
  styleUrls: ['./audio-block.component.scss']
})
export class AudioBlockComponent implements OnInit {
  @Input() descrRu: string;
  @Input() descrUa: string;
  @Input() lang: any;


  @Input() audio: any;
  @Input() formats: any;
  @Input() maxSize: number;

  @Output() newAudio = new EventEmitter();
  @Output() newDescrRu = new EventEmitter();
  @Output() newDescrUa = new EventEmitter();

  error = '';

  getAudio(e) {
    this.error = null;
    const file = e.target.files[0];
    if (file) {
      const fileExtension = file.name.replace(/^.*\./, '');
      const checkFormat = this.formats.filter(format => {
        return format === fileExtension.toLowerCase();
      });
      if (checkFormat.length) {
        if (file.size > this.maxSize) {
          this.translate.get('add-material-page.audio-block.error-size').pipe(take(1)).subscribe(content => {
            this.error = content;
          });
        } else {
          const reader = new FileReader(); // CREATE AN NEW INSTANCE.
          const that = this;
          reader.onload = (_e: any) => {
            this.audio = file;
            that.newAudio.emit(file)
          };
          reader.readAsDataURL(file);
        }
      } else {
        this.translate.get('add-material-page.audio-block.error-format').pipe(take(1)).subscribe(content => {
          this.error = content;
        });
      }
    }
  }

  constructor(private translate: TranslateService) {
  }

  ngOnInit() {
  }
}
