import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-ratio-voting-widget',
  templateUrl: './ratio-voting-widget.component.html',
  styleUrls: ['./ratio-voting-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RatioVotingWidgetComponent implements OnInit {
  vote_compartion: any = {};
  init = false;
  empty = true;

  @Input() set changeСompartion(data) {
    if (data.data) {
      this.empty = false;
      if (Object.keys(data).length) {
        // console.log(data);
        data.for_percent = Math.floor(data.for_match * 100 / data.laws_count);
        data.against_percent = Math.floor(data.against_match * 100 / data.laws_count);
        data.abstained_percent = Math.floor(data.abstained_match * 100 / data.laws_count);
        data.diff_for_user_percent = Math.floor(data.diff_for_user * 100 / data.laws_count);
        data.diff_against_user_percent = Math.floor(data.diff_against_user * 100 / data.laws_count);
        data.diff_abstained_user_percent = Math.floor(data.diff_abstained_user * 100 / data.laws_count);
        this.vote_compartion = data;
        this.init = true;
      }
    } else {
      this.empty = true;
    }

  }

  constructor() {
  }

  ngOnInit() {
  }

}
