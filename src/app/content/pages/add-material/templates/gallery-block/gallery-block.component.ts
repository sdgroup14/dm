import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {AddMaterialService} from '../../services/add-material.service';
import {TranslateService} from '@ngx-translate/core';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-gallery-block',
  templateUrl: './gallery-block.component.html',
  styleUrls: ['./gallery-block.component.scss']
})
export class GalleryBlockComponent implements OnInit {

  @Output() setSlidesChange = new EventEmitter();
  slides = [];


  activeSlide: any;
  error = '';

  i = 0;
  config: any = {
    observable: true,
    autoHeight: true,
    direction: 'horizontal',
    navigation: {
      nextEl: '.button-next',
      prevEl: '.button-prev'
    }
  };
  @Input() lang: any;
  @Input() formats: any;
  @Input() maxSize: number;
  @Input() minWidth: number;
  @ViewChild('slider', {static: false}) swiper;

  @Input() set setSlides(slides) {
    // console.log(slides);
    if (slides && slides.length) {
      this.slides = slides;
      this.activeSlide = slides[0];
    }
  }


  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.slides, event.previousIndex, event.currentIndex);
    this.activeSlide = this.slides[this.i];
  }

  deleteSlide(slide) {
    if (slide.hasOwnProperty('id')) {
      this.service.deleteSlide(slide.id).subscribe(resp => {
        this.error = '';
        setTimeout(() => {
          this.swiper.directiveRef.swiper().update();
        }, 10);
        const index = this.slides.indexOf(slide);
        this.slides.splice(index, 1);
        this.activeSlide = this.slides[this.i];
        // if (index === this.i) {
        //   this.swiper.directiveRef.swiper().slideTo(0);
        //
        // }
      });
    } else {
      this.error = '';
      setTimeout(() => {
        this.swiper.directiveRef.swiper().update();
      }, 10);
      const index = this.slides.indexOf(slide);
      this.slides.splice(index, 1);
      this.activeSlide = this.slides[this.i];
    }
  }

  getPhotos(e) {
    this.error = null;
    const files = e.target.files;
    // console.log(files);
    // {
    //
    // }
    if (this.slides.length > 8) {
      this.translate.get('add-material-page.gallery-block.max-photo-length').pipe(take(1)).subscribe(content => {
        this.error = content;
      });
      return;
    }
    for (const file of files) {
      if (this.slides.length > 8) {
        this.translate.get('add-material-page.gallery-block.max-photo-length').pipe(take(1)).subscribe(content => {
          this.error = content;
        });
        return;
      }
      if (file) {
        const fileExtension = file.name.replace(/^.*\./, '');
        const checkFormat = this.formats.filter(format => {
          return format === fileExtension.toLowerCase();
        });
        if (checkFormat.length) {
          if (file.size > this.maxSize) {
            this.translate.get('add-material-page.gallery-block.error-size').pipe(take(1)).subscribe(content => {
              this.error = content;
            });
          } else {
            const reader = new FileReader(); // CREATE AN NEW INSTANCE.
            const that = this;
            reader.onload = (_e: any) => {
              const img = new Image();
              img.src = _e.target.result;

              img.onload = () => {
                const w = img.width;
                if (w < this.minWidth) {
                  this.translate.get('add-material-page.gallery-block.error-min-size').pipe(take(1)).subscribe(content => {
                    that.error = content + this.minWidth + 'px';
                  });
                } else {
                  this.slides.push({
                    img: file,
                    caption_ru: '',
                    caption_ua: ''
                  });
                  that.setSlidesChange.emit(this.slides);
                  setTimeout(() => {
                    that.swiper.directiveRef.swiper().update();
                    this.activeSlide = this.slides[this.i];
                  }, 10);

                  // this.img = file;
                  // that.newImg.emit(file);
                }
              };
            };
            reader.readAsDataURL(file);
          }
        } else {
          this.translate.get('add-material-page.gallery-block.error-format').pipe(take(1)).subscribe(content => {
            this.error = content + this.formats.join(', ') + ')';
          });
        }
      }
    }

  }

  constructor(private service: AddMaterialService, private translate: TranslateService) {
  }

  ngOnInit() {
  }

}
