import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DateService} from '../../../../../services/date.service';

@Component({
  selector: 'app-law-list-bottom',
  templateUrl: './law-list-bottom.component.html',
  styleUrls: ['./law-list-bottom.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LawListBottomComponent implements OnInit {
  laws: any = [];
  page = 1;
  last_page = 1;
  @Output() changePage = new EventEmitter();

  @Input() set changeList(data) {
    if (Object.keys(data).length) {
      // console.log(data);
      this.laws = data.list;
      this.last_page = data.last_page;
    }
  }

  pageHandler() {
    this.page += 1;
    this.changePage.emit(this.page);
  }

  constructor(public dateService: DateService) {
  }

  ngOnInit() {
  }

}
