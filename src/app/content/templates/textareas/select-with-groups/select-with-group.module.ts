import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SelectWithGroupsComponent} from './select-with-groups.component';
import {FormsModule} from '@angular/forms';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {SheredTranslateModule} from '../../../../modules/shered-translate.module';
import {ClickOutsideModule} from '../../../../directives/click-outside/click-outside.module';

@NgModule({
  declarations: [SelectWithGroupsComponent],
  imports: [
    CommonModule,
    FormsModule,
    PerfectScrollbarModule,
    ClickOutsideModule,
    SheredTranslateModule
  ],
  exports: [SelectWithGroupsComponent]
})
export class SelectWithGroupModule { }
