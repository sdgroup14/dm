import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SocialWidgetComponent} from './social-widget.component';
import {ShareService} from '../../../../services/share.service';

@NgModule({
  declarations: [SocialWidgetComponent],
  imports: [
    CommonModule
  ],
  exports: [SocialWidgetComponent],
  providers: [ShareService]
})
export class SocialWidgetModule { }
