import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-news-html-template',
  templateUrl: './news-html-template.component.html',
  styleUrls: ['./news-html-template.component.scss']
})
export class NewsHtmlTemplateComponent implements OnInit {
  items: any[] = [];

  @Input() set length(count) {
    this.items.length = count;
  }

  constructor() {
  }

  ngOnInit() {
  }

}
