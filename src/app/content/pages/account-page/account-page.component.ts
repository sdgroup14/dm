import {Component, OnDestroy, OnInit} from '@angular/core';
import {AccountPageService} from './services/account-page.service';
import {take} from 'rxjs/operators';
import {NavigationEnd, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
// import {fadeAnimation} from '../../../animations/animations';
import {PagePreloaderService} from '../../templates/preloaders/page-preloader/page-preloader.service';
import {Subscription} from 'rxjs';
import {MetaService} from '../../../services/meta.service';

@Component({
  selector: 'app-account-page',
  templateUrl: './account-page.component.html',
  styleUrls: ['./account-page.component.scss'],
  // animations: [fadeAnimation]
})
export class AccountPageComponent implements OnInit, OnDestroy {
  private subscriptions = new Subscription();
  isNotif = 0;
  menu: any = [];

  constructor(
    private appMeta: MetaService,
    private service: AccountPageService,
    private page_spinner: PagePreloaderService,
    private translate: TranslateService,
    private router: Router) {

    router.events.subscribe((ev: any) => {
      if (ev instanceof NavigationEnd) {
        // Hide loading indicator
        if (ev.url === '/account/notifications') {
          this.isNotif = 0;
        } else {
          this.service.checkNotif().pipe(take(1)).subscribe((resp: any) => {
            // console.log(resp);
            this.isNotif = resp.result.count;
          });
        }
      }

    });
  }

  ngOnDestroy() {
    this.page_spinner.show();
    this.subscriptions.unsubscribe();
  }

  ngOnInit() {
    this.menu = this.translate.get('account-page.nav');
    // setTimeout(()=> {
    this.page_spinner.hide();

    this.appMeta.setMeta(
      '',
      '',
      '',
      '',
      '',
      true
    );
    // }, 100)

  }

}
