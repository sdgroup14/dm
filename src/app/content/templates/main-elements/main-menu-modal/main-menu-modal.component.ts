import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {LangService} from '../../../../services/lang.service';
import {NavigationEnd, Router} from '@angular/router';
import {CommonAppService} from '../../../../services/common-app.service';

@Component({
  selector: 'app-main-menu-modal',
  templateUrl: './main-menu-modal.component.html',
  styleUrls: ['./main-menu-modal.component.scss']
})
export class MainMenuModalComponent implements OnInit {
  @Input() defaultLang: boolean;
  // tslint:disable-next-line:no-output-native
  // isOpen = false;
  isOpen = false;
  @Output() close = new EventEmitter();
  menu: any = [];

  // close() {
  //   // setTimeout(() => {
  //   //   this.isOpen = false;
  //   // }, 150);
  //   this.isOpen = true;
  //   this.service.mobileMenuModal.next(true);
  // }

  constructor(public langService: LangService, public router: Router, private translate: TranslateService, private service: CommonAppService) {
//     this.router.events.subscribe((event: any) => {
//       if (event instanceof NavigationEnd) {
//         if (this.isOpen) {
//           // console.log('123')
//           // setTimeout
//           // setTimeout(() => {
//           //   this.isOpen = true;
//           // }, 150);
//           setTimeout(() => {
//             this.isOpen = false;
//             this.router.navigate([{outlets: {modals: null}}]);
//           }, 100);
//           // this.close.emit(true)
//
//         }
// // console.log('close')
//         // const routerLang: any = event.url.split('/')[1];
//         // const _lang = routerLang === 'ru' ? 'ru' : 'uk';
//         // moment.locale(_lang);
//       }
//     });
  }

  ngOnInit() {
    this.menu = this.translate.get('mobile-modal.menu');
  }

}
