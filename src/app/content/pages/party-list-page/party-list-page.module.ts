import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PartyListPageRoutingModule } from './party-list-page-routing.module';
import {PartyListPageComponent} from './party-list-page.component';
import {PartyListPageService} from './services/party-list-page.service';
import {SheredPaginationModule} from '../../../modules/shered-pagination.module';
import {SelectSearchModule} from '../../templates/textareas/select-search/select-search.module';
import {SelectWithGroupModule} from '../../templates/textareas/select-with-groups/select-with-group.module';
import {SimpleSelectModule} from '../../templates/textareas/simple-select/simple-select.module';
import {SheredTranslateModule} from '../../../modules/shered-translate.module';
import {RightWidgetToolbarModule} from '../../templates/widgets/right-widget-toolbar/right-widget-toolbar.module';
import {FormsModule} from '@angular/forms';
import {BtnCloseModule} from '../../templates/elements/btn-close/btn-close.module';
import {EmpyListStringModule} from '../../templates/elements/empy-list-string/empy-list-string.module';
import {SheredTooltipModule} from '../../../modules/shered-tooltip.module';

@NgModule({
  declarations: [
    PartyListPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    PartyListPageRoutingModule,
    SheredPaginationModule,
    SelectSearchModule,
    SelectWithGroupModule,
    SimpleSelectModule,
    SheredTranslateModule,
    RightWidgetToolbarModule,
    SheredTooltipModule,
    BtnCloseModule,
    EmpyListStringModule
  ],
  providers: [PartyListPageService]
})
export class PartyListPageModule { }
