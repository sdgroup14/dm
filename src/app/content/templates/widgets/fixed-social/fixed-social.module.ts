import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SocialWidgetModule} from '../social-widget/social-widget.module';
import {FixedSocialComponent} from './fixed-social.component';
import {BackBtnFixedModule} from '../../elements/back-btn-fixed/back-btn-fixed.module';

@NgModule({
  declarations: [FixedSocialComponent],
  imports: [
    CommonModule,
    SocialWidgetModule,
    BackBtnFixedModule
  ],
  exports: [SocialWidgetModule, FixedSocialComponent]
})
export class FixedSocialModule { }
