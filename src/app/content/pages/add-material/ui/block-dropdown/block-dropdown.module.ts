import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BlockDropdownComponent} from './block-dropdown.component';
import {SheredTranslateModule} from '../../../../../modules/shered-translate.module';
import {SheredTooltipModule} from '../../../../../modules/shered-tooltip.module';
import {RouterModule} from '@angular/router';
import {ClickOutsideModule} from '../../../../../directives/click-outside/click-outside.module';

@NgModule({
  declarations: [BlockDropdownComponent],
  imports: [
    CommonModule,
    SheredTranslateModule,
    ClickOutsideModule,
    SheredTooltipModule,
    RouterModule
  ],
  exports: [BlockDropdownComponent]
})
export class BlockDropdownModule { }
