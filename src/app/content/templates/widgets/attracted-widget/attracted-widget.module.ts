import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AttractedWidgetComponent} from './attracted-widget.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {SheredTranslateModule} from '../../../../modules/shered-translate.module';
import {NewsHtmlTemplateModule} from '../../preloaders/news-html-template/news-html-template.module';
import {RouterModule} from '@angular/router';
import {EmpyListStringModule} from '../../elements/empy-list-string/empy-list-string.module';

@NgModule({
  declarations: [AttractedWidgetComponent],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    SheredTranslateModule,
    NewsHtmlTemplateModule,
    RouterModule,
    EmpyListStringModule
  ],
  exports: [AttractedWidgetComponent]
})
export class AttractedWidgetModule { }
