import {Component, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'app-mobile-share-widget',
  templateUrl: './mobile-share-widget.component.html',
  styleUrls: ['./mobile-share-widget.component.scss']
})
export class MobileShareWidgetComponent implements OnInit {
  isShow = false;
  lastScrollTop = 0;

// element should be replaced with the actual target element on which you have applied scroll, use window in case of no target element.

  @HostListener('document:scroll', ['$event']) onScroll(e) {
    // console.log(window.scrollY)


    const st = window.pageYOffset || document.documentElement.scrollTop; // Credits: "https://github.com/qeremy/so/blob/master/so.dom.js#L426"
    if (st > this.lastScrollTop && st > 200) {
      // downscroll code
      this.isShow = true;
    } else if(st < 200){
      this.isShow = false;
      // upscroll code
    }
    this.lastScrollTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling
    // if (this.el) {
    //   if (0 >= this.el.nativeElement.getBoundingClientRect().top - 20) {
    //     this.fixed = true;
    //   } else {
    //     this.fixed = false;
    //   }
    // }
  }

  constructor() {
  }

  ngOnInit() {
  }

}
