import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

declare var twttr: any;

@Component({
  selector: 'app-embed-block',
  templateUrl: './embed-block.component.html',
  styleUrls: ['./embed-block.component.scss']
})
export class EmbedBlockComponent implements OnInit, OnChanges {
  @Input() type: number;
  @Input() embed: any;
  @Output() embedChange = new EventEmitter();

  update() {
    if (this.type === 9) {
      setTimeout(() => {
        twttr.widgets.load();
      }, 1);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.update();
    // const embed: SimpleChange = changes.embed;
    // console.log('prev value: ', embed.previousValue);
    // console.log('got name: ', embed.currentValue);
    // if(){
    //
    // }
    // this._name = name.currentValue.toUpperCase();
  }

  constructor() {
  }

  ngOnInit() {
  }

}
