import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CurveService {

  drawLine(ctx, startX, startY, endX, endY, delta, dash?) {
    const dx = endX - startX;
    // const delta = 1;
    ctx.strokeStyle = '#AEB5BB';
    ctx.lineWidth = 1;
    ctx.beginPath();
    if (dash) {
      ctx.setLineDash([4, 4]);
    }
    ctx.moveTo(startX, startY);
    ctx.bezierCurveTo(startX + dx * delta, startY, endX - dx * delta, endY, endX, endY);
    ctx.stroke();
    ctx.beginPath();
    ctx.strokeStyle = '#AEB5BB';
    ctx.lineWidth = 1;
    ctx.stroke();
  }

  drawArrow(ctx, fromx, fromy, tox, toy, angle) {
    // const startXPos = endX + 20;
    // const startYPos = endY - 5;
    // const endXPos = endX;
    // const endYPos = endY;
    const headlen = 7;
    ctx.lineTo(tox, toy);
    ctx.lineTo(tox - headlen * Math.cos(angle - Math.PI / 5), toy - headlen * Math.sin(angle - Math.PI / 5));
    ctx.moveTo(tox, toy);
    ctx.lineTo(tox - headlen * Math.cos(angle + Math.PI / 5), toy - headlen * Math.sin(angle + Math.PI / 5));
  }

  constructor() {
  }
}
