import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PersonsListPageRoutingModule} from './persons-list-page-routing.module';
import {PersonsListPageComponent} from './persons-list-page.component';
import {PersonsListPageService} from './services/persons-list-page.service';
import {SheredPaginationModule} from '../../../modules/shered-pagination.module';
import {SelectSearchModule} from '../../templates/textareas/select-search/select-search.module';
import {SelectWithGroupModule} from '../../templates/textareas/select-with-groups/select-with-group.module';
import {SimpleSelectModule} from '../../templates/textareas/simple-select/simple-select.module';
import {SheredTranslateModule} from '../../../modules/shered-translate.module';
import {FormsModule} from '@angular/forms';
import {RightWidgetToolbarModule} from '../../templates/widgets/right-widget-toolbar/right-widget-toolbar.module';
import {BtnCloseModule} from '../../templates/elements/btn-close/btn-close.module';
import {EmpyListStringModule} from '../../templates/elements/empy-list-string/empy-list-string.module';
import {SheredTooltipModule} from '../../../modules/shered-tooltip.module';

@NgModule({
  declarations: [PersonsListPageComponent],
  imports: [
    CommonModule,
    FormsModule,
    PersonsListPageRoutingModule,
    SheredPaginationModule,
    SelectSearchModule,
    SelectWithGroupModule,
    SimpleSelectModule,
    SheredTooltipModule,
    SheredTranslateModule,
    RightWidgetToolbarModule,
    BtnCloseModule,
    EmpyListStringModule,
  ],
  providers: [PersonsListPageService]
})
export class PersonsListPageModule {
}
